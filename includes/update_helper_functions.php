<?php

/**
 * @file
 * Helper functions for update hooks.
 */

/**
 * Function to get the new config for auto/manual blocks.
 *
 * @param array $config
 *   The block config.
 * @param string $content_type
 *   The content type.
 * @param string $type
 *   The type of block.
 *
 * @return array
 *   The array of new config.
 */
function _uw_custom_blocks_get_new_config(
  array $config,
  string $content_type,
  string $type
): array {

  // Get all the defaults for the new block.
  $new_config = [
    'label' => $config['label'],
    'provider' => 'uw_custom_blocks',
    'label_display' => $config['label_display'],
    'heading_level' => $config['heading_level'] ?? 'h2',
    'content_type' => $content_type,
  ];

  if ($type == 'auto') {

    // Get the automatic content types.
    $cts = \Drupal::service('uw_custom_blocks.uw_block_service')
      ->getAutomaticContentTypes();

    // Set all the content types in the new config.
    foreach (array_keys($cts) as $ct) {
      $new_config[$ct] = [];
    }

    // Set the new block id to automatic.
    $new_config['id'] = 'uw_cbl_automatic_list';
  }

  if ($type == 'manual') {

    // Get the manual content types.
    $cts = \Drupal::service('uw_custom_blocks.uw_block_service')
      ->getManualContentTypes();

    // Set all the content types in the new config.
    foreach (array_keys($cts) as $ct) {
      $new_config[$ct] = [];
    }

    // Set the new block id to manual.
    $new_config['id'] = 'uw_cbl_manual_list';
  }

  if ($type == 'teaser') {

    // Set the new block id to teaser.
    $new_config['id'] = 'uw_cbl_teaser';
  }

  // Get the rest of the new config based on content type.
  switch ($type) {

    case 'auto':
      _uw_set_auto_new_config(
        $new_config,
        $config,
        $content_type
      );
      break;

    case 'manual':
      _uw_set_manual_new_config(
        $new_config,
        $config,
        $content_type
      );
      break;

    case 'teaser':
      if (isset($config['tid'])) {
        $new_config['nid'] = $config['tid'];
      }
      elseif (isset($config['entity_id'])) {
        $new_config['nid'] = $config['entity_id'];
      }
      else {
        $new_config['nid'] = $config['nid'];
      }
      break;
  }

  return $new_config;
}

/**
 * Function to set the rest of the auto new config.
 *
 * @param array $new_config
 *   Array of new config.
 * @param array $config
 *   Array of current config.
 * @param string $content_type
 *   The content type.
 */
function _uw_set_auto_new_config(
  array &$new_config,
  array $config,
  string $content_type
): void {

  // Get the rest of the config based on the content type.
  switch ($content_type) {

    case 'blog':
      $new_config[$content_type]['limit'] = _uw_get_limit_from_config($config);
      $new_config[$content_type]['tags'] = _uw_get_value_from_exposed_filters(
        $config,
        'field_uw_blog_tags_target_id',
        TRUE
      );
      break;

    case 'catalog':
      $new_config[$content_type]['heading_selector'] = $config['heading_selector'] ?? 'h2';
      break;

    case 'catalog_item':
      $new_config[$content_type]['limit'] = _uw_get_limit_from_config($config);
      $new_config[$content_type]['show_all_items'] = $config['show_all_items'];
      $new_config[$content_type]['tags'] = $config['tags'] ?? NULL;
      $new_config[$content_type]['catalog'] = $config['catalog'] ? [$config['catalog']] : NULL;
      break;

    case 'contact':
      $new_config[$content_type]['limit'] = _uw_get_limit_from_config($config);
      $new_config[$content_type]['groups'] = $config['nids'];
      $new_config[$content_type]['show_photos'] = $config['show_photos'];
      $new_config[$content_type]['sort'] = $config['sort'];
      break;

    case 'event':
      $new_config[$content_type]['limit'] = _uw_get_limit_from_config($config);
      $new_config[$content_type]['type'] = _uw_get_value_from_exposed_filters(
        $config,
        'field_uw_event_type_target_id',
        FALSE
      );
      $new_config[$content_type]['tags'] = _uw_get_value_from_exposed_filters(
        $config,
        'field_uw_event_tags_target_id',
        TRUE
      );
      break;

    case 'news_item':
      $new_config[$content_type]['limit'] = _uw_get_limit_from_config($config);
      $new_config[$content_type]['tags'] = _uw_get_value_from_exposed_filters(
        $config,
        'field_uw_news_tags_target_id',
        TRUE
      );
      break;

    case 'opportunity':
      $new_config[$content_type]['limit'] = _uw_get_limit_from_config($config);
      $new_config[$content_type]['type'] = $config['exposed_filter_values']['opportunity_type'] ?? [];
      $new_config[$content_type]['employment'] = $config['exposed_filter_values']['employment_type'] ?? [];
      $new_config[$content_type]['rate_of_pay'] = $config['exposed_filter_values']['rate_pay_type'] ?? [];
      break;

    case 'profile':
      $new_config[$content_type]['limit'] = _uw_get_limit_from_config($config);
      $new_config[$content_type]['types'] = !empty($config['nids']) ? $config['nids'] : [];
      $new_config[$content_type]['sort'] = $config['sort'];
      $new_config[$content_type]['show_photos'] = $config['show_photos'];
      break;

    case 'publication_reference':
      $new_config[$content_type]['limit'] = _uw_get_limit_from_config($config);
      $new_config[$content_type]['keywords'] = $config['keywords'] ?? [];
      $new_config[$content_type]['type'] = $config['type'] ?? [];
      $new_config[$content_type]['year'] = $config['year'] ?? NULL;
      $new_config[$content_type]['search'] = $config['search'] ?? NULL;
      break;
  }
}

/**
 * Function to set the rest of the manual new config.
 *
 * @param array $new_config
 *   Array of new config.
 * @param array $config
 *   Array of current config.
 * @param string $content_type
 *   The content type.
 */
function _uw_set_manual_new_config(
  array &$new_config,
  array $config,
  string $content_type
): void {

  // If this is a contact, have to get the show title.
  if ($content_type == 'contact') {
    $new_config[$content_type]['show_title'] = $config['show_title'];
  }

  // Get the rest of the config.
  $new_config[$content_type]['show_image'] = $config['show_image'];
  $new_config[$content_type]['num_' . $content_type . '_ids'] = $config['num_' . $content_type . '_ids'];
  $new_config[$content_type][$content_type . '_ids'] = $config[$content_type . '_ids'];
}

/**
 * Function to get machine name of old automatic blocks.
 *
 * @return string[]
 *   Array of machine names.
 */
function _uw_get_auto_block_machine_names(): array {

  return [
    'views_block:uw_view_blogs-blogs_listing_block' => 'blog',
    'uw_cbl_catalog_item_listing' => 'catalog_item',
    'uw_cbl_catalog_listing' => 'catalog',
    'uw_cbl_contacts_automatic' => 'contact',
    'views_block:uw_view_events-events_listing_block' => 'event',
    'views_block:uw_view_news_items-news_items_listing_block' => 'news_item',
    'views_block:uw_view_opportunities-block_1' => 'opportunity',
    'uw_cbl_profiles_automatic' => 'profile',
    'uw_cbl_publications_listing' => 'publication_reference',
  ];
}

/**
 * Function to get machine name of old automatic blocks.
 *
 * @return string[]
 *   Array of machine names.
 */
function _uw_get_manual_block_machine_names(): array {

  return [
    'uw_cbl_contacts_manual' => 'contact',
    'uw_cbl_profiles_manual' => 'profile',
  ];
}

/**
 * Function to get the configs to delete.
 *
 * @return string[]
 *   Array of configs to delete.
 */
function _uw_get_configs_to_delete(): array {

  return [
    'layout_builder_browser.layout_builder_browser_block.uw_lbb_catalog_item_list',
    'layout_builder_browser.layout_builder_browser_block.uw_lbb_catalog_list',
    'layout_builder_browser.layout_builder_browser_block.uw_lbb_contact_list_automatic',
    'layout_builder_browser.layout_builder_browser_block.uw_lbb_contact_list_manual',
    'layout_builder_browser.layout_builder_browser_block.views_blockuw_view_news_items-news_items_listing_block',
    'layout_builder_browser.layout_builder_browser_block.uw_lbb_opportunity_list',
    'layout_builder_browser.layout_builder_browser_block.uw_lbb_profile_list_automatic',
    'layout_builder_browser.layout_builder_browser_block.uw_lbb_profile_list_manual',
    'layout_builder_browser.layout_builder_browser_block.uw_lbb_project_listing',
    'layout_builder_browser.layout_builder_browser_block.uw_lbb_publication_listing',
    'layout_builder_browser.layout_builder_browser_block.uw_lbb_services_list',
    'layout_builder_browser.layout_builder_browser_block.views_blockuw_view_blogs-blogs_listing_block',
    'layout_builder_browser.layout_builder_browser_block.views_blockuw_view_events-events_listing_block',
    'layout_builder_browser.layout_builder_browser_block.views_blockuw_view_news_items-news_items_listing_block.yml',
    'layout_builder_browser.layout_builder_browser_block.uw_cbl_blog_teaser.yml',
    'layout_builder_browser.layout_builder_browser_block.uw_cbl_event_teaser.yml',
    'layout_builder_browser.layout_builder_browser_block.uw_cbl_news_teaser.yml',
    'layout_builder_browser.layout_builder_browser_block.uw_cbl_profile_teaser.yml',
    'layout_builder_browser.layout_builder_browser_block.uw_cbl_catalog_item_teaser.yml',
    'layout_builder_browser.layout_builder_browser_block.uw_cbl_catalog_teaser.yml',
    'layout_builder_browser.layout_builder_browser_block.uw_cbl_contact_teaser.yml',
    'layout_builder_browser.layout_builder_browser_block.uw_cbl_opportunity_teaser.yml',
    'layout_builder_browser.layout_builder_browser_block.uw_cbl_project_teaser.yml',
    'layout_builder_browser.layout_builder_browser_block.uw_cbl_publication_teaser.yml',
    'layout_builder_browser.layout_builder_browser_block.uw_cbl_service_teaser.yml',
    'layout_builder_browser.layout_builder_browser_blockcat.uw_bc_teasers.yml',
  ];
}

/**
 * Function to get the limit from config.
 *
 * @param array $config
 *   The array of config.
 *
 * @return int
 *   The limit.
 */
function _uw_get_limit_from_config(array $config): int {

  // If there are values in the items per page and
  // the value is not none (meaning the default),
  // then get the value from the config.
  // If this is none of has no value in items per
  // page then return the default value of 3.
  if (isset($config['items_per_block'])) {
    return $config['items_per_block'];
  }
  elseif (
    isset($config['items_per_page']) &&
    $config['items_per_page'] !== 'none'
  ) {
    return $config['items_per_page'];
  }
  else {

    // If this already has a limit setting, check that
    // it is of none and use it, if not return default.
    if (
      isset($config['limit']['limit']) &&
      $config['limit']['limit'] !== 'none'
    ) {
      return $config['limit']['limit'];
    }
    else {
      return 3;
    }
  }
}

/**
 * Function to get the values from the exposed filters.
 *
 * @param array $config
 *   Array of config.
 * @param string $field_name
 *   The field name.
 * @param bool $has_target_id
 *   Flag if this has a target id.
 *
 * @return array
 *   Values from the exposed filters.
 */
function _uw_get_value_from_exposed_filters(
  array $config,
  string $field_name,
  bool $has_target_id
): array {

  // If there is a value in the exposed filter and there
  // are actual values in it, then process it.  If there
  // are no values then send an empty array.
  if (
    isset($config['exposed_filter_values'][$field_name]) &&
    !empty($config['exposed_filter_values'][$field_name])
  ) {

    // If this is an array, get the values into an array.
    // If not an array, get just the value.
    if (is_array($config['exposed_filter_values'][$field_name])) {

      // Step through each value and set new config.
      foreach ($config['exposed_filter_values'][$field_name] as $id) {

        // If this has a target id, then get value using the
        // target id, if not just use the value.
        if ($has_target_id) {
          $new_config[] = $id['target_id'];
        }
        else {
          $new_config[] = $id;
        }
      }
    }
    else {

      // Get the value based on if it has a target id.
      if ($has_target_id) {
        $new_config = $config['exposed_filter_values'][$field_name]['target_id'];
      }
      else {
        $new_config = $config['exposed_filter_values'][$field_name];
      }
    }
  }
  else {

    // Set to empty array since there are no values to get.
    $new_config = [];
  }

  return $new_config;
}

/**
 * Function to delete the old block config.
 */
function _uw_delete_old_blocks_config(): void {

  // Config to be deleted.
  $configs = _uw_get_configs_to_delete();

  // Step through each of the config and delete it.
  foreach ($configs as $config) {
    \Drupal::configFactory()
      ->getEditable($config)->delete();
  }
}

/**
 * Function to set the new block config.
 *
 * @param array $nodes
 *   Array of nodes.
 */
function _uw_set_new_block_config(array $nodes) {

  // Get the auto and manual block machine names.
  $auto_block_machine_names = _uw_get_auto_block_machine_names();
  $manual_block_machine_names = _uw_get_manual_block_machine_names();

  // Step through each node and check if has full width image,
  // if it does convert to image block.
  foreach ($nodes as $node) {

    // Load all the revisions for the node.
    $vids = \Drupal::service('entity_type.manager')->getStorage('node')->revisionIds($node);

    // Step through each revision, and check if we have to
    // change the settings for the listing blocks.
    foreach ($vids as $vid) {

      // Load the revision node.
      $revision_node = \Drupal::service('entity_type.manager')->getStorage('node')->loadRevision($vid);

      // Comments from here down are the same as above.
      $layout = $revision_node->get('layout_builder__layout');
      $sections = $layout->getSections();

      // Step through each of the sections and check components
      // for full width image.
      foreach ($sections as $section) {

        // Load the components.
        $components = $section->getComponents();

        // Step through each of the components.
        foreach ($components as $component) {

          // Get the current block config.
          $config = $component->get('configuration');

          // If this block is in the old auto blocks,
          // then get the new config.
          if (
            in_array(
              $config['id'],
              array_keys($auto_block_machine_names)
            )
          ) {

            // Get the new config for the automatic block.
            $new_config = _uw_custom_blocks_get_new_config(
              $config,
              $auto_block_machine_names[$config['id']],
              'auto'
            );

            // Save the component.
            $component->setConfiguration($new_config);
          }

          // If this block is in the old auto blocks,
          // then get the new config.
          if (
            in_array(
              $config['id'],
              array_keys($manual_block_machine_names)
            )
          ) {

            // Get the new config for the automatic block.
            $new_config = _uw_custom_blocks_get_new_config(
              $config,
              $manual_block_machine_names[$config['id']],
              'manual'
            );

            // Save the component.
            $component->setConfiguration($new_config);
          }

          // If the config id contains teaser, get the new config
          // for a teaser.
          if (str_contains($config['id'], '_teaser')) {

            // Get the content based on the config id.
            $content_type = str_replace('uw_cbl_', '', $config['id']);
            $content_type = str_replace('_teaser', '', $content_type);

            // Ensure that we get right name for pub ref.
            if ($content_type == 'publication') {
              $content_type = 'publication_reference';
            }

            // Get the new config for the teaser block.
            $new_config = _uw_custom_blocks_get_new_config(
              $config,
              $content_type,
              'teaser'
            );

            // Save the component.
            $component->setConfiguration($new_config);
          }
        }
      }

      // Set the syncing to true so that a new revision
      // is not create and the current revision is only
      // updated.
      // https://www.drupal.org/node/3052114.
      // https://www.drupal.org/project/drupal/issues/3335119.
      $revision_node->setSyncing(TRUE);

      // Save the revision node.
      $revision_node->save();
    }
  }
}

/**
 * Function to update the layout builder temp for auto/manual blocks.
 */
function _uw_update_layout_builder_temp_auto_manual() {

  // Get the auto and manual block machine names.
  $auto_block_machine_names = _uw_get_auto_block_machine_names();
  $manual_block_machine_names = _uw_get_manual_block_machine_names();

  // The DB connection.
  $database = \Drupal::database();

  // Query to get the layout builder from nodes.
  $query = $database->select('key_value_expire', 'kv');
  $query->fields('kv');
  $query->condition('kv.collection', '%layout%', 'LIKE');
  $result = $query->execute();

  // Step through the results and change out any full
  // width image to image.
  foreach ($result as $record) {

    // Unserialize the value, which has the sections in it.
    $value = unserialize($record->value);

    // Get the section storage from the serialized data.
    $layout = $value->data['section_storage'];

    // Get out the sections.
    $sections = $layout->getSections();

    // Flag to update the layout.
    $layout_flag = FALSE;

    // Step through each of the sections and check components
    // for full width image.
    foreach ($sections as $section) {

      // Load the components.
      $components = $section->getComponents();

      // Step through each of the components.
      foreach ($components as $component) {

        // Get the config for component.
        $config = $component->get('configuration');

        // If this is an auto block, then get the new config.
        if (
          in_array(
            $config['id'],
            array_keys($auto_block_machine_names)
          )
        ) {
          $new_config = _uw_custom_blocks_get_new_config(
            $config,
            $auto_block_machine_names[$config['id']],
            'auto'
          );
        }

        // If this block is in the old auto blocks,
        // then get the new config.
        if (
          in_array(
            $config['id'],
            array_keys($manual_block_machine_names)
          )
        ) {

          // Get the new config for the automatic block.
          $new_config = _uw_custom_blocks_get_new_config(
            $config,
            $manual_block_machine_names[$config['id']],
            'manual'
          );
        }

        // If the config id contains teaser, get the new config
        // for a teaser.
        if (str_contains($config['id'], '_teaser')) {

          // Get the content based on the config id.
          $content_type = str_replace('uw_cbl_', '', $config['id']);
          $content_type = str_replace('_teaser', '', $content_type);

          // Ensure that we get right name for pub ref.
          if ($content_type == 'publication') {
            $content_type = 'publication_reference';
          }

          // Get the new config for the teaser block.
          $new_config = _uw_custom_blocks_get_new_config(
            $config,
            $content_type,
            'teaser'
          );
        }

        // Only save the component if there is new config.
        if (isset($new_config) && $new_config) {

          // Save the component.
          $component->setConfiguration($new_config);
          $layout_flag = TRUE;
        }
      }
    }

    // Update the layout if the flag is set.
    if ($layout_flag) {

      // Save the layout.
      $layout->save();
    }

    // Set the updated sections.
    $value->data['section_storage'] = $layout;

    // Update the DB.
    $database->update('key_value_expire')
      ->fields(['value' => serialize($value)])
      ->condition('name', $record->name)
      ->execute();
  }
}
