/**
 * @file
 * Javascript for banners block.
 */

(function ($, Drupal) {
  Drupal.behaviors.uw_cbl_banners = {
    attach: function (context, settings) {

      // On document ready, set the banner options based on selections.
      $(document).ready(function () {

        // Set the banner options.
        setBannerOptions();

        // When the text overlay style changes, update the banner options.
        $('select[name="settings[block_form][field_uw_text_overlay_style]"]').change(function () {
          setBannerOptions();
        });
      });

      // Function to set banner options.
      function setBannerOptions() {

        // Hide the other text field.
        $('.field--name-field-uw-other-text').each(function () {
          $(this).hide();
        });

        // Show the small text field.
        $('.field--name-field-uw-ban-small-text').each(function () {
          $(this).show();
        });

        // If this is a split text overlay, hide the small text
        // and show the other text.
        if ($('select[name="settings[block_form][field_uw_text_overlay_style]"]').val() == 'split') {

          // Show the other text field.
          $('.field--name-field-uw-other-text').each(function () {
            $(this).show();
          });

          // Hide the small text field.
          $('.field--name-field-uw-ban-small-text').each(function () {
            $(this).hide();
          });
        }
      }
    }
  };
})(jQuery, Drupal);
