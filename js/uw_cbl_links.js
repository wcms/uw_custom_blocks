/**
 * @file
 */

(function ($, Drupal) {
  Drupal.behaviors.uw_cbl_links = {
    attach: function (context, settings) {
      $(document).ready(function () {

        // Run the settings for the links.
        linksSettings();

        // When the link style changes, run the changes to the settings.
        $('select[name="settings[link_style][style]"]').on('change', linksSettings);

        // Get the number of background types.
        var backgrounds = $('select[name*="[link_info][background]"]').length;

        // Step through each background types and check the settings.
        for(var i = 0; i < backgrounds; i++) {
          $('select[name="settings[items_fieldset][items][' + i + '][link_info][background]"]').on('change', linksSettings);
        }
      });

      // Function to set the links settings.
      function linksSettings() {

        // Get the link styles and the number of background types.
        var linkStyle = $('select[name="settings[link_style][style]"]').val();
        var backgrounds = $('select[name*="[link_info][background]"]').length;

        // Step through all the background types and set settings.
        for(var i = 0; i < backgrounds; i++) {

          // Get the specific background type.
          var backgroundType = $('select[name="settings[items_fieldset][items][' + i + '][link_info][background]"]').val();

          // Reset all the elements to hide and remove the form required.
          $('.form-item-settings-items-fieldset-items-' + i + '-link-info-background-color').hide();
          $('.form-item-settings-items-fieldset-items-' + i + '-link-info-image-link-coloring').hide();
          $('.form-item-settings-items-fieldset-items-' + i + '-link-info-link-coloring').hide();
          $('#background_image-media-library-wrapper-settings-items_fieldset-items-' + i + '-link_info').hide();
          $('#background_image-media-library-wrapper-settings-items_fieldset-items-' + i + '-link_info legend span').removeClass('form-required');

          // Provide the correct logic to show form elements.
          // See the document in ISTWCMS-6305 ticket.
          if (backgroundType == 'image') {

            $('#background_image-media-library-wrapper-settings-items_fieldset-items-' + i + '-link_info').show();
            $('#background_image-media-library-wrapper-settings-items_fieldset-items-' + i + '-link_info legend span').addClass('form-required');

            if (linkStyle == 'option') {
              $('.form-item-settings-items-fieldset-items-' + i + '-link-info-link-coloring').show();
            }
            else {
              $('.form-item-settings-items-fieldset-items-' + i + '-link-info-image-link-coloring').show();
            }
          }
          else if (backgroundType == 'color') {
            $('.form-item-settings-items-fieldset-items-' + i + '-link-info-background-color').show();
          }
          else {
            $('.form-item-settings-items-fieldset-items-' + i + '-link-info-link-coloring').show();
          }
        }
      }
    }
  };
})(jQuery, Drupal);
