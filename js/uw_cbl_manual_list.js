/**
 * @file
 */

(function ($, Drupal) {
  Drupal.behaviors.uw_cbl_manual_listing = {
    attach: function (context, settings) {
      $(document).ready(function () {

        // Add on change to select content type.
        $('select[name="settings[content_type]"]').on('change', resetValues);

        // Function to reset the values in the form.
        function resetValues() {

          // Get the content type from the form.
          var contentType = $('select[name="settings[content_type]"]').val();

          // Get all the content types.
          var contentTypes = $('select[name="settings[content_type]"] option').toArray().map(it => $(it).val());

          // Step through each of the content types and reset the values.
          for (i = 0; i < contentTypes.length; i++) {

            // Get the name of the content type.
            var name = contentTypes[i];

            // If this is not the current content type, then reset the values.
            if (name != contentType) {

              // Get the number of ids so we can reset them.
              var numIds = $('table[id*="edit-settings-' + name.replace('_', '-') + '-items-fieldset-' + name.replace('_', '-') + '-ids"] tr').length;

              // Step through the number if ids and reset them.
              for(j = 0; j < numIds; j++) {

                // If this is a show image field for profiles, reset it.
                if ($('select[name="settings[' + name + '][show_image]"]').length) {
                  $('select[name="settings[' + name + '][show_image]"]').val(1);
                }

                // If this is a show title field for contacts, reset it.
                if ($('select[name="settings[' + name + '][show_title]"]').length) {
                  $('select[name="settings[' + name + '][show_title]"]').val(1);
                }

                // If this is an entity id field, reset it.
                if ($('input[name="settings[' + name + '][items_fieldset][' + name + '_ids][' + j + '][id]"]').length) {
                  $('input[name="settings[' + name + '][items_fieldset][' + name + '_ids][' + j + '][id]"]').val('');
                }
              }
            }
          }
        }
      });
    }
  };
})(jQuery, Drupal);
