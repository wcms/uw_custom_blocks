/**
 * @file
 */

(function ($, Drupal) {
  Drupal.behaviors.uw_cbl_teaser = {
    attach: function (context, settings) {
      $(document).ready(function () {

        // Add on change to select content type.
        $('select[name="settings[content_type]"]').on('change', resetValues);

        // Function to reset the values in the form.
        function resetValues() {

          // Get the content type from the form.
          var contentType = $('select[name="settings[content_type]"]').val();

          // Get all the content types.
          var contentTypes = $('select[name="settings[content_type]"] option').toArray().map(it => $(it).val());

          // Step through each of the content types and reset the values.
          for (i = 0; i < contentTypes.length; i++) {

            // Get the name of the content type.
            var name = contentTypes[i];

            // If this is not the current content type, then reset the values.
            if (name != contentType) {

              // If this is the show image, reset it.
              if ($('select[name="settings[' + name + '][show_image]"]').length) {
                $('select[name="settings[' + name + '][show_image]"]').val(1);
              }

              // If this is the id, reset it.
              if ($('input[name="settings[' + name + '][nid]"]').length) {
                $('input[name="settings[' + name + '][nid]"]').val('');
              }
            }
          }
        }
      });
    }
  };
})(jQuery, Drupal);
