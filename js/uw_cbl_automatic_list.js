/**
 * @file
 */

(function ($, Drupal) {
  Drupal.behaviors.uw_cbl_automatic_listing = {
    attach: function (context, settings) {
      $(document).ready(function () {

        // Add on change to select content type.
        $('select[name="settings[content_type]"]').on('change', resetValues);

        // Function to reset the values in the form.
        function resetValues() {

          // Get the selected content type from the form.
          var selectedContentType = $('select[name="settings[content_type]"]').val();

          // Get all the content types.
          var options = $('select[name="settings[content_type]"] option').toArray().map(it => $(it).val());

          // Step through each of the options and reset the values.
          for (i = 0; i < options.length; i++) {

            // If there is a content type from the select list,
            // reset the values.
            if (options[i]) {

              // If this is not the selected content type, then reset the values.
              if (selectedContentType != options[i]) {

                // Reset the tags.
                if ($('input[name="settings[' + options[i] + '][tags]"]').length) {
                  $('input[name="settings[' + options[i] + '][tags]"]').val('');
                }

                // Reset the number of items.
                if ($('select[name="settings[' + options[i] + '][limit]"]').length) {
                  $('select[name="settings[' + options[i] + '][limit]"]').val(3);
                }

                // Reset the show all items.
                if ($('input[name="settings[' + options[i] + '][show_all_items]"]').length) {
                  $('input[name="settings[' + options[i] + '][show_all_items]"]').prop('checked', true);
                }

                // Reset the heading selector.
                if ($('select[name="settings[' + options[i] + '][heading_selector]"]').length) {
                  $('select[name="settings[' + options[i] + '][heading_selector]"]').val('h2');
                }

                // Reset the sort.
                if ($('select[name="settings[' + options[i] + '][sort]"]').length) {
                  if (options[i] !== 'event') {
                    $('select[name="settings[' + options[i] + '][sort]"]').val('alphabetic');
                  }
                  else {
                    $('select[name="settings[' + options[i] + '][sort]"]').val('ascending');
                  }
                }

                // Reset the show photos.
                if ($('select[name="settings[' + options[i] + '][show_photos]"]').length) {
                  $('select[name="settings[' + options[i] + '][show_photos]"]').val(1);
                }

                // Reset the date select box.
                // Trigger a change event so Drupal States update.
                if ($('select[name="settings[' + options[i] + '][date]"]').length) {
                  $('select[name="settings[' + options[i] + '][date]"]').val('now').change();
                }

                // Reset the date from box.
                if ($('input[name="settings[' + options[i] + '][range][from][date]"]').length) {
                  $('input[name="settings[' + options[i] + '][range][from][date]"]').val('');
                }

                // Reset the date to box.
                if ($('input[name="settings[' + options[i] + '][range][to][date]"]').length) {
                  $('input[name="settings[' + options[i] + '][range][to][date]"]').val('');
                }

                // Reset the date sort select box.
                if ($('select[name="settings[' + options[i] + '][range][sort]"]').length) {
                  $('select[name="settings[' + options[i] + '][range][sort]"]').val('ascending');
                }
              }
            }
          }

          // If this is not a event, reset the event type.
          if (selectedContentType != 'event') {
            $('select[name="settings[event][type][]"]').val('');
          }

          // If this is not a catalog item, reset the values.
          if (selectedContentType != 'catalog_item') {
            $('input[name="settings[catalog_item][catalog]"]').val('');
          }

          // If this is not a contact, reset the tags.
          if (selectedContentType != 'contact') {
            $('input[name="settings[contact][groups]"]').val('');
          }

          // If this is not a opportunity, reset the values.
          if (selectedContentType != 'opportunity') {
            $('select[name="settings[opportunity][type]"]').val('');
            $('select[name="settings[opportunity][employment]"]').val('');
            $('select[name="settings[opportunity][rate_of_pay]"]').val('');
          }

          // If this is not a profile, reset the tags.
          if (selectedContentType != 'profile') {
            $('input[name="settings[profile][types]"]').val('');
          }

          // If this is not a project, reset the tags.
          if (selectedContentType != 'project') {
            $('select[name="settings[project][role]"]').val('');
            $('select[name="settings[project][status]"]').val('');
            $('select[name="settings[project][topic]"]').val('');
          }

          // If this is not a pub ref, reset the values.
          if (selectedContentType != 'publication_reference') {
            $('input[name="settings[publication_reference][search]"]').val('');
            $('input[name="settings[publication_reference][keywords]"]').val('');
            $('div[id*="edit-settings-publication-reference-type-wrapper-type"] input').each(function (item, value) {
              $(this).prop('checked', false);
            });
            $('input[name="settings[publication_reference][year]"]').val('');
          }

          // If this is not a service, reset the categories.
          if (selectedContentType != 'service') {
            $('input[name="settings[service][categories]"]').val('');
          }
        }
      });
    }
  };
})(jQuery, Drupal);
