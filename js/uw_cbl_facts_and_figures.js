/**
 * @file
 */

(function ($, Drupal) {
  Drupal.behaviors.uw_cbl_facts_and_figures = {
    attach: function (context, settings) {
      $(document).ready(function () {

        if (typeof Drupal.tableDrag == 'undefined') {
          return;
        }
        for (var prop in Drupal.tableDrag) {
          if (prop.indexOf('uw-ffs-wrapper') !== -1) {
            var table = Drupal.tableDrag[prop];
            table.onDrop = function () {
              $('.tabledrag-changed-warning').hide();
            }
          }
        }
      });
    }
  };
})(jQuery, Drupal);
