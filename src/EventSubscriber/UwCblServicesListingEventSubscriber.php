<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block services listing event subscriber.
 */
class UwCblServicesListingEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Default constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager from core.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Preprocess blocks with copy text and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_services_list')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content')['#block_content'];

      // Get the services for this listing.
      $services = $block->field_uw_services->getValue();

      // If there are no terms, then it is all, so get them
      // from the vocab.
      if (empty($services)) {

        // Load all the terms from the service categories.
        $services = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree('uw_vocab_service_categories');
      }

      // Set the arguments to start of blank.
      $args = '';

      // Counter to check for + in arguments.
      $count = 0;

      // Step through each of the services and get the tid.
      foreach ($services as $service) {

        // If we have more than one service add the +.
        if ($count > 0) {
          $args = $args . '+';
        }

        // If services is an object it uses tid.
        // If services is not an object, it is coming from
        // the field so get the target_id.
        if (is_object($service)) {

          // Add the tid to the argument.
          $args = $args . $service->tid;
        }
        else {

          // Add the tid to the argument, which is the target_id.
          $args = $args . $service['target_id'];
        }

        // Increment the counter, so that we get proper +.
        $count++;
      }

      // Get the view, using the arguments from above.
      $view = views_embed_view('uw_view_service_show_nodes', 'services_specific_page', $args);

      // Set the render array for the services listing block..
      $build = [
        '#theme' => 'uw_block_services_list',
        '#view' => $view,
      ];

      // Set the content variables to our new render array.
      $variables->set('content', $build);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
