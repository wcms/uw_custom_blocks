<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block social intents event subscriber.
 */
class UwCblSocialIntentsEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    if ($this->checkLayoutBuilder($event, 'Social Intents')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for social intents.
      $form['#validate'][] = [$this, 'validateSocialIntents'];
    }
  }

  /**
   * Preprocess blocks with social intents and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_social_intents')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content')['#block_content'];

      // Set the variables for Social Intents.
      $social_intents = [
        'username' => $block->field_uw_si_username->value,
        'uniqueid' => $block->uuid(),
      ];

      // If there are mailchimps, assign them to the variables for the
      // template to use.
      if (isset($social_intents)) {

        // Set the render array for the mailchimp content.
        $build = [
          '#theme' => 'uw_block_social_intents',
          '#social_intents' => $social_intents,
        ];

        // Update the content variable to use our new build array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * Form validation for social intents.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateSocialIntents(array &$form, FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {
        if ($block['field_uw_si_username'][0]['value'] != '') {
          preg_match('/^[a-zA-Z0-9_-]+$/', $block['field_uw_si_username'][0]['value'], $matches);
          // If there is no valid Social Intents username entered, set error.
          if (empty($matches) || !$matches[0]) {
            $form_state->setErrorByName('settings][block_form][field_uw_si_username', 'You must enter a valid username for Social Intents.');
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'alterForm',
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
