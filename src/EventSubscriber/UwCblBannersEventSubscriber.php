<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\Service\UWServiceInterface;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Banner Event subscriber.
 *
 * @package Drupal\uw_custom_blocks\EventSubscriber
 */
class UwCblBannersEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file url generator.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * UW service.
   *
   * @var \Drupal\uw_cfg_common\Service\UWServiceInterface
   */
  protected $uwService;

  /**
   * Default constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileUrlGenerator $fileUrlGenerator
   *   The file url generator.
   * @param \Drupal\uw_cfg_common\Service\UWServiceInterface $uwService
   *   Custom UW service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    FileUrlGenerator $fileUrlGenerator,
    UWServiceInterface $uwService
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->uwService = $uwService;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

  /**
   * Preprocess banner block and send info to theme.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   Event for preprocess.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {
    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_banner_images')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content');

      $block_content = $block['#block_content'];

      // Set the variables for the blockquote.
      $banners = [];

      // Process banner images.
      foreach ($block_content->field_uw_banner_item as $banner_item) {
        $banner_para = $banner_item->entity;
        $type = $banner_para->getType();

        // Set the other text to null so that it displays
        // correctly on the block.
        $other_text = NULL;

        // If this is not a split type banner, use small text.
        // If it is split, small text is null and set the
        // other text.
        if ($block_content->field_uw_text_overlay_style->value !== 'split') {
          $small_text = $banner_para->field_uw_ban_small_text->value;
        }
        else {

          // Set small text to null.
          $small_text = NULL;

          // Ensure that other text exists in the paragraph.
          if ($banner_para->hasField('field_uw_other_text')) {

            // Get the other text.
            $other_text = $banner_para->field_uw_other_text->getValue();

            // If it is not empty, set the render array.  If it is
            // empty then set to null so it does not display.
            if (!empty($other_text)) {
              $other_text = [
                '#type' => 'processed_text',
                '#text' => $other_text[0]['value'],
                '#format' => $other_text[0]['format'],
              ];
            }
          }
        }

        $banner = [
          'type' => $type,
          'big_text' => $banner_para->field_uw_ban_big_text->value,
          'small_text' => $small_text,
          'other_text' => $other_text,
          'uuid' => uniqid(),
        ];

        // Link details needs to be created using for loop.
        foreach ($banner_para->field_uw_ban_link as $link_item) {
          $banner['link'] = $link_item->getUrl()->toString();
        }

        if ($type === 'uw_para_image_banner') {
          $banner['image'] = $this->uwService->prepareResponsiveImage($banner_para->field_uw_ban_image->entity, 'uw_ris_media');
        }
        elseif ($type === 'uw_para_local_video_banner') {
          $banner['image'] = $this->uwService->prepareResponsiveImage($banner_para->field_uw_ban_fallback_image->entity, 'uw_ris_media');

          // The block will not break when the uploaded video (mp4) is deleted.
          if (isset($banner_para->field_uw_ban_video->entity->field_media_file->entity)) {
            $file_uri = $banner_para->field_uw_ban_video->entity->field_media_file->entity->getFileUri();
            $banner['video'] = $this->fileUrlGenerator->generateAbsoluteString($file_uri);
          }
        }
        elseif ($type === 'uw_para_vimeo_video_banner') {
          $banner['image'] = $this->uwService->prepareResponsiveImage($banner_para->field_uw_ban_fallback_image->entity, 'uw_ris_media');

          // The block will not break when the uploaded vimeo is deleted.
          if (isset($banner_para->field_uw_ban_vimeo_video->entity->field_media_oembed_video)) {

            // Get the media entity.
            $media = $banner_para->field_uw_ban_vimeo_video->entity;

            // Set the video content to be the rendering of the media object.
            $banner['video'] = $this->entityTypeManager
              ->getViewBuilder('media')
              ->view($media, 'default');
          }
        }

        // If we are using an image, set the sources and the
        // img_element and remove the image array element.
        // Also add on the alt for the image.
        if (isset($banner['image']['sources'])) {
          $banner['sources'] = $banner['image']['responsive_sources'];
          $banner['img_element'] = $banner['image']['img_element']['#uri'];
          $banner['alt'] = $banner['image']['alt'];
          unset($banner['image']);
        }

        // If there is a tag, get the info.
        // If not set tag to null.
        if ($term = $banner_para->field_uw_faculty_school_banner->entity) {
          $banner['tag'][] = [
            'url' => NULL,
            'title' => uw_get_short_faculty_tag($term->label()),
            'faculty' => uw_get_org_class_from_term($term),
            'type' => 'full',
          ];
        }
        else {
          $banner['tag'] = NULL;
        }

        $images[] = $banner;
      }

      $banners = [
        'images' => $images,
        'autoplay' => $block_content->field_uw_autoplay->value,
        'slide_speed' => $block_content->field_uw_slide_speed->value,
        'style' => $block_content->field_uw_text_overlay_style->value,
        'text_overlay' => $block_content->field_uw_text_overlay_style->value,
        'transition_speed' => $block_content->field_uw_transition_speed->value,
        'uuid' => $block_content->uuid(),
      ];

      // If there are blockquotes, assign them to the variable for the template.
      if (isset($banners)) {

        // Set the render array for the blockquote content.
        $build = [
          '#theme' => 'uw_block_banner',
          '#banners' => $banners,
        ];

        // Set the content variables to our new render array.
        $variables->set('content', $build);
      }
    }
  }

}
