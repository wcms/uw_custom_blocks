<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\Service\UWServiceInterface;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Full width image block event subscriber.
 *
 * @package Drupal\uw_custom_blocks\EventSubscriber
 */
class UwCblFullWidthImageEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * UW service.
   *
   * @var \Drupal\uw_cfg_common\Service\UWServiceInterface
   */
  protected $uwService;

  /**
   * Default constructor.
   *
   * @param \Drupal\uw_cfg_common\Service\UWServiceInterface $uwService
   *   Custom UW service.
   */
  public function __construct(UWServiceInterface $uwService) {
    $this->uwService = $uwService;
  }

  /**
   * Preprocess blocks with full width image and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_image')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content')['#block_content'];

      if ($image = $block->field_uw_image->entity) {
        $output = [
          'image' => $this->uwService->prepareResponsiveImage($image, 'uw_ris_media'),
          'caption' => $image->field_uw_image_caption->value,
          'alt' => $image->field_media_image->alt,
        ];
      }

      // If there are images, update the content variable.
      if (isset($output)) {

        // Set the render array for the image gallery content.
        $build = [
          '#theme' => 'uw_block_image',
          '#image' => $output,
        ];

        // Update the content variable to use our new build array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
