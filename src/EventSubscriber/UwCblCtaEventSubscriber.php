<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block cta event subscriber.
 */
class UwCblCtaEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor for facts and figures class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Preprocess blocks with CTAs and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_call_to_action')) {

      // All cta will be placed here, starting with empty array.
      $ctas = [];
      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content');

      // Get the CTA blocks from the content variable.
      $cta_paragraphs = $block['#block_content']->field_uw_cta_details;

      // Iterate cta (could be more than one).
      foreach ($cta_paragraphs as $cta_paragraph) {
        $cta = [];
        $cta_entity = $cta_paragraph->entity;

        /** @var \Drupal\link\Plugin\Field\FieldType\LinkItem $link */
        foreach ($cta_entity->field_uw_cta_link as $link) {
          $cta['link'] = $link->getUrl()->toString();
        }

        $cta['theme'] = $cta_entity->field_uw_cta_theme->value;

        /** @var \Drupal\uw_custom_blocks\Plugin\Field\FieldType\CtaItem $cta_details */
        foreach ($cta_entity->field_uw_cta_text_details as $cta_details) {
          $type = $cta_details->getCtaStyle();

          if ($type === 'icon') {
            // Load the file entity using the target_id from the image field.
            /** @var \Drupal\file\FileInterface $file */
            $file = $this->entityTypeManager->getStorage('file')->load($cta_details->getCtaIcon());

            // Load in the image style that we are going to use.
            /** @var \Drupal\image\ImageStyleInterface $style */
            $style = $this->entityTypeManager->getStorage('image_style')->load('uw_is_icon');

            $cta['details'][] = [
              'icon' => $style->buildUrl($file->getFileUri()),
              'type' => $type,
            ];
          }
          else {
            $cta['details'][] = [
              'type' => $cta_details->getCtaStyle(),
              'text' => $cta_details->getCtaText(),
            ];
          }
        }
        $ctas[] = $cta;
      }

      // If ctas exist, assigned them theme and values.
      if (!empty($ctas)) {
        $build = [
          '#theme' => 'uw_block_cta',
          '#ctas' => $ctas,
        ];

        // Update the content variable to use our new build array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
