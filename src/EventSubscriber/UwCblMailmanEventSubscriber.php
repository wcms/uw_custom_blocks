<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block mailman event subscriber.
 */
class UwCblMailmanEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    if ($this->checkLayoutBuilder($event, 'Mailman')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for blockquote.
      $form['#validate'][] = [$this, 'validateMailman'];
    }
  }

  /**
   * Form validation for mailman.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateMailman(array &$form, FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {

        // If there is no mailman server selected, set error.
        if ($block['field_uw_mm_server'][0]['value'] == '') {

          // Set the form error for not having a mailman server.
          $form_state->setErrorByName('settings][block_form][field_uw_mm_server', 'You must select a mailman server.');
        }
        // If there is no mailman mailing list subscription URL entered,
        // set error.
        elseif ($block['field_uw_mm_servername'][0]['value'] == '') {

          // Set the form error for not having mailman mailing list
          // subscription URL.
          $form_state->setErrorByName('settings][block_form][field_uw_mm_servername', 'You must enter mailman mailing list subscription URL.');
        }
      }
    }
  }

  /**
   * Preprocess blocks with mailman and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_mailman')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content')['#block_content'];

      // Set the variables for the mailman.
      $mailman = [
        'server' => $block->field_uw_mm_server->value,
        'servername' => $block->field_uw_mm_servername->value,
        'uniqueid' => $block->uuid(),
      ];

      // If there are mailmans, assign them to the variables for the
      // template to use.
      if (isset($mailman)) {

        // Set the render array for the mailman content.
        $build = [
          '#theme' => 'uw_block_mailman',
          '#mailman' => $mailman,
        ];

        // Update the content variable to use our new build array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'alterForm',
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
