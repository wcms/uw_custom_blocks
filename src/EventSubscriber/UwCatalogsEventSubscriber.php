<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Cache\Cache;
use Drupal\core_event_dispatcher\EntityHookEvents;
use Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Catalog event subscriber.
 *
 * @package Drupal\uw_custom_blocks\EventSubscriber
 */
class UwCatalogsEventSubscriber implements EventSubscriberInterface {

  /**
   * If catalogs taxonomy term is updated, clear view caches and term cache.
   */
  public function catalogTaxonomyUpdate(EntityUpdateEvent $event) {
    $entity = $event->getEntity();

    if ($entity->getEntityTypeId() === 'taxonomy_term' && $entity->bundle() === 'uw_vocab_catalogs') {
      Cache::invalidateTags([
        'config:views.view.uw_view_catalog_show_terms',
        'config:views.view.uw_view_catalog_show_nodes',
        'taxonomy_term:' . $entity->id(),
      ]);
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      EntityHookEvents::ENTITY_UPDATE => 'catalogTaxonomyUpdate',
    ];
  }

}
