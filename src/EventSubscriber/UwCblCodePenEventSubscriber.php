<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblCodePenEventSubscriber.
 *
 * Adds Validation and Preprocess for CodePen block.
 */
class UwCblCodePenEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    if ($this->checkLayoutBuilder($event, 'CodePen')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for CodePen.
      $form['#validate'][] = [$this, 'uwCblCodePenValidation'];
    }
  }

  /**
   * Form validation for CodePen.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function uwCblCodePenValidation(array &$form, FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {
        // CodePen usernames can only contain letters, numbers, and dashes.
        // CodePen ID is assumed to always be 6 or 7 letters.
        // Display name and title can be anything.
        $codepen_username_regex = '/^[a-zA-Z0-9_-]+$/';
        $codepen_id_regex = '/^[a-zA-Z]{6,7}$/';

        // If there is an unsupported ID, set error.
        if (!empty($block['field_codepen_id'][0]['value']) && !preg_match($codepen_id_regex, $block['field_codepen_id'][0]['value'])) {
          $form_state->setErrorByName('settings][block_form][field_codepen_id', 'CodePen ID field contains invalid information (should be 6 or 7 letters only).');
        }

        // If there is an unsupported username, set error.
        if ($block['field_codepen_username'][0]['value'] == 'anon') {
          $form_state->setErrorByName('settings][block_form][field_codepen_username', 'CodePen does not allow embedding anonymous pens.');
        }
        elseif (!empty($block['field_codepen_username'][0]['value']) && !preg_match($codepen_username_regex, $block['field_codepen_username'][0]['value'])) {
          $form_state->setErrorByName('settings][block_form][field_codepen_username', 'CodePen username field contains invalid characters.');
        }
      }
    }
  }

  /**
   * Preprocess blocks with CodePen and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_codepen')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content')['#block_content'];

      // Variable for CodePen.
      $codepen['id'] = $block->field_codepen_id->value;
      $codepen['username'] = $block->field_codepen_username->value;
      $codepen['title'] = $block->field_codepen_displayed_title->value;
      $codepen['author'] = $block->field_codepen_displayed_author->value;
      $codepen['default_tabs'] = $block->field_codepen_default_tabs->value;
      $codepen['height'] = $block->field_codepen_pixel_height->value;

      // If there are CodePens, update the content variable.
      if (isset($codepen)) {

        // Set the render array for the CodePen content.
        $build = [
          '#theme' => 'uw_block_codepen',
          '#codepen' => $codepen,
        ];

        // Update the content variable to use our new build array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'alterForm',
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
