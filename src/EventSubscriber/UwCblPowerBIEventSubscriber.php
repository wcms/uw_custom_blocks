<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block power bi event subscriber.
 */
class UwCblPowerBIEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    if ($this->checkLayoutBuilder($event, 'PowerBI')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for powerbi.
      $form['#validate'][] = [$this, 'validatePowerbi'];
    }
  }

  /**
   * Form validation for powerbi.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validatePowerbi(array &$form, FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings');

    // If there are settings, continue to process.
    // If there is a block, continue to process.
    // Set the field error for having a text, but wrong powerbi URL pattern.
    if ($settings && ($block = $settings['block_form']) && $block['field_uw_powerbi_url'][0]['value'] !== '') {
      $powerbi_pattern = '/https\:\/\/app.powerbi.com/';
      if (!preg_match($powerbi_pattern, $block['field_uw_powerbi_url'][0]['value'], $matches)) {
        $form_state->setErrorByName(
        'settings][block_form][field_uw_powerbi_url',
         $this->t('Input does not match required pattern.')
        );
      }
    }
  }

  /**
   * Preprocess blocks with powerbi and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_powerbi')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load the block.
      $block = $variables->getByReference('content')['#block_content'];

      // Variable for powerbi.
      $powerbi['url'] = $block->field_uw_powerbi_url->value;

      // If there are powerbi, update the content variable.
      if (isset($powerbi)) {

        // Set the render array for the powerbi content.
        $build = [
          '#theme' => 'uw_block_powerbi',
          '#powerbi' => $powerbi,
        ];

        // Update the content variable to use our new build array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'alterForm',
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
