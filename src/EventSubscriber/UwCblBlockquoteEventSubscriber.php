<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block blockquote event subscriber.
 */
class UwCblBlockquoteEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Preprocess blocks with blockquote and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_blockquote')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content');

      // Set the variables for the blockquote.
      $blockquote = [
        'text' => $block['field_uw_bq_quote_text'][0],
        'attribution' => $block['field_uw_bq_quote_attribution'][0] ?? '',
      ];

      // If there are blockquotes, assign them to the variable for the template.
      if (isset($blockquote)) {

        // Set the render array for the blockquote content.
        $build = [
          '#theme' => 'uw_block_blockquote',
          '#blockquote' => $blockquote,
        ];

        // Set the content variables to our new render array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
