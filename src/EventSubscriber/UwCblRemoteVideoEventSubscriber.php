<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\Service\UWServiceInterface;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block remove video event subscriber.
 */
class UwCblRemoteVideoEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UW service.
   *
   * @var \Drupal\uw_cfg_common\Service\UWServiceInterface
   */
  protected $uwService;

  /**
   * Default constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager from core.
   * @param \Drupal\uw_cfg_common\Service\UWServiceInterface $uwService
   *   Custom UW service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, UWServiceInterface $uwService) {
    $this->entityTypeManager = $entityTypeManager;
    $this->uwService = $uwService;
  }

  /**
   * Preprocess blocks with remote video and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_remote_video')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content')['#block_content'];

      // Get the media object.
      $media = $block->field_uw_remote_video->entity;

      // Set the remote video URL.
      $remote_video['view']['url'] = $media->field_media_oembed_video->value;

      // Set the remote video title.
      $remote_video['view']['title'] = $media->label();

      // Check for YouTube.
      if (preg_match('/^https?:\/\/(?:(?:www\.)?youtube(?:-nocookie)?\.com\/(?:watch\?(?:\S+&)?v=|embed\/|.+#.+\/)|youtu\.be\/)(?:[\w\-]{11})(?:[#&?]\S*)?$/', $remote_video['view']['url'])) {

        // Set the type of video to YouTube.
        $remote_video['view']['type'] = 'YouTube';
      }
      // Check for YouTube playlist.
      elseif (preg_match('/^(https?:\/\/)?(www\.)?(youtube\.com|youtu\.?be)\/(playlist\?list=)([a-zA-Z0-9_-]+)$/', $remote_video['view']['url'])) {

        // Set the type of video to YouTube playlist.
        $remote_video['view']['type'] = 'YouTube playlist';
      }
      // Check for YouTube short.
      elseif (preg_match('/https?:\/\/(www\.)?youtube\.com\/shorts\/[a-zA-Z0-9_-]+/', $remote_video['view']['url'])) {

        // Set the type of video to YouTube short.
        $remote_video['view']['type'] = 'YouTube short';
      }
      // Check for Vimeo.
      elseif (strrpos($remote_video['view']['url'], 'vimeo')) {

        // Set the type of video to Vimeo.
        $remote_video['view']['type'] = 'Vimeo';
      }
      // Generic video.
      else {

        // Set the type of video to Generic.
        $remote_video['view']['type'] = 'Generic';
      }

      // Set the video content to be the rendering of the media object.
      $remote_video['video'] = $this->entityTypeManager->getViewBuilder('media')->view($media, 'default');

      // If there are remote videos, assign them to the variables for the
      // template to use.
      if (isset($remote_video)) {

        // Set the render array for the remote video content.
        $build = [
          '#theme' => 'uw_block_remote_video',
          '#remote_video' => $remote_video,
        ];

        // Update the content variable to use our new build array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
