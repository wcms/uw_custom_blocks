<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block tableau event subscriber.
 */
class UwCblTableauEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    if ($this->checkLayoutBuilder($event, 'Tableau')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for instagram.
      $form['#validate'][] = [$this, 'validateTableau'];
    }
  }

  /**
   * Form validation for tableau.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateTableau(array &$form, FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {

        // If there is no tableau server selected, set error.
        if ($block['field_uw_tbl_server'] == NULL) {

          // Set the form error for not having a tableau server selected.
          $form_state->setErrorByName('settings][block_form][field_uw_tbl_server', 'You must select a Tableau server.');
        }
        // If there is no site name entered, set error.
        elseif ($block['field_uw_tbl_site_name'][0]['value'] == '') {

          // Set the form error for not having a site name entered.
          $form_state->setErrorByName('settings][block_form][field_uw_tbl_site_name', 'You must enter a Tableau site name.');
        }
        // If there is no tableau name entered, set error.
        elseif ($block['field_uw_tbl_tableau_name'][0]['value'] == '') {

          // Set the form error for not having a site name entered.
          $form_state->setErrorByName('settings][block_form][field_uw_tbl_tableau_name', 'You must enter a Tableau name.');
        }
        // If there is no tableau height entered, set error.
        elseif ($block['field_uw_tbl_tableau_height'][0]['value'] == '') {

          // Set the form error for not having a tableau height entered.
          $form_state->setErrorByName('settings][block_form][field_uw_tbl_tableau_height', 'You must enter a Tableau height.');
        }
        // If there is no tableau display tabs selected, set error.
        elseif ($block['field_uw_tbl_display_tabs'] == NULL) {

          // Set the form error for not having a tableau display tabs selected.
          $form_state->setErrorByName('settings][block_form][field_uw_tbl_display_tabs', 'You must select a Tableau display tabs.');
        }
      }
    }
  }

  /**
   * Preprocess blocks with Tableaus and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {
    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_tableau')) {
      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content')['#block_content'];

      $host_url = $block->field_uw_tbl_server->value;
      if ($host_url === 'Private') {
        $host_url = 'https://iap-tableau.private.uwaterloo.ca/';
        $block->field_uw_tbl_site_name->value = '/t/' . $block->field_uw_tbl_site_name->value;
      }
      else {
        $host_url = 'https://public.tableau.com/';
      }

      $tableau = [
        'key' => $block->uuid(),
        'host_url' => $host_url,
        'tableau_site' => $block->field_uw_tbl_site_name->value,
        'url' => $block->field_uw_tbl_tableau_name->value,
        'tabs' => $block->field_uw_tbl_display_tabs->value === 'Yes' ? 'yes' : 'no',
        'height' => (int) $block->field_uw_tbl_tableau_height->value,
      ];

      // If there are tableaus, assign them to the variables for the template to
      // use.
      if (isset($tableau)) {
        // Set the render array for the Tableau content.
        $build = [
          '#theme' => 'uw_block_tableau',
          '#tableau' => $tableau,
        ];

        // Update the content variable to use our new build array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'alterForm',
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
