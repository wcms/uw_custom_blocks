<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Messenger\Messenger;
use Drupal\views\Views;

/**
 * UW custom block for related links.
 *
 * @package Drupal\uw_custom_blocks\EventSubscriber
 */
class UwCblRelatedLinksEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The variable for current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The variable for Drupal messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   The current path.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, CurrentPathStack $currentPath, Messenger $messenger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentPath = $currentPath;
    $this->messenger = $messenger;
  }

  /**
   * Preprocess block. Theme links as a list.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_related_links')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load the links.
      $links = $variables->getByReference('content')['#block_content']->get('field_uw_rl_related_link');

      // The items used for displaying related links.
      $items = [];

      // Step through each link and get title.
      foreach ($links->getIterator() as $link) {

        // Reset the title for every link, so that
        // we do not get duplicate titles.
        $title = '';

        // Get the URL from the link entity.
        $url = $link->getUrl();

        // If the link field has a title, we just use that.
        // If not, then we have to try and get the title.
        if ($link->getValue()['title']) {
          $title = $link->get('title')->getString();
        }
        else {

          // Ensure that the URL is routed first before trying to
          // get a title, or we will get a WSOD.  If url is not
          // routed, eventually it is going to just display the URL
          // as the title and a 404 will be shown when link is
          // clicked on.
          if ($url->isRouted()) {

            // Check if related link is a view, if so load view
            // and get title.  If not load entity and get title.
            if (preg_match('/^view\./', $url->getRouteName())) {

              // Get the parts of the route name, which include the
              // view id as the second element.
              $parts = explode('.', $url->getRouteName());

              // If we are able to load a view, then get the title.
              // If not just set the title to the default of Link.
              if ($view = Views::getView($parts[1])) {

                // Set the display of the view which is the third
                // element of the exploded route.
                $view->setDisplay($parts[2]);

                // Get the title from the view.
                $title = $view->getTitle();
              }
            }
            elseif ($url->getRouteName() == 'entity.node.canonical') {

              // Get the route parameters, so that we can look
              // and see if this has a title and that the route
              // actually exists.
              $params = $url->getRouteParameters();

              // Get the entity type from the route parameters.
              $entity_type = key($params);

              // Load in the entity.
              $entity = $this->entityTypeManager->getStorage($entity_type)->load($params[$entity_type]);

              // Use the entity for the title.
              $title = $entity->get('title')->getString();
            }
          }
        }

        // If there is no title, then we have to get it from the
        // URL, we are just going to blindly trust internal URLs
        // and it will throw a 404 if not found.  This will handle
        // even the cases where a path has changed for a node.
        if (!strlen($title)) {

          // If the URL is external, just set title to URL.
          // If not external, then set title to full path of internal,
          // regardless if URL exists or not.
          if ($url->isExternal()) {
            $title = $url->toString();
          }
          else {
            $title = Url::fromUserInput($url->toString(), ['absolute' => TRUE])->toString();
          }
        }

        // Create the related link and add to items array.
        $items[] = Link::fromTextAndUrl($title, $url);
      }

      // Only render if there are related links, if not
      // set build to null so nothing gets printed.
      if ($items) {
        $build = [
          '#theme' => 'item_list',
          '#items' => $items,
        ];
      }
      else {
        $build = NULL;
      }

      // Update the content variable to use our new build array.
      $variables->set('content', $build);
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
