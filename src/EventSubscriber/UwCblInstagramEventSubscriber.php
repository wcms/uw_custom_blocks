<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block instagram event subscriber.
 */
class UwCblInstagramEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    if ($this->checkLayoutBuilder($event, 'Instagram')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for instagram.
      $form['#validate'][] = [$this, 'validateInstagram'];
    }
  }

  /**
   * Form validation for instagram.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateInstagram(array &$form, FormStateInterface $form_state) {

    // Validation pattern for instagram url post/video.
    $pattern = '/^https?:\/\/(?:www\.)?instagram\.com\/(?:p|tv)\/(?:[^\/?#&]+).*/';

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {

        // If there is no text in the Instagram URL, set error.
        if ($block['field_uw_ing_url'][0]['value'] == '') {

          // Set the form error for not having a Instagram URL.
          $form_state->setErrorByName('settings][block_form][field_uw_ing_url', 'You must enter an Instagram URL.');
        }
        // Check if provided URL matches the pattern.
        elseif (preg_match($pattern, $block['field_uw_ing_url'][0]['value']) === FALSE) {

          // Set the form error for not matching required pattern.
          $form_state->setErrorByName('settings][block_form][field_uw_ing_url', 'You must use instagram post or video URL.');
        }
      }
    }
  }

  /**
   * Preprocess blocks with Instagram and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_instagram')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content')['#block_content'];

      // Variable for Instagram.
      $instagram['url'] = $block->field_uw_ing_url->value;

      // If there are instagrams, update the content variable.
      if (isset($instagram)) {

        // Set the render array for the instagram content.
        $build = [
          '#theme' => 'uw_block_instagram',
          '#instagram' => $instagram,
        ];

        // Update the content variable to use our new build array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'alterForm',
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
