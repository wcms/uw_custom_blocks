<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for widget form Link Url field.
 *
 * @package Drupal\uw_custom_blocks\EventSubscriber
 */
class UwWidgetFormLinkUrlDescriptionEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;
  // This is needed when passing $this to validation function. Without it,
  // there will be an error saying database can not be serialized.
  use DependencySerializationTrait;

  /**
   * Widget alter for Link field type.
   *
   * @param \Drupal\field_event_dispatcher\Event\Field\WidgetTypeFormAlterEvent $event
   *   Widget form alter event.
   */
  public function alterWidgetLinkField(WidgetTypeFormAlterEvent $event): void {
    $message = $this->t('Start typing the title of a piece of content to select it. You can also enter an internal path such as %internal-url or an external URL such as %url.',
      [
        '%internal-url' => '/blog',
        '%url' => 'https://example.com',
      ]
    );

    /** @var \Drupal\Core\Field\FieldItemListInterface $items */
    $items = $event->getContext()['items'];
    $fieldDefinition = $items->getFieldDefinition();

    // Is link text disabled(0), optional(1) or required(2).
    $link_text = $fieldDefinition->getSetting('title');

    // Field help text from field definition.
    $help_text = $fieldDefinition->getDescription();

    $element = &$event->getElement();

    // When link text is disabled, field's help text will be displayed beneath
    // url field. But if link text is optional or required, help text will be
    // displayed under link text (textfield) field.
    if (!$link_text && $help_text) {
      $element['uri']['#description']['#items'][1] = $message;
    }
    else {
      $element['uri']['#description'] = $message;
    }
  }

  /**
   * Alter contact node form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent $event
   *   The event.
   */
  public function alterNodeContactForm(FormIdAlterEvent $event): void {
    $form = &$event->getForm();
    $form['#validate'][] = [$this, 'validateContactForm'];
  }

  /**
   * Validation handler for Contact form.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state with values.
   */
  public function validateContactForm(array &$form, FormStateInterface $form_state) {
    $this->setFormsErrors([
      'field_uw_ct_contact_link_persona', 'field_uw_ct_contact_link_profile',
    ], $form_state);
  }

  /**
   * Set form errors if nolink is used.
   *
   * @param array $fields
   *   List of fields to check.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function setFormsErrors(array $fields, FormStateInterface $form_state) {
    foreach ($fields as $field) {
      $field_value = $form_state->getValue($field);

      if ($field_value && is_array($field_value)) {
        foreach ($field_value as $key => $value) {

          if (!empty($value['uri']) && $value['uri'] === 'route:<nolink>') {
            $form_state->setErrorByName($field . "][$key][uri",
              $this->t('Value &lt;nolink&gt; is not supported.'));
          }
        }
      }
    }
  }

  /**
   * Profile form alter.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent $event
   *   Dispatched event for form alter.
   */
  public function alterNodeProfileForm(FormIdAlterEvent $event): void {
    $form = &$event->getForm();
    $form['#validate'][] = [$this, 'validateProfileForm'];
  }

  /**
   * Validation handler for Profile form.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state with values.
   */
  public function validateProfileForm(array &$form, FormStateInterface $form_state) {
    $this->setFormsErrors([
      'field_uw_ct_profile_info_link', 'field_uw_ct_profile_link_persona',
    ], $form_state);
  }

  /**
   * Event form alter.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent $event
   *   Dispatched event for form alter.
   */
  public function alterNodeEventForm(FormIdAlterEvent $event): void {
    $form = &$event->getForm();
    $form['#validate'][] = [$this, 'validateEventForm'];
  }

  /**
   * Validation handler for Profile form.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state with values.
   */
  public function validateEventForm(array &$form, FormStateInterface $form_state) {
    // Allow <nolink> for event host field.
    if ($form_state->getValue('field_uw_event_host')[0]['uri'] != 'route:<nolink>') {
      $this->setFormsErrors(['field_uw_event_host'], $form_state);
    }
    $this->setFormsErrors(['field_uw_event_website'], $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    return [
      'hook_event_dispatcher.widget_link.alter' => 'alterWidgetLinkField',
      'hook_event_dispatcher.form_node_uw_ct_contact_form.alter' => 'alterNodeContactForm',
      'hook_event_dispatcher.form_node_uw_ct_contact_edit_form.alter' => 'alterNodeContactForm',
      'hook_event_dispatcher.form_node_uw_ct_profile_form.alter' => 'alterNodeProfileForm',
      'hook_event_dispatcher.form_node_uw_ct_profile_edit_form.alter' => 'alterNodeProfileForm',
      'hook_event_dispatcher.form_node_uw_ct_event_form.alter' => 'alterNodeEventForm',
      'hook_event_dispatcher.form_node_uw_ct_event_edit_form.alter' => 'alterNodeEventForm',
    ];
  }

}
