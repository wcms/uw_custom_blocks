<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block mailchimp event subscriber.
 */
class UwCblMailchimpEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    if ($this->checkLayoutBuilder($event, 'Mailchimp')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for mailchimp block.
      $form['#validate'][] = [$this, 'validateMailchimp'];

    }
  }

  /**
   * Form validation for mailchimp.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateMailchimp(array &$form, FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {
      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {

        if ($block['field_uw_mailchimp'][0]['value'] != '') {
          // Decodes all HTML entities including numerical
          // ones to regular UTF-8 bytes.
          $text = Html::decodeEntities(trim($block['field_uw_mailchimp'][0]['value']));
          $correct_action = FALSE;
          $correct_end = FALSE;
          // Check for the only one "<!End mc_embed_signup>" is the end of the
          // pasted text.
          if (preg_match('/(<!--End mc_embed_signup-->)$/', $text)) {
            $correct_end = TRUE;
          }

          // Check for the only one form action which contains
          // .list-manage.com/subscribe/post.
          if (preg_match_all('/(action(\\s*)=(\\s*)([\"\'])?(?(1)(.*?)\\2|([^\s\>]+)))/', $text, $matches)) {
            if (count($matches[0]) == 1 && (strpos($matches[0][0], ".list-manage.com/subscribe/post") !== FALSE)) {
              $correct_action = TRUE;
            }
          }
          // Remove unuseful tags after making sure that Begin,
          // End and form action are correct for MailChimp source code format.
          if (strcasecmp(strtok($text, "\n"), "<!-- Begin MailChimp Signup Form -->") == 1 && $correct_end && $correct_action) {
            $filter_text = preg_replace(
              [
                '#<script(.*?)>(.*?)</script>#is',
                '/<style\\b[^>]*>(.*?)<\\/style>/s',
                '/(<link\b.+href=")(?!http)([^"]*)(".*>)/',
                '%<object.+?</object>%is',
                '@<head[^>]*?>.*?</head>@siu',
                '@<embed[^>]*?.*?</embed>@siu',
                '@<applet[^>]*?.*?</applet>@siu',
                '@<noframes[^>]*?.*?</noframes>@siu',
              ], '', $text);

            // Set the filter text as field_uw_mailchimp value.
            $settings['block_form']['field_uw_mailchimp'][0]['value'] = $filter_text;
            $form_state->setValue('settings', $settings);
          }
          else {
            $form_state->setErrorByName('settings][block_form][field_uw_mailchimp', 'Incorrect MailChimp source code has been entered.');
          }
        }
      }
    }
  }

  /**
   * Preprocess blocks with mailchimp and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_mailchimp')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content')['#block_content'];

      // Set the variables for the mailchimp.
      $mailchimp = [
        'sourcecode' => $block->field_uw_mailchimp->value,
        'uniqueid' => $block->uuid(),
      ];

      // If there are mailchimps, assign them to the variables for the
      // template to use.
      if (isset($mailchimp)) {

        // Set the render array for the mailchimp content.
        $build = [
          '#theme' => 'uw_block_mailchimp',
          '#mailchimp' => $mailchimp,
        ];

        // Update the content variable to use our new build array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'alterForm',
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
