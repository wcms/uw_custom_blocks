<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\Service\UWServiceInterface;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block image gallery event subscriber.
 */
class UwCblImageGalleryEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  use DependencySerializationTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UW service.
   *
   * @var \Drupal\uw_cfg_common\Service\UWServiceInterface
   */
  protected $uwService;

  /**
   * File URL generation.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Default constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager from core.
   * @param \Drupal\uw_cfg_common\Service\UWServiceInterface $uwService
   *   Custom UW service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   File URL Generator.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    UWServiceInterface $uwService,
    FileUrlGeneratorInterface $fileUrlGenerator
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->uwService = $uwService;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * Preprocess blocks with image gallery and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_image_gallery')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content')['#block_content'];

      // Get the type of image gallery.
      $type_of_gallery = $block->field_uw_type_of_gallery->value;

      // Add the id to the variable.
      $images['id'] = $block->uuid();

      // Get the media object.
      $mids = $block->field_uw_ig_images->getValue('list');

      // Step through each of the media ids and get out variables.
      foreach ($mids as $mid) {

        // Load in the media entity.
        $entity = $this->entityTypeManager->getStorage('media')->load($mid['target_id']);

        // Ensure that there is an entity, before adding the image.
        // This takes care when someone deletes the media but not
        // from the image galley block.
        if ($entity) {

          // Get the fid.
          $fid = $entity->field_media_image->entity->fid->value;

          // Load file.
          $file = $this->entityTypeManager->getStorage('file')->load($fid);

          // Get origin image URI.
          $image_uri = $file->getFileUri();

          // Get the image based on choice of image gallery.
          if ($type_of_gallery == 'slider' || $type_of_gallery == NULL) {

            // Set the variables for the image.
            $images['images'][] = [
              'image' => $this->uwService->prepareResponsiveImage($entity, 'uw_ris_image_gallery'),
              'caption' => $entity->field_uw_image_caption->value,
              'original' => $this->fileUrlGenerator->generateAbsoluteString($image_uri),
            ];
          }
          else {

            // Load image style iamge gallery square image style.
            $style = $this->entityTypeManager->getStorage('image_style')
              ->load('uw_is_image_gallery_square');

            // Get URL.
            $url = $style->buildUrl($image_uri);

            $images['images'][] = [
              'image' => [
                'thumbnail' => $url,
                'original' => $this->fileUrlGenerator->generateAbsoluteString($image_uri),
              ],
              'caption' => $entity->field_uw_image_caption->value,
              'alt' => $entity->field_media_image->alt,
            ];
          }
        }
      }

      // If there are images, update the content variable.
      if (isset($images)) {

        // Set the image gallery fields.
        $image_gallery['type'] = $block->field_uw_type_of_gallery->value;
        $image_gallery['images'] = $images;
        $image_gallery['caption_format'] = $block->field_uw_caption_format->value ?? 'underneath_photo';
        $image_gallery['caption_alignment'] = $block->field_uw_caption_alignment->value ?? 'left';

        if ($image_gallery['type'] === 'slider') {
          $image_gallery['nav'] = $block->field_uw_gallery_nav_options->value;
          $image_gallery['display_images'] = $block->field_uw_gallery_num->value;
        }

        // Set the render array for the image gallery content.
        $build = [
          '#theme' => 'uw_block_image_gallery',
          '#image_gallery' => $image_gallery,
        ];

        // Update the content variable to use our new build array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'alterForm',
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {
    // Check that we are on a layout builder add/update.
    if ($this->checkLayoutBuilder($event, 'Image gallery')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the custom form alter to the layout builder.
      $form['settings']['block_form']['#process'][] = [
        $this, 'processDefaultType',
      ];
    }
  }

  /**
   * Get the layout object.
   *
   * @param array $element
   *   Form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The element.
   */
  public function processDefaultType(array $element, FormStateInterface $form_state) {

    // Add states based on gallery type selected. This will hide/show
    // additional fields when slider type is selected.
    $element['field_uw_gallery_num']['widget']['#states'] = [
      'invisible' => [
        ['select[name="settings[block_form][field_uw_type_of_gallery]"]' => ['value' => 'grid']],
      ],
    ];

    $element['field_uw_gallery_nav_options']['widget']['#states'] = [
      'invisible' => [
        ['select[name="settings[block_form][field_uw_type_of_gallery]' => ['value' => 'grid']],
      ],
    ];
    $element['field_uw_caption_alignment']['widget']['#states'] = [
      'invisible' => [
        ['select[name="settings[block_form][field_uw_caption_format]"]' => ['value' => 'hidden']],
      ],
    ];

    return $element;
  }

}
