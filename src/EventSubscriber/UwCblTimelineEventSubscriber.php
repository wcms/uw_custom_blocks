<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\Service\UWServiceInterface;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block timeline event subscriber.
 */
class UwCblTimelineEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UW service.
   *
   * @var \Drupal\uw_cfg_common\Service\UWServiceInterface
   */
  protected $uwService;

  /**
   * Constructor for facts and figures class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\uw_cfg_common\Service\UWServiceInterface $uwService
   *   Custom UW service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    UWServiceInterface $uwService
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->uwService = $uwService;
  }

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    if ($this->checkLayoutBuilder($event, 'Timeline')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for instagram.
      $form['#validate'][] = 'Drupal\uw_custom_blocks\EventSubscriber\UwCblTimelineEventSubscriber::validateTimeline';
    }
  }

  /**
   * Form validation for timeline.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateTimeline(array &$form, FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {

        // Variable to break out of loops.
        $error_is_set = FALSE;

        // Step through each of the timeline and check for text.
        foreach ($block['field_uw_timeline'] as $key => $timeline) {
          // Ensure that we are on an actual timeline.
          if (is_int($key)) {
            $item = $key + 1;
            // If both headline and content are empty, set the form error.
            if (($timeline['subform']['field_uw_timeline_headline'][0]['value'] == '') && ($timeline['subform']['field_uw_timeline_content'][0]['value'] == '')) {
              // Set the element to have the error.
              $element = 'settings][block_form][field_uw_timeline][' . $key . '][subform][field_uw_timeline_headline][0][value';
              $form_state->setErrorByName($element, 'Timeline ' . $item . ' cannot have both of the "Headline" and "Content" fields empty.');
              $error_is_set = TRUE;
            }
          }

          // If there is an error already set, break out of the loop.
          if ($error_is_set) {
            break;
          }
        }
      }
    }
  }

  /**
   * Preprocess blocks with copy text and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_timeline')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content');

      // Get the type of sort for this timeline.
      $sort = $block['#block_content']->field_uw_timeline_sort->value;

      // Get the timeline field, that contains all the entries
      // for the timeline.
      $timeline = $block['#block_content']->field_uw_timeline;

      // Load the image style once. Avoiding loading style for every paragraph.
      $style = $this->entityTypeManager->getStorage('image_style')->load('thumbnail');

      // Step through each timeline entry and get variables.
      foreach ($timeline as $t) {

        // Get the entity for the timeline field.
        $entity = $t->entity;

        // Get the date parts, separated into year, month and day.
        $date = explode('-', $entity->field_uw_timeline_date->value);

        // Set the year.
        $year = $date[0];

        // Set the month, dropping leading zero if any.
        $month = $date[1][0] == 0 ? $date[1][1] : $date[1];

        // Set the day, dropping the leading zero if any.
        $day = $date[2][0] == 0 ? $date[2][1] : $date[2];

        // Set the image variable to null, in case there is no
        // image, and we get no image in the loop.
        $image = NULL;
        $image_alt = NULL;

        // If there is a entity, meaning that there is a file, then process it
        // and set the variables.
        if ($media = $entity->field_uw_timeline_photo->entity) {

          // Get image entity from the media object.
          $file = $media->field_media_image->entity;

          // Set the image variable to the location of the image with the image
          // style applied.
          $image = $style->buildUrl($file->getFileUri());

          // Get the alt text for the image.
          $image_alt = $media->field_media_image->alt;
        }

        // Get the link from the entity.
        $link = $entity->field_uw_timeline_link->getValue();

        // Ensure that it has a value and set the variable.
        if (isset($link[0]['uri'])) {
          $link = Url::fromUri($link[0]['uri'])->toString();
        }

        // Set the items array, look at the gesso yml for
        // timeline items array setup.
        $items[$year][$month][$day][] = [
          'headline' => $entity->field_uw_timeline_headline->value,
          'image' => $image,
          'image_alt' => $image_alt,
          'link' => $link,
          'content' => [
            '#type' => 'processed_text',
            '#text' => $entity->field_uw_timeline_content->value ?? '',
            '#format' => 'uw_tf_standard',
          ],
        ];
      }

      // Sort the items if needed.
      if ($sort == 'asc') {
        // Sort the items ascending if selected.
        $this->uwService->ksortRecursive($items);
      }
      elseif ($sort == 'desc') {
        // Sort the items descending if selected.
        $this->uwService->krsortRecursive($items);
      }

      // Get the style, removing the leading vertical-.
      $style = str_replace(
        'vertical-',
        '',
        $block['#block_content']->field_uw_timeline_style->value
      );

      // Set the variable for timeline to be passed to
      // the template.
      $timeline = [
        'style' => $style,
        'items' => $items,
      ];

      // Set the render array for the timeline content.
      $build = [
        '#theme' => 'uw_block_timeline',
        '#timeline' => $timeline,
      ];

      // Set the content variables to our new render array.
      $variables->set('content', $build);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'alterForm',
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
