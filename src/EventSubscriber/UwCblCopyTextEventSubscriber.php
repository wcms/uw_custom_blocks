<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block copy text event subscriber.
 */
class UwCblCopyTextEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  /**
   * Preprocess blocks with copy text and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_copy_text')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content');

      // Set the render array for the copy text content.
      $build = [
        '#theme' => 'uw_block_copy_text',
        '#copy_text' => $block['field_uw_copy_text'][0],
      ];

      // Set the content variables to our new render array.
      $variables->set('content', $build);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
      FormHookEvents::FORM_ALTER => 'alterForm',
    ];
  }

  /**
   * Alter form.
   *
   * As per https://www.drupal.org/project/drupal/issues/3095304
   * adding additional js library to the form. Used comment #27 as solution.
   * Once fixed in Drupal core, this should be removed. Don't forget to remove
   * library entry and js file.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    if ($this->checkLayoutBuilder($event, 'Copy text')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add library to the form.
      $form['#attached']['library'][] = 'uw_custom_blocks/uw_cbl_copy_text';
    }
  }

}
