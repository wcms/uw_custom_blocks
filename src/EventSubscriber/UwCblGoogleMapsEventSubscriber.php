<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block power bi event subscriber.
 */
class UwCblGoogleMapsEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  const GOOGLE_MAP_PATTERN = 'https\:\/\/www\.google\.com\/maps\/embed\?pb=';
  const GOOGLE_MY_MAP_PATTERN = 'https\:\/\/www\.google\.com\/maps\/d\/u\/1\/embed\?mid=';

  use StringTranslationTrait;

  /**
   * Alter form.
   *
   * Adds Extra Validation.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {
    if ($this->checkLayoutBuilder($event, 'Google Maps')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for google maps.
      $form['#validate'][] = [$this, 'validateGoogleMaps'];
    }
  }

  /**
   * Form validation for google maps.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateGoogleMaps(array &$form, FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {

        // If there is no text in the google maps URL, set error.
        if ($block['field_gmaps_embedded_url'][0]['uri'] == '') {

          // Set the form error for not having a Google Maps or
          // Google My Maps URL.
          $form_state->setErrorByName(
            'settings][block_form][field_uw_gmaps_embedded_url',
            $this->t('You must enter a Google Maps or Google My Maps URL.')
          );
        }

        $combined_pattern = '/^(' . self::GOOGLE_MY_MAP_PATTERN . '|' . self::GOOGLE_MAP_PATTERN . ')/';
        if (!preg_match($combined_pattern, $block['field_gmaps_embedded_url'][0]['uri'], $matches)) {
          $form_state->setErrorByName(
            'settings][block_form][field_uw_gmaps_embedded_url',
            $this->t('Input does not match required pattern.')
          );
        }
      }
    }
  }

  /**
   * Preprocess blocks with google maps and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_google_maps')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load the block.
      $block = $variables->getByReference('content')['#block_content'];

      // Variable for google maps.
      $google_maps['height'] = $block->field_gmaps_height->value;
      $google_maps['url'] = $block->field_gmaps_embedded_url->uri;

      if (preg_match('/^' . self::GOOGLE_MAP_PATTERN . '/', $google_maps['url'], $matches)) {
        $google_maps['type'] = 'Google Maps';
      }
      elseif (preg_match('/^' . self::GOOGLE_MY_MAP_PATTERN . '/', $google_maps['url'], $matches)) {
        $google_maps['type'] = 'Google My Maps';
      }
      else {
        $google_maps['type'] = 'unknown';
      }

      // If there are google maps, update the content variable.
      if (isset($google_maps)) {

        // Set the render array for the google maps content.
        $build = [
          '#theme' => 'uw_block_google_maps',
          '#google_maps' => $google_maps,
        ];

        // Update the content variable to use our new build array.
        $variables->set('content', $build);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'alterForm',
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

}
