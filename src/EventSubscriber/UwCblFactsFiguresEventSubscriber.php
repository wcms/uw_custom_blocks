<?php

namespace Drupal\uw_custom_blocks\EventSubscriber;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\FormHookEvents;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * UW custom block fact and figures event subscriber.
 */
class UwCblFactsFiguresEventSubscriber extends UwCblBase implements EventSubscriberInterface {

  use DependencySerializationTrait;

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * File url generator object.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * Constructor for facts and figures class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\Core\File\FileUrlGenerator $fileUrlGenerator
   *   File url generator object.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, FileUrlGenerator $fileUrlGenerator) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * Preprocess blocks with CTAs and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_facts_and_figures')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block.
      $block = $variables->getByReference('content');

      // The counter used for the number of FF text.
      $text_details_count = 0;

      // Get the attributes from variables.
      $attributes = $variables->get('attributes');

      // Get the classes from attributes.
      $classes = $attributes['class'] ?? NULL;

      // If there are classes in the attributes, check for carousel settings.
      if (!empty($classes)) {

        // These are the classes to check for.
        $classes_to_check = [
          'uw-carousel--one-item',
          'uw-carousel--two-items',
          'uw-carousel--three-items',
          'uw-carousel--four-items',
        ];

        // Step through each of the classes in attributes and see if we have
        // any carousel settings and set a variable if we do.
        foreach ($classes as $class) {
          if (in_array($class, $classes_to_check)) {
            switch ($class) {
              case 'uw-carousel--one-item':
                $num_per_carousel = 1;
                break;

              case 'uw-carousel--two-items':
                $num_per_carousel = 2;
                break;

              case 'uw-carousel--four-items':
                $num_per_carousel = 4;
                break;

              case 'uw-carousel--three-items':
                $num_per_carousel = 3;
                break;

              default:
                $num_per_carousel = 0;
                break;
            }
          }
        }
      }

      // Get the FF blocks from the content variable.
      $fact_figure = $block['#block_content']->field_uw_ff_fact_figure->entity;

      // The uuid of the paragraph to use as a unique identifier.
      $ffs['id'] = $fact_figure->uuid();

      // The background color for the FF.
      $ffs['bg_colour'] = $fact_figure->field_uw_ff_dbg_color->value;

      // The default colour of the FF.
      $ffs['theme'] = $fact_figure->field_uw_ff_def_color->value;

      // The show bubbles of the FF.
      $ffs['show'] = $fact_figure->field_uw_ff_show_bubbles->value;

      // The text align of the FF.
      $ffs['text_align'] = $fact_figure->field_uw_ff_textalign->value;

      // The number of FFs to show in the carousel, if not defined use 1.
      $ffs['num_per_carousel'] = $num_per_carousel ?? 0;

      // Get the details about the FF.
      $ff_details = $fact_figure->field_uw_fact_figure;

      // Step through each detail of the FF and get the content.
      foreach ($ff_details as $ff_detail) {

        // Get paragraph entity from details.
        $entity = $ff_detail->entity;

        // Get the actual FF info.
        $ff_info = $entity->field_uw_ff_info;

        // Load in the image style that we are going to use.
        /** @var \Drupal\image\ImageStyleInterface $style */
        $style = $this->entityTypeManager->getStorage('image_style')->load('thumbnail');

        // Load the media entity storage.
        $media_storage = $this->entityTypeManager->getStorage('media');

        // Step through each info and get out all the text details.
        /** @var \Drupal\uw_custom_blocks\Plugin\Field\FieldType\FactTextItem $info */
        foreach ($ff_info as $info) {

          // Get the type of text (big, medium, small, icon).
          $text_type = $info->getFactStyle();

          // If the text type is icon, load icon, if not use type of text.
          if ($text_type === 'icon') {

            // Load the media entity using the target_id from the image field.
            /** @var \Drupal\media\MediaInterface $media */
            $media = $media_storage->load($info->getFactIcon());

            /** @var \Drupal\file\FileInterface $file */
            $file = $media->field_media_image_1->entity;

            // Make sure file/media exists before use.
            if ($file) {
              // Set the text details with the icon.
              $file_details = $file->toArray();
              if ($file_details['filemime'][0]['value'] !== 'image/svg+xml') {
                $ffs_details[$text_details_count][] = [
                  // Build the URL using the image style and file uri.
                  'alt' => $media->field_media_image_1->alt,
                  'icon' => $style->buildUrl($file->getFileUri()),
                  'type' => $text_type,
                ];
              }
              else {
                $ffs_details[$text_details_count][] = [
                  // Build the URL without the image style but with file uri
                  // because svg image cannot use image styles.
                  // Note: this will display svg at the size specified in svg.
                  'alt' => $media->field_media_image_1->alt,
                  'icon' => $this->fileUrlGenerator->generate($file->getFileUri()),
                  'type' => $text_type,
                ];
              }
            }
          }
          else {

            // Get the values of the text.
            $ffs_details[$text_details_count][] = [
              'text' => $info->getFactText(),
              'type' => $text_type,
              'info_type' => $info->getFactInfographicsType(),
              'info_text' => $info->getFactInfographicsTextTransformed(),
              'info_suffix' => $info->getFactInfographicsTextSuffix(),
            ];
          }
        }

        // Create render array for caption, and let twig renders it. This way
        // rich text can be used without twig raw filter.
        if ($caption = $entity->field_uw_ff_caption) {
          // Only do when caption has content,
          // since TWIG can't seem to detect this is blank.
          if ($caption->value) {
            $ffs_details[$text_details_count][] = [
              'type' => 'caption',
              'caption' => [
                '#type' => 'processed_text',
                '#format' => $caption->format,
                '#text' => $caption->value,
              ],
            ];
          }
        }

        // Increment the text details counter.
        $text_details_count++;
      }
    }

    // If there are ff details add the ff detail variable.
    if (isset($ffs_details)) {
      $ffs['details'] = $ffs_details;
    }

    // If there are FFs, assign them to the variables for the template to use.
    if (isset($ffs)) {

      // Set the render array for the ff content.
      $build = [
        '#theme' => 'uw_block_facts_figures',
        '#ffs' => $ffs,
      ];

      // Update the content variable to use our new build array.
      $variables->set('content', $build);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      FormHookEvents::FORM_ALTER => 'alterForm',
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    // Check that we are on a layout builder add/update.
    if ($this->checkLayoutBuilder($event, 'Facts and figures')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the custom form alter to the layout builder.
      $form['settings']['block_form']['#process'][] = [
        $this, 'processDefaultColour',
      ];
    }
  }

  /**
   * Get the layout object.
   *
   * @param array $element
   *   Form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The element.
   */
  public function processDefaultColour(array $element, FormStateInterface $form_state) {
    // The default background colour (field_uw_ff_dbg_color) is gold,
    // hide default colour field (field_uw_ff_def_color).
    $element['field_uw_ff_fact_figure']['widget'][0]['subform']['field_uw_ff_def_color']['#states'] = [
      'invisible' => [
        ['select[name="settings[block_form][field_uw_ff_fact_figure][0][subform][field_uw_ff_dbg_color]"]' => ['value' => 'gold']],
      ],
    ];
    return $element;
  }

}
