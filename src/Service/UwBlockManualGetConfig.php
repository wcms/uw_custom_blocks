<?php

namespace Drupal\uw_custom_blocks\Service;

/**
 * Class UwBlockManualGetConfig.
 *
 * UW Service that gets config from form state.
 *
 * @package Drupal\uw_custom_common\Service
 */
class UwBlockManualGetConfig {

  /**
   * UW block service.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockService
   */
  protected $uwBlockService;

  /**
   * Default constructor.
   *
   * @param \Drupal\uw_custom_blocks\Service\UwBlockService $uwBlockService
   *   The UW block service.
   */
  public function __construct(
    UwBlockService $uwBlockService
  ) {

    $this->uwBlockService = $uwBlockService;
  }

  /**
   * Function to get the selected config.
   *
   * @param string $content_type
   *   The content type.
   * @param array $values
   *   The array of form state values.
   *
   * @return array
   *   Array of selected config.
   */
  public function getConfig(
    string $content_type,
    array $values
  ): array {

    // Get the content type from the values.
    $content_type = $values['content_type'];

    // Get ids name.
    $ids_name = $content_type . '_ids';

    // Get the ids.
    $ids = $values[$content_type]['items_fieldset'][$ids_name];

    // Sort the ids by weight.
    $ids = uw_sort_array_by_weight($ids);

    // Get the contact ids as a sequential array.
    $ids = $this->uwBlockService->getNodeIds($ids);

    // Set the configuration for block.
    $config['num_' . $ids_name] = count($ids);
    $config['show_title'] = $values[$content_type]['show_title'] ?? 0;
    $config['show_image'] = $values[$content_type]['show_image'] ?? 0;
    $config[$ids_name] = $ids;

    // If this is a catalog item, then store in_catalog into the config.
    if ($content_type == 'catalog_item') {
      $config['in_catalog'] = $values[$content_type]['in_catalog'] ?? 0;
    }
    // If this is an event, then store the style into the config.
    if ($content_type == 'event') {
      $config['style'] = $values[$content_type]['style'];
      $config['sort_order'] = $values[$content_type]['sort_order'];
    }

    return $config;
  }

}
