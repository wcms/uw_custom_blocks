<?php

namespace Drupal\uw_custom_blocks\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class UwBlockAutoElements.
 *
 * UW Service that gets form elements for automatic blocks.
 *
 * @package Drupal\uw_custom_common\Service
 */
class UwBlockAutoElements {

  use StringTranslationTrait;

  /**
   * UW block service.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockService
   */
  protected $uwBlockService;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Default constructor.
   *
   * @param \Drupal\uw_custom_blocks\Service\UwBlockService $uwBlockService
   *   The UW block service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    UwBlockService $uwBlockService,
    EntityTypeManagerInterface $entityTypeManager
  ) {

    $this->uwBlockService = $uwBlockService;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Function to get all the form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   */
  public function getAutoFormElements(
    array &$form,
    array $config
  ): void {

    // Get all the form elements.
    $this->getBlogElements($form, $config);
    $this->getCatalogElements($form, $config);
    $this->getCatalogItemElements($form, $config);
    $this->getContactElements($form, $config);
    $this->getEventElements($form, $config);
    $this->getNewsItemElements($form, $config);
    $this->getOpportunityElements($form, $config);
    $this->getProfileElements($form, $config);
    $this->getProjectElements($form, $config);
    $this->getPublicationReferenceElements($form, $config);
    $this->getServiceElements($form, $config);
  }

  /**
   * Function to get the blog form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   */
  private function getBlogElements(
    array &$form,
    array $config
  ): void {

    // The contacts container for element.
    $form['blog'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Blog list settings'),
      '#states' => [
        'visible' => [
          'select[name="settings[content_type]"]' => [
            'value' => 'blog',
          ],
        ],
      ],
    ];

    // The number of contacts to show form element.
    $form['blog']['limit'] = $this->uwBlockService->getLimitPerBlockFormElement(
      'blog',
      $config
    );

    // The groups form element.
    $form['blog']['tags'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Blog tag',
      $config['blog'] ?? [],
      'uw_vocab_blog_tags',
      'tags'
    );

    // Add the all items to the form.
    $this->addAllItemsToForm(
      'blog',
      isset($config['blog']) ? $config['blog'] : [],
      $form
    );
  }

  /**
   * Function to get the catalog form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   */
  private function getCatalogElements(
    array &$form,
    array $config
  ): void {

    // The catalog container for element.
    $form['catalog'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Catalog list settings'),
      '#states' => [
        'visible' => [
          'select[name="settings[content_type]"]' => [
            'value' => 'catalog',
          ],
        ],
      ],
    ];

    // The heading selector element.
    $form['catalog']['heading_selector'] = [
      '#title' => $this->t('Heading selector'),
      '#type' => 'select',
      '#options' => [
        'h2' => $this->t('h2'),
        'h3' => $this->t('h3'),
        'h4' => $this->t('h4'),
        'h5' => $this->t('h5'),
        'h6' => $this->t('h6'),
      ],
      '#default_value' => $config['catalog']['heading_selector'] ?? '',
      '#required' => TRUE,
      '#description' => $this->t('Select a heading level to be applied to the names of the catalogs. This should be one level lower than the preceding header. For example, if the block title (an h2) is set to display, this should be set to h3.'),
    ];
  }

  /**
   * Function to get the catalog items form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   */
  private function getCatalogItemElements(
    array &$form,
    array $config
  ): void {

    // The contacts container for element.
    $form['catalog_item'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Catalog item list settings'),
      '#states' => [
        'visible' => [
          'select[name="settings[content_type]"]' => [
            'value' => 'catalog_item',
          ],
        ],
      ],
    ];

    // The number of contacts to show form element.
    $form['catalog_item']['limit'] = $this->uwBlockService->getLimitPerBlockFormElement(
      'catalog_item',
      $config
    );

    // The groups form element.
    $form['catalog_item']['catalog'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Catalog',
      $config['catalog_item'] ?? [],
      'uw_vocab_catalogs',
      'catalog'
    );

    // The catalog item tags form element.
    $form['catalog_item']['tags'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Category',
      $config['catalog_item'] ?? [],
      'uw_vocab_catalog_categories',
      'tags'
    );

    // Add the all items to the form.
    $this->addAllItemsToForm(
      'catalog_item',
      isset($config['catalog_item']) ? $config['catalog_item'] : [],
      $form
    );
  }

  /**
   * Function to get the contact form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   */
  private function getContactElements(
    array &$form,
    array $config
  ): void {

    // The contacts container for element.
    $form['contact'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Contact list settings'),
      '#states' => [
        'visible' => [
          'select[name="settings[content_type]"]' => [
            'value' => 'contact',
          ],
        ],
      ],
    ];

    // The number of contacts to show form element.
    $form['contact']['limit'] = $this->uwBlockService->getLimitPerBlockFormElement(
      'contact',
      $config
    );

    // The show photo form element.
    $form['contact']['show_photos'] = $this->uwBlockService->getShowImageFormElement(
      'contact',
      $config
    );

    // The groups form element.
    $form['contact']['groups'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Group',
      $config['contact'] ?? [],
      'uw_vocab_contact_group',
      'groups'
    );

    // How to sort the contacts form element.
    $form['contact']['sort'] = $this->uwBlockService->getSortFormElement(
      'contact',
      $config['contact'] ?? []
    );

    // Add the all items to the form.
    $this->addAllItemsToForm(
      'contact',
      isset($config['contact']) ? $config['contact'] : [],
      $form
    );
  }

  /**
   * Function to get the event form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   */
  private function getEventElements(
    array &$form,
    array $config
  ): void {

    // The events container for element.
    $form['event'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Event list settings'),
      '#states' => [
        'visible' => [
          'select[name="settings[content_type]"]' => [
            'value' => 'event',
          ],
        ],
      ],
    ];

    // The number of events to show form element.
    $form['event']['limit'] = $this->uwBlockService->getLimitPerBlockFormElement(
      'event',
      $config
    );

    // The groups form element.
    $form['event']['type'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Event type',
      $config['event'] ?? [],
      'uw_tax_event_type',
      'type',
      TRUE,
      TRUE
    );

    // The groups form element.
    $form['event']['tags'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Event tag',
      $config['event'] ?? [],
      'uw_tax_event_tags',
      'tags'
    );

    // The group form element.
    $form['event']['date'] = [
      '#title' => $this->t('Time period'),
      '#type' => 'select',
      '#options' => [
        'now' => $this->t('Current and upcoming only'),
        'past' => $this->t('Past only'),
        'all' => $this->t('All events regardless of date'),
        'range' => $this->t('Date range'),
      ],
      '#default_value' => $config['event']['date'] ?? 'now',
      '#required' => TRUE,
    ];

    // The groups form element.
    $form['event']['sort'] = [
      '#title' => $this->t('Sort order'),
      '#type' => 'select',
      '#options' => [
        'ascending' => $this->t('Past to future'),
        'descending' => $this->t('Future to past'),
      ],
      '#default_value' => $config['event']['sort'] ?? 'ascending',
      '#states' => [
        'visible' => [
          'select[name="settings[event][date]"]' => [
            'value' => 'all',
          ],
        ],
      ],
    ];

    // Add event date range group when selecting date range.
    $form['event']['range'] = [
      '#type' => 'fieldset',
      '#states' => [
        'visible' => [
          'select[name="settings[event][date]"]' => [
            'value' => 'range',
          ],
        ],
      ],
    ];

    // The date range group form element.
    $form['event']['range']['from'] = [
      '#type' => 'datetime',
      '#title' => $this->t('From'),
      '#default_value' => $config['event']['from'] ?? '',
      '#date_time_format' => 'short',
      '#date_timezone' => 'America/Toronto',
      '#date_date_element' => 'date',
      '#date_time_element' => 'none',
      '#date_year_range' => '-0:+1',
    ];

    // The date range group form element.
    $form['event']['range']['to'] = [
      '#type' => 'datetime',
      '#title' => $this->t('To'),
      '#default_value' => $config['event']['to'] ?? '',
      '#date_time_format' => 'short',
      '#date_timezone' => 'America/Toronto',
      '#date_date_element' => 'date',
      '#date_time_element' => 'none',
      '#date_year_range' => '-0:+1',
    ];

    // The date range group form element.
    $form['event']['range']['sort'] = [
      '#title' => $this->t('Sort order'),
      '#type' => 'select',
      '#options' => [
        'ascending' => $this->t('Past to future'),
        'descending' => $this->t('Future to past'),
      ],
      '#default_value' => $config['event']['range_sort'] ?? 'ascending',
    ];

    // Add the style for events.
    $form['event']['style'] = [
      '#type' => 'select',
      '#title' => $this->t('Style'),
      '#options' => [
        'default' => $this->t('Default'),
        'agenda' => $this->t('Agenda'),
      ],
      '#default_value' => $config['event']['style'] ?? 'default',
    ];

    // Add the all items to the form.
    $this->addAllItemsToForm(
      'event',
      isset($config['event']) ? $config['event'] : [],
      $form
    );

  }

  /**
   * Function to get the news items form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   */
  private function getNewsItemElements(
    array &$form,
    array $config
  ): void {

    // The news item container for element.
    $form['news_item'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('News item list settings'),
      '#states' => [
        'visible' => [
          'select[name="settings[content_type]"]' => [
            'value' => 'news_item',
          ],
        ],
      ],
    ];

    // The number of items to show form element.
    $form['news_item']['limit'] = $this->uwBlockService->getLimitPerBlockFormElement(
      'news_item',
      $config
    );

    // The tags form element.
    $form['news_item']['tags'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'News tag',
      $config['news_item'] ?? [],
      'uw_vocab_news_tags',
      'tags'
    );

    // Add the all items to the form.
    $this->addAllItemsToForm(
      'news_item',
      isset($config['news_item']) ? $config['news_item'] : [],
      $form
    );
  }

  /**
   * Function to get the opportunity form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   */
  private function getOpportunityElements(
    array &$form,
    array $config
  ): void {

    // The opportunity container for element.
    $form['opportunity'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Opportunity list settings'),
      '#states' => [
        'visible' => [
          'select[name="settings[content_type]"]' => [
            'value' => 'opportunity',
          ],
        ],
      ],
    ];

    // The number of items to show form element.
    $form['opportunity']['limit'] = $this->uwBlockService->getLimitPerBlockFormElement(
      'opportunity',
      $config
    );

    // The groups form element.
    $form['opportunity']['type'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Opportunity type',
      $config['opportunity'] ?? [],
      'uw_vocab_opportunity_type',
      'type',
      TRUE,
      FALSE,
      '- Any -',
    );

    // The groups form element.
    $form['opportunity']['employment'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Employment type',
      $config['opportunity'] ?? [],
      'uw_vocab_opportunity_employment',
      'employment',
      TRUE,
      FALSE,
      '- Any -',
    );

    // The groups form element.
    $form['opportunity']['rate_of_pay'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Rate of pay type',
      $config['opportunity'] ?? [],
      'uw_vocab_opportunity_payrate',
      'rate_of_pay',
      TRUE,
      FALSE,
      '- Any -',
    );

    // Add the all items to the form.
    $this->addAllItemsToForm(
      'opportunity',
      isset($config['opportunity']) ? $config['opportunity'] : [],
      $form
    );
  }

  /**
   * Function to get the profile form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   */
  private function getProfileElements(
    array &$form,
    array $config
  ): void {

    // The profiles container for element.
    $form['profile'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Profile list settings'),
      '#states' => [
        'visible' => [
          'select[name="settings[content_type]"]' => [
            'value' => 'profile',
          ],
        ],
      ],
    ];

    // The number of profiles to show form element.
    $form['profile']['limit'] = $this->uwBlockService->getLimitPerBlockFormElement(
      'profile',
      $config
    );

    // The show photo form element.
    $form['profile']['show_photos'] = $this->uwBlockService->getShowImageFormElement(
      'profile',
      $config
    );

    // The groups form element.
    $form['profile']['types'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Type',
      $config['profile'] ?? [],
      'uw_vocab_profile_type',
      'types'
    );

    // How to sort the profiles form element.
    $form['profile']['sort'] = $this->uwBlockService->getSortFormElement(
      'profile',
      $config
    );

    // Add the all items to the form.
    $this->addAllItemsToForm(
      'profile',
      isset($config['profile']) ? $config['profile'] : [],
      $form
    );
  }

  /**
   * Function to get the project form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   */
  private function getProjectElements(
    array &$form,
    array $config
  ): void {

    // The projects container for element.
    $form['project'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Project list settings'),
      '#states' => [
        'visible' => [
          'select[name="settings[content_type]"]' => [
            'value' => 'project',
          ],
        ],
      ],
    ];

    // The number of projects to show form element.
    $form['project']['limit'] = $this->uwBlockService->getLimitPerBlockFormElement(
      'project',
      $config
    );

    // The role form element.
    $form['project']['role'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Role',
      $config['project'] ?? [],
      'uw_vocab_project_role',
      'role',
      TRUE,
      FALSE,
      '- Any -'
    );

    // The status form element.
    $form['project']['status'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Status',
      $config['project'] ?? [],
      'uw_vocab_project_status',
      'status',
      TRUE,
      FALSE,
      '- Any -'
    );

    // The topic form element.
    $form['project']['topic'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Topic',
      $config['project'] ?? [],
      'uw_vocab_project_topic',
      'topic',
      TRUE,
      FALSE,
      '- Any -'
    );

    // Add the all items to the form.
    $this->addAllItemsToForm(
      'project',
      isset($config['project']) ? $config['project'] : [],
      $form
    );
  }

  /**
   * Function to get the publication reference form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   */
  private function getPublicationReferenceElements(
    array &$form,
    array $config
  ): void {

    // The publication reference container for element.
    $form['publication_reference'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Publication reference list settings'),
      '#states' => [
        'visible' => [
          'select[name="settings[content_type]"]' => [
            'value' => 'publication_reference',
          ],
        ],
      ],
    ];

    // The number of publications to show form element.
    $form['publication_reference']['limit'] = $this->uwBlockService->getLimitPerBlockFormElement(
      'publication_reference',
      $config
    );

    // The search form element.
    $form['publication_reference']['search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#default_value' => $config['publication_reference']['search'] ?? NULL,
    ];

    // If there are keywords, need to load the entities
    // for the default value.
    if (isset($config['publication_reference']['keywords'])) {

      // Step through each of the keywords and get
      // the full keyword entity.
      foreach ($config['publication_reference']['keywords'] as $keyword) {

        // Add the entity to the default values array.
        $default_values[] = $this->entityTypeManager
          ->getStorage('bibcite_keyword')
          ->load($keyword);
      }
    }

    // The keywords form element.
    $form['publication_reference']['keywords'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'bibcite_keyword',
      '#title' => $this->t('Keyword(s)'),
      '#tags' => TRUE,
      '#default_value' => $default_values ?? '',
    ];

    // Get all the bibcite reference types.
    $ref_types = $this->entityTypeManager
      ->getStorage('bibcite_reference_type')
      ->loadMultiple();

    // The array to store the options.
    $options = [];

    // Step through each of the bibcite reference
    // types and add to the options array.
    foreach ($ref_types as $ref_type) {
      $options[$ref_type->id()] = $ref_type->label();
    }

    // Details wrapper for the reference type.
    $form['publication_reference']['type_wrapper'] = [
      '#type' => 'details',
      '#title' => $this->t('Type'),
    ];

    // The reference type form element.
    $form['publication_reference']['type_wrapper']['type'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $config['publication_reference']['type'] ?? [],
    ];

    // The year form element.
    $form['publication_reference']['year'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Year'),
      '#default_value' => $config['publication_reference']['year'] ?? NULL,
    ];
  }

  /**
   * Function to get the service form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   */
  private function getServiceElements(
    array &$form,
    array $config
  ): void {

    // The service container for element.
    $form['service'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Service list settings'),
      '#states' => [
        'visible' => [
          'select[name="settings[content_type]"]' => [
            'value' => 'service',
          ],
        ],
      ],
    ];

    // The categories form element.
    $form['service']['categories'] = $this->uwBlockService->getTaxonomyTermFormElement(
      'Categories',
      $config['service'] ?? [],
      'uw_vocab_service_categories',
      'categories'
    );

    // Add the all items to the form.
    $this->addAllItemsToForm(
      'service',
      isset($config['service']) ? $config['service'] : [],
      $form
    );
  }

  /**
   * Function to add all items to the form.
   *
   * @param string $content_type
   *   The content type.
   * @param array $config
   *   The config.
   * @param array &$form
   *   The form.
   */
  private function addAllItemsToForm(
    string $content_type,
    array $config,
    array &$form
  ): void {

    // Configure whether to show items promoted to the front page or all.
    $form[$content_type]['show_all_items'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include all eligible items, not just those promoted to the home page'),
      '#default_value' => isset($config['show_all_items']) ? $config['show_all_items'] : TRUE,
    ];
  }

}
