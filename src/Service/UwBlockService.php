<?php

namespace Drupal\uw_custom_blocks\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\uw_cfg_common\Service\UwNodeContent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\Views;

/**
 * Class UwBlockService.
 *
 * UW Service that holds common functionality used by uw blocks.
 *
 * @package Drupal\uw_custom_common\Service
 */
class UwBlockService {

  // Need this in order for ajax and translations
  // to work correctly.
  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * UW service.
   *
   * @var \Drupal\uw_cfg_common\Service\UwNodeContent
   */
  protected $uwNodeContent;

  /**
   * Default constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager from core.
   * @param \Drupal\Core\Database\Connection $database
   *   The database entity.
   * @param \Drupal\uw_cfg_common\Service\UwNodeContent $uwNodeContent
   *   The node content service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    Connection $database,
    UwNodeContent $uwNodeContent
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->database = $database;
    $this->uwNodeContent = $uwNodeContent;
  }

  /**
   * Function to get teasers of nodes.
   *
   * @param array $nids
   *   Array of nids with weights.
   * @param int $show_image
   *   Flag to show the image.
   * @param int $show_title
   *   Flag to show the title.
   *
   * @return array
   *   Array of teasers.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTeasers(
    array $nids,
    int $show_image = 1,
    int $show_title = 1
  ): array {

    // Blank array to at least return something.
    $teasers = [];

    // Step through each nids and setup variable for template.
    foreach ($nids as $nid) {

      // Load the node.
      $node = $this->entityTypeManager->getStorage('node')->load($nid['id']);

      // If there is no node, i.e. the node has been deleted,
      // or the node is not unpublished, then just skip over
      // processing it.
      if (!$node || !$node->isPublished()) {
        continue;
      }

      // Get the teaser data.
      $teaser = $this->uwNodeContent->getNodeContent(
        $node,
        'teaser',
        'all'
      );

      // If we are not to show the image, set the image to null,
      // so that it doesn't display.
      if ($teaser && !$show_image) {
        $teaser['image'] = NULL;
      }

      // If we are not to show the title, set the title to null,
      // so that it doesn't display.
      if (!$show_title) {
        $teaser['title'] = NULL;
      }

      // Set teaser variables in the array.
      $teasers[$nid['weight']] = $teaser;
    }

    return $teasers;
  }

  /**
   * Function to get the show title form element.
   *
   * @param string $name
   *   The name of the element.
   * @param array $config
   *   The config for the block.
   *
   * @return array
   *   The show title form element.
   */
  public function getShowTitleFormElement(string $name, array $config): array {

    // The form element for show the title.
    return [
      '#type' => 'select',
      '#title' => $this->t('Show @name title?', ['@name' => $name]),
      '#description' => $this->t('Select if you want to show the title of the @name', ['@name' => $name]),
      '#options' => [
        1 => 'Yes',
        0 => 'No',
      ],
      '#default_value' => $config['show_title'] ?? 1,
    ];
  }

  /**
   * Function to get the show image form element.
   *
   * @param string $name
   *   The name of the element.
   * @param array $config
   *   The config for the block.
   *
   * @return array
   *   The show title form element.
   */
  public function getShowImageFormElement(string $name, array $config): array {

    // The show image form element.
    return [
      '#type' => 'select',
      '#title' => $this->t('Show @name image?', ['@name' => $name]),
      '#description' => $this->t('Select if you want to show the image of the @name.', ['@name' => $name]),
      '#options' => [
        1 => 'Yes',
        0 => 'No',
      ],
      '#default_value' => $config[$name]['show_photos'] ?? $config[$name]['show_image'] ?? 1,
    ];
  }

  /**
   * Function to get the in catalog form element.
   *
   * @param string $name
   *   The name of the element.
   * @param array $config
   *   The config for the block.
   *
   * @return array
   *   The in catalog form element.
   */
  public function getShowInCatalogFormElement(string $name, array $config): array {

    // The form element for show the title.
    return [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove "In catalog" indicator'),
      '#default_value' => $config[$name]['in_catalog'] ?? 0,
    ];
  }

  /**
   * Function to get taxonomy term form element.
   *
   * @param string $name
   *   The name of the element.
   * @param array $config
   *   The config for the block.
   * @param string $vocab
   *   The machine name of the taxonomy vocabulary.
   * @param string $config_name
   *   The name of the config.
   * @param bool $select
   *   Flag if this is a select element.
   * @param bool $multiple
   *   Flag if this is a multiple select element.
   * @param string $empty_option
   *   The empty option value.
   *
   * @return array
   *   The types form element.
   */
  public function getTaxonomyTermFormElement(
    string $name,
    array $config,
    string $vocab,
    string $config_name,
    bool $select = FALSE,
    bool $multiple = FALSE,
    string $empty_option = NULL
  ): array {

    // If this is a multiple or not a select, then make
    // the title plural. If not multiple just set the
    // title to the name.
    if ($multiple || !$select) {

      // Get the title for the element, it needs to be pluralized.
      if ($name == 'Category') {
        $title = 'Categories';
      }
      elseif ($name == 'Status') {
        $title = 'Status';
      }
      elseif (substr($name, -1) != 's') {
        $title = $name . '(s)';
      }
      else {
        $title = $name;
      }
    }
    else {
      $title = $name;
    }

    // Get the element based on if select list or not.
    if ($select) {

      // Load the terms.
      $terms = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadByProperties(['vid' => $vocab]);

      // If there are terms, set the options for the
      // select list, if not set empty array.
      if ($terms) {

        // Step through each term and set the options.
        foreach ($terms as $term) {
          $options[$term->id()] = $term->label();
        }
      }
      else {
        $options = [];
      }

      // Get the select element.
      $element = [
        '#type' => 'select',
        '#title' => $this->t('@title', ['@title' => $title]),
        '#options' => $options,
        '#multiple' => $multiple,
        '#default_value' => $config[$config_name] ?? '',
      ];

      // If there is an empty option, add it the
      // form element.
      if ($empty_option) {
        $element['#empty_option'] = $empty_option;
      }
    }
    else {

      // If there are terms in the config, get the objects
      // or set terms to null.
      if (
        isset($config[$config_name]) &&
        !empty($config[$config_name])
      ) {

        $terms = $this->entityTypeManager
          ->getStorage('taxonomy_term')
          ->loadMultiple($config[$config_name]);
      }
      else {
        $terms = NULL;
      }

      // Get the autocomplete element.
      $element = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'taxonomy_term',
        '#title' => $this->t('@title', ['@title' => $title]),
        '#tags' => TRUE,
        '#default_value' => $terms,
        '#selection_settings' => [
          'target_bundles' => [$vocab],
        ],
      ];
    }

    // The group form element.
    return $element;
  }

  /**
   * Function to get types form element.
   *
   * @param string $name
   *   The name of the element.
   * @param array $config
   *   The config for the block.
   * @param string $vocab
   *   The machine name of the taxonomy vocabulary.
   *
   * @return array
   *   The types form element.
   */
  public function getTypesFormElement(
    string $name,
    array $config,
    string $vocab
  ): array {

    // At least have blank types.
    $types = NULL;

    // If there is some config with types, get it.
    if (isset($config['nids']) && $nids = $config['nids']) {
      $types = $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple($nids);
    }

    // The group form element.
    return [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#title' => $this->t('Type to list'),
      '#description' => $this->t('Select a group or leave blank to display all @name. Only published @name will be displayed.', ['@name' => $name . 's']),
      '#tags' => TRUE,
      '#default_value' => $types,
      '#selection_settings' => [
        'target_bundles' => [$vocab],
      ],
    ];
  }

  /**
   * Function to get sort form element.
   *
   * @param string $name
   *   The name of the element.
   * @param array $config
   *   The config for the block.
   *
   * @return array
   *   The types form element.
   */
  public function getSortFormElement(string $name, array $config): array {

    // The sort form element.
    return [
      '#type' => 'select',
      '#title' => $this->t('Sort'),
      '#description' => $this->t('Alphabetic sort would use the name for sorting purposes field; global arrangement would use the weights from the rearrange @name view.', ['@name' => $name]),
      '#options' => [
        'alphabetic' => $this->t('Alphabetic'),
        'global' => $this->t('Global arrangement'),
      ],
      '#default_value' => $config['sort'] ?? 'alphabetic',
    ];
  }

  /**
   * Function to get the items for automatic block.
   *
   * @param string $name
   *   The name of the node.
   * @param array $config
   *   The config for the block.
   * @param string $config_name
   *   The name of the config.
   *
   * @return array
   *   Array of items.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getAutomaticItems(
    string $name,
    array $config,
    string $config_name
  ): array {

    $items = [];

    // Get the config for the block.
    $types = $config[$config_name];
    $sort = $config['sort'];
    $show_photos = $config['show_photos'];

    // Get the group/type field name based on the
    // type of block.
    $group_field_name = 'field_uw_ct_' . $name;
    if ($name == 'contact') {
      $group_field_name .= '_group';
    }
    else {
      $group_field_name .= '_type';
    }

    // If sort is alphabetic, use entity query.
    if ($sort === 'alphabetic') {

      // Build the query for contacts.
      $query = $this->entityTypeManager->getStorage('node')->getQuery()->accessCheck(FALSE);
      $query->condition('type', 'uw_ct_' . $name);
      $query->condition('status', NodeInterface::PUBLISHED);
      $query->condition('field_uw_exclude_auto', FALSE);

      // If the types are not empty add them to the query.
      if (!empty($types)) {

        // Add the condition to the query.
        $query->condition($group_field_name, $types, 'IN');
      }

      // Continue to build the query.
      $query->sort('field_uw_ct_' . $name . '_sort_name');

      // Ensuring that we get a proper limit.
      if (isset($config['limit']['limit'])) {
        $limit = $config['limit']['limit'];
      }
      elseif (isset($config['limit'])) {
        $limit = $config['limit'];
      }
      else {
        $limit = 3;
      }

      $query->range(0, $limit);

      // Add the show all items, if the config is set.
      if (
        isset($config['show_all_items']) &&
        !$config['show_all_items']
      ) {
        $query->condition('promote', 1, '=');
      }

      // Load the entities from the query.
      $entities = $this->entityTypeManager->getStorage('node')
        ->loadMultiple($query->execute());

      // Create teaser for each published entity.
      foreach ($entities as $entity) {

        // Get the teaser.
        $teaser = $this->uwNodeContent->getNodeContent(
          $entity,
          'teaser',
          'all'
        );

        // Remove the image if the config says to.
        if ($teaser && !$show_photos) {
          $teaser['image'] = NULL;
        }

        // Add the contact teaser to the array.
        $items[] = $teaser;
      }
    }
    elseif ($sort === 'global') {

      // If sort is using global, in that case use view to get results, and
      // check each contact for type.
      $view = Views::getView('uw_view_' . $name . 's');
      $view->execute('rearrange_' . $name . 's');

      // Counter variable.
      $count = 0;

      // Step through all the view results.
      foreach ($view->result as $item) {

        // Get the entity.
        $entity = $item->_entity;

        // Unpublished entity is skipped.
        if (!$entity->isPublished()) {
          continue;
        }

        // If there are no types provided for filter, use global list as is.
        if (empty($types)) {

          // Load the teaser.
          $teaser = $this->uwNodeContent->getNodeContent(
            $entity,
            'teaser',
            'all'
          );

          // Remove the image if the config says to.
          if ($teaser && !$show_photos) {
            $teaser['image'] = NULL;
          }

          $items[] = $teaser;
          continue;
        }

        // Array to store the types.
        $groups = [];

        // If types are defined, make sure to check each entity for group(s).
        foreach ($entity->$group_field_name as $group) {
          $groups[] = $group->target_id;
        }

        // Add contact only when all requested types match entity groups.
        if (array_intersect($types, $groups)) {

          // Only add if we have not hit the config limit.
          if ($count < $config['limit']) {

            // Load the teaser.
            $teaser = $this->uwNodeContent->getNodeContent(
              $entity,
              'teaser',
              'all'
            );

            // Remove the image if the config says to.
            if ($teaser && !$show_photos) {
              $teaser['image'] = NULL;
            }

            // Add the teaser to array of contacts.
            $items[] = $teaser;

            // Increment the counter.
            $count++;
          }
        }
      }
    }

    return $items;
  }

  /**
   * Function to get the limit per block form element.
   *
   * @param string $name
   *   The name of the element.
   * @param array $config
   *   The config for the block.
   *
   * @return array
   *   The limit per block form element.
   */
  public function getLimitPerBlockFormElement(string $name, array $config) {

    // The number of items to show form element.
    $form = [
      '#type' => 'select',
      '#title' => $this->t('Items per block'),
      '#description' => $this->t(
        'How many @name to display.',
        [
          '@name' => str_replace(
            '_',
            ' ',
            $name
          ) . 's',
        ]
      ),
      '#options' => [
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        10 => 10,
        12 => 12,
        20 => 20,
        24 => 24,
        40 => 40,
        48 => 48,
      ],
      '#default_value' => $config[$name]['limit'] ?? 3,
    ];

    return $form;
  }

  /**
   * Function to validate the block listing.
   *
   * @param array $form
   *   The form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $name
   *   The name of the block.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function validateListingBlock(
    array &$form,
    FormStateInterface $form_state,
    string $name
  ) {

    // The block ids.
    $ids_name = $name . '_ids';

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Get the ids of the listing from the form_state.
    $ids = $values['items_fieldset'][$ids_name];

    // Step through each id and check if its published.
    foreach ($ids as $key => $id) {

      // If there is an empty node, simply break out of
      // the validation and let Drupal core handle this
      // type of validation through entity autocomplete.
      if (!$id['id']) {

        // If entity autocomplete is empty set error message, and
        // it will not allow any block type field to be empty.
        $form_state->setError(
          $form['items_fieldset'][$ids_name][$key]['id'],
          $this->t(
            'The @name field cannot be empty.',
            ['@name' => $name]
          )
        );
        break;
      }

      // Get the node.
      $node = $this->entityTypeManager->getStorage('node')->load($id['id']);

      // If its not published, set an error.
      if ($node && !$node->isPublished()) {

        // Get the element name on the form.
        $element = 'items_fieldset][' . $ids_name . '][' . $key . '][id';

        // Set an error.
        $form_state->setErrorByName(
          $element,
          $this->t(
            '@title @name is not published.',
            ['@title' => $node->getTitle(), '@name' => $name]
          )
        );

        // Break out of the loop once we find an unpublished node.
        break;
      }
    }
  }

  /**
   * Function to get the terms in a list for views.
   *
   * @param array $terms
   *   The array of terms.
   *
   * @return string
   *   The term list usable for views.
   */
  public function getTermList(array $terms): string {

    // The term arg list.
    $term_list = '';

    // Counter for the comma.
    $count = 0;

    // Step through each of the tags and add to
    // the list.
    foreach ($terms as $term) {

      // If the count is greater than 0, add the
      // comma to the tag arg list.
      if ($count > 0) {
        $term_list .= ',';
      }

      // Add the tag and increment the comma counter.
      $term_list .= $term;
      $count++;
    }

    return $term_list;
  }

  /**
   * Function to get the automatic list block content types.
   *
   * @return array
   *   Array of content types.
   */
  public function getAutomaticContentTypes(): array {

    return [
      'blog' => $this->t('Blog posts'),
      'catalog' => $this->t('Catalogs'),
      'catalog_item' => $this->t('Catalog items'),
      'contact' => $this->t('Contacts'),
      'event' => $this->t('Events'),
      'news_item' => $this->t('News items'),
      'opportunity' => $this->t('Opportunities'),
      'profile' => $this->t('Profiles'),
      'project' => $this->t('Projects'),
      'publication_reference' => $this->t('Publication references'),
      'service' => $this->t('Services'),
    ];
  }

  /**
   * Function to get the manual list block content types.
   *
   * @return array
   *   Array of content types.
   */
  public function getManualContentTypes(): array {

    return [
      'blog' => $this->t('Blog posts'),
      'catalog' => $this->t('Catalogs'),
      'catalog_item' => $this->t('Catalog items'),
      'contact' => $this->t('Contacts'),
      'event' => $this->t('Events'),
      'news_item' => $this->t('News items'),
      'opportunity' => $this->t('Opportunities'),
      'profile' => $this->t('Profiles'),
      'project' => $this->t('Projects'),
      'service' => $this->t('Services'),
    ];
  }

  /**
   * Function to get the manual list block content types.
   *
   * @return array
   *   Array of content types.
   */
  public function getCtWithMachineNames(): array {

    return [
      'blog' => 'uw_ct_blog',
      'catalog' => 'uw_ct_catalog',
      'catalog_item' => 'uw_ct_catalog_item',
      'contact' => 'uw_ct_contact',
      'event' => 'uw_ct_event',
      'news_item' => 'uw_ct_news_item',
      'opportunity' => 'uw_ct_opportunity',
      'profile' => 'uw_ct_profile',
      'project' => 'uw_ct_project',
      'service' => 'uw_ct_service',
    ];
  }

  /**
   * Function to get the manual list block content type friendly names.
   *
   * @return array
   *   Array of content types.
   */
  public function getCtWithFriendlyNames(): array {

    return [
      'blog' => $this->t('Blog post'),
      'catalog' => $this->t('Catalog'),
      'catalog_item' => $this->t('Catalog item'),
      'contact' => $this->t('Contact'),
      'event' => $this->t('Event'),
      'news_item' => $this->t('News item'),
      'opportunity' => $this->t('Opportunity'),
      'profile' => $this->t('Profile'),
      'project' => $this->t('Project'),
      'service' => $this->t('Service'),
    ];
  }

  /**
   * Put the ids into sequential order.
   *
   * @param array $ids_values
   *   An array of ids to put in sequential order.
   *
   * @return array
   *   The correct array of sequential ids.
   */
  public function getNodeIds(array $ids_values): array {

    // Ensure that we have something to return.
    $ids = [];

    // Step through each of the ids and setup
    // sequential array.
    foreach ($ids_values as $ids_value) {

      // If there is an id, place it into the array.
      if ($ids_value['id'] !== NULL) {
        $ids[] = $ids_value;
      }
    }

    return $ids;
  }

}
