<?php

namespace Drupal\uw_custom_blocks\Service;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class UwBlockManualElements.
 *
 * UW Service that gets form elements for manual blocks.
 *
 * @package Drupal\uw_custom_common\Service
 */
class UwBlockManualElements {

  // Need this in order for ajax and translations
  // to work correctly.
  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * UW block service.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockService
   */
  protected $uwBlockService;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Default constructor.
   *
   * @param \Drupal\uw_custom_blocks\Service\UwBlockService $uwBlockService
   *   The UW block service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    UwBlockService $uwBlockService,
    EntityTypeManagerInterface $entityTypeManager
  ) {

    $this->uwBlockService = $uwBlockService;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Function to get all the form elements.
   *
   * @param array $form
   *   The form.
   * @param array $config
   *   The block config.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function getManualFormElements(
    array &$form,
    array $config,
    FormStateInterface $form_state
  ): void {

    // The content type with images.
    $ct_with_images = [
      'contact',
      'profile',
    ];

    // The content type with titles.
    $ct_with_title = [
      'contact',
    ];

    // Get the content types with machine names.
    $cts = $this->uwBlockService->getCtWithMachineNames();
    $ct_names = $this->uwBlockService->getCtWithFriendlyNames();

    // Step through each content type and get the elements.
    foreach ($cts as $ct => $machine_name) {
      // The content type container for element.
      $form[$ct] = [
        '#type' => 'fieldset',
        '#title' => $this->t('@ct list settings', ['@ct' => $ct_names[$ct]]),
        '#states' => [
          'visible' => [
            'select[name="settings[content_type]"]' => [
              'value' => $ct,
            ],
          ],
        ],
      ];

      // If the content type has images, set the form element.
      if (in_array($ct, $ct_with_images)) {

        // The form element for show the image.
        $form[$ct]['show_image'] = $this->uwBlockService->getShowImageFormElement(
          $ct,
          $config
        );
      }

      // If the content type has title, set the form element.
      if (in_array($ct, $ct_with_title)) {

        // The form element for show the title.
        $form[$ct]['show_title'] = $this->uwBlockService->getShowTitleFormElement(
          $ct,
          $config
        );
      }

      // The catalog item content type should have 'in catalog' checkbox.
      if ($ct == 'catalog_item') {
        // The form element for show the title.
        $form[$ct]['in_catalog'] = $this->uwBlockService->getShowInCatalogFormElement(
          $ct,
          $config
        );
      }

      // Set the nids.
      $this->getManualBlockNodeFormElement(
        $form,
        $form_state,
        $ct,
        $machine_name,
        $config
      );

      // If this is an event, add the style option.
      if ($ct == 'event') {
        $form[$ct]['style'] = [
          '#type' => 'select',
          '#title' => $this->t('Style'),
          '#options' => [
            'default' => $this->t('Default'),
            'agenda' => $this->t('Agenda'),
          ],
          '#default_value' => $config['event']['style'] ?? 'default',
        ];

        $form[$ct]['sort_order'] = [
          '#type' => 'select',
          '#title' => $this->t('Sort order'),
          '#options' => [
            'default' => $this->t('As entered'),
            'chronological' => $this->t('Chronological order'),
          ],
          '#default_value' => $config['event']['sort_order'] ?? 'default',
        ];
      }
    }
  }

  /**
   * Function to get the nodes for manual blocks.
   *
   * @param array $form
   *   The form element.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $name
   *   The name of the block.
   * @param string $bundle
   *   The machine name of the node.
   * @param array $config
   *   The array of config for the block.
   */
  public function getManualBlockNodeFormElement(
    array &$form,
    FormStateInterface &$form_state,
    string $name,
    string $bundle,
    array $config
  ) {

    // The index for the ids of the block.
    $ids_name = $name . '_ids';

    // The num of ids for the block type name.
    // Used to index on the config.
    $num_ids_name = 'num_' . $name . '_ids';

    // Get the status if we are updating a block or loading for first
    // time/new block.
    // This is set to true whenever we add/remove a node.
    // We need this in order to load in the correct config for the block,
    // either from the block config or from the form_state.
    $updating_block = $form_state->get('updating_block');

    // If we are updating a block, the number of ids is in
    // the form_state.
    if ($updating_block) {

      // Gather the number of names in the form already.
      $num_ids = $form_state->get($num_ids_name);
    }

    // Here we are loading for the first time or editing a block.
    else {

      // If the block config has number of ids set, meaning that
      // we are editing a block, then set the number of ids based
      // on block config.
      if (isset($config[$name][$num_ids_name])) {

        // Set the number of ids based on block config.
        $form_state->set($num_ids_name, $config[$name][$num_ids_name]);
        $num_ids = $config[$name][$num_ids_name];
      }

      // Otherwise this the first load of a block, so we have
      // initially set the number of ids to 1.
      else {

        // Set the number of ids to 1.
        $form_state->set($num_ids_name, 1);
        $num_ids = 1;
      }

      // Set the variable in the form state for updating blocks so that we can
      // use it later.  If we do not set this, we can not use it later when
      // we do an add/remove node.
      $form_state->set('updating_block', FALSE);
    }

    // Now get the correct updating block.
    $updating_block = $form_state->get('updating_block');

    // Set the title based on the content type.
    $ct_names = $this->uwBlockService->getCtWithFriendlyNames();
    if ($name == 'opportunity') {
      $title = 'Opportunities';
    }
    else {
      $title = $ct_names[$name] . 's';
    }

    // The fieldset for the content type.
    $form[$name]['items_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('<span class="form-required">@title</span>', ['@title' => $title]),
      '#prefix' => '<div id="items-fieldset-' . $name . '-wrapper">',
      '#suffix' => '</div>',
    ];

    // The help text for the items.
    $form[$name]['items_fieldset']['help_text'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Begin typing the name of a @name, then select it from the autocomplete to include it in the listing.', ['@name' => strtolower($ct_names[$name])]),
    ];

    // If we are not updating a block, we have to get the node ids
    // from the block config.
    if (!$updating_block) {
      if (isset($config[$name])) {
        // Get the  ids from the block config.
        $ids = $config[$name][$ids_name];

        // Set the ids into the form state.
        $form_state->set($ids_name, $ids);
      }
    }
    // If we are on updating a block, get the ids from the form_state.
    else {

      // Get the ids from the form_state.
      $ids = $form_state->get($ids_name);
    }

    // Setup the table using tabledrag.
    $form[$name]['items_fieldset'][$ids_name] = [
      '#type' => 'table',
      '#empty' => $this->t('No items.'),
      '#tableselect' => FALSE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'items-ids',
        ],
      ],
    ];

    // Add the form elements for the items, this is done by stepping
    // through the number of ids and adding the form element.
    for ($i = 0; $i < $num_ids; $i++) {

      // Check if the id is set and if so, then use it, if not
      // set it to NULL so that we can get correct default value.
      if (isset($ids[$i]['id'])) {
        $id = $ids[$i]['id'];
        $weight = $ids[$i]['weight'];
      }
      else {
        $id = NULL;
        $weight = $i;
      }

      // Set the weight for the row.
      $form[$name]['items'][$i]['#weight'] = $weight;

      // If this is a catalog, use taxonomy term for the autocomplete,
      // otherwise use node.
      if ($bundle == 'uw_ct_catalog') {

        $form[$name]['items_fieldset'][$ids_name][$weight]['id'] = [
          '#type' => 'entity_autocomplete',
          '#target_type' => 'taxonomy_term',
          '#selection_handler' => 'default',
          '#selection_settings' => [
            'target_bundles' => ['uw_vocab_catalogs'],
          ],
          '#default_value' => $id == NULL ? '' : $this->entityTypeManager->getStorage('taxonomy_term')->load($id),
        ];
      }
      else {

        $form[$name]['items_fieldset'][$ids_name][$weight]['id'] = [
          '#type' => 'entity_autocomplete',
          '#target_type' => 'node',
          '#selection_handler' => 'default',
          '#selection_settings' => [
            'target_bundles' => [$bundle],
          ],
          '#default_value' => $id == NULL ? '' : $this->entityTypeManager->getStorage('node')->load($id),
        ];
      }

      // If this is the first element, add the form required class.
      if ($weight == 0) {
        $form[$name]['items_fieldset'][$ids_name][$weight]['#attributes'] = [
          'class' => ['form-required', 'draggable'],
        ];
      }
      else {
        // Set the table to draggable.
        $form[$name]['items_fieldset'][$ids_name][$weight]['#attributes']['class'][] = 'draggable';
      }

      // The weight for the row of the table.
      $form[$name]['items_fieldset'][$ids_name][$weight]['weight'] = [
        '#type' => 'weight',
        '#required' => TRUE,
        '#title' => $this->t('Weight for @title', ['@title' => $i]),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        '#attributes' => ['class' => ['items-ids']],
        '#delta' => $num_ids,
      ];
    }

    // Ensure we have correct name for the button.
    $button_name = str_replace('_', ' ', $name);

    // The Add more button form element.
    $form[$name]['items_fieldset']['buttons']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add @name', ['@name' => $button_name]),
      '#submit' => [[$this, 'addOne']],
      '#ajax' => [
        'callback' => [$this, 'addmoreCallback'],
        'wrapper' => 'items-fieldset-' . $name . '-wrapper',
        'name' => $name,
      ],
    ];

    // If we have more than one id, put in the Remove item button.
    if ($num_ids > 1) {

      // Ensure we have correct name for the button.
      $button_name = str_replace('_', ' ', $name);

      // The remove button form element.
      $form[$name]['items_fieldset']['buttons']['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove @name', ['@name' => $button_name]),
        '#submit' => [[$this, 'removeCallback']],
        '#ajax' => [
          'callback' => [$this, 'addmoreCallback'],
          'wrapper' => 'items-fieldset-' . $name . '-wrapper',
          'name' => $name,
        ],
        '#limit_validation_errors' => [],
      ];
    }
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(
    array &$form,
    FormStateInterface $form_state
  ) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Get the content type from the values.
    $content_type = $values['settings']['content_type'];

    // Return the form element.
    return $form['settings'][$content_type]['items_fieldset'];
  }

  /**
   * Submit handler for the "add <block_type>>" button.
   *
   * Increments the number of and causes a form rebuild.
   */
  public function addOne(
    array &$form,
    FormStateInterface $form_state
  ) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Get the content type from the values.
    $content_type = $values['settings']['content_type'];

    // Set the variables to handle the ids.
    $ids_name = $content_type . '_ids';
    $num_ids_name = 'num_' . $content_type . '_ids';

    // Get the number of ids from the form_state.
    $num_ids = $form_state->get($num_ids_name);

    // Increment the number of ids.
    $num_ids++;

    // Get the ids and put into a sequential array.
    // If we don't do this then the array indexes are not
    // consistent and then set back into the form state.
    $form_state->set(
      $ids_name,
      $this->uwBlockService->getNodeIds(
        $values['settings'][$content_type]['items_fieldset'][$ids_name]
      )
    );

    // Set the number of ids back to the form state.
    $form_state->set($num_ids_name, $num_ids);

    // Set the flag that we have now updated the block, so that
    // when we go to rebuild the form it grabs the values from
    // the form_state and not the block config.
    $form_state->set('updating_block', TRUE);

    // Since our buildForm() method relies on the value of 'num_<name>_ids' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove <block_type>" button.
   *
   * Decrements the number of ids and causes a form rebuild.
   */
  public function removeCallback(
    array &$form,
    FormStateInterface $form_state
  ) {

    // Get the triggered element.
    $triggerd_element = $form_state->getTriggeringElement();

    // Get the content type from the triggered element.
    $content_type = $triggerd_element['#ajax']['name'];

    // Get the num of ids name.
    $num_ids_name = 'num_' . $content_type . '_ids';

    // Get the number of ids from the form_state.
    $num_ids = $form_state->get($num_ids_name);

    // Decrement the number of ids.
    $num_ids--;

    // If we have more than 1 ids, decrement and set back
    // to the form_state.
    if ($num_ids > 0) {
      $form_state->set($num_ids_name, $num_ids);
    }

    // Set the flag that we have now updated the block, so that
    // when we go to rebuild the form it grabs the values from
    // the form_state and not the block config.
    $form_state->set('updating_block', TRUE);

    // Since our buildForm() method relies on the value of 'num_ids' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

}
