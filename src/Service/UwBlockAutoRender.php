<?php

namespace Drupal\uw_custom_blocks\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\ViewExecutable;

/**
 * Class UwBlockAutoRender.
 *
 * UW Service that gets render arrays for automatic blocks.
 *
 * @package Drupal\uw_custom_common\Service
 */
class UwBlockAutoRender {

  /**
   * UW block service.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockService
   */
  protected $uwBlockService;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Default constructor.
   *
   * @param \Drupal\uw_custom_blocks\Service\UwBlockService $uwBlockService
   *   The UW block service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    UwBlockService $uwBlockService,
    EntityTypeManagerInterface $entityTypeManager
  ) {

    $this->uwBlockService = $uwBlockService;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Function to get the render array based on content type.
   *
   * @param string $content_type
   *   The content type.
   * @param array $config
   *   The block config.
   *
   * @return array
   *   The render array.
   */
  public function getRenderArray(
    string $content_type,
    array $config
  ): array {

    return match ($content_type) {
      'blog' => $this->getBlogRenderArray($config[$content_type]),
      'catalog' => $this->getCatalogRenderArray($config[$content_type]),
      'catalog_item' => $this->getCatalogItemRenderArray($config[$content_type]),
      'contact' => $this->getContactRenderArray($config[$content_type]),
      'event' => $this->getEventRenderArray($config[$content_type]),
      'news_item' => $this->getNewsItemRenderArray($config[$content_type]),
      'opportunity' => $this->getOpportunityRenderArray($config[$content_type]),
      'profile' => $this->getProfileRenderArray($config[$content_type]),
      'project' => $this->getProjectRenderArray($config[$content_type]),
      'publication_reference' => $this->getPublicationReferenceRenderArray($config[$content_type]),
      'service' => $this->getServiceRenderArray($config[$content_type])
    };
  }

  /**
   * Function to get render array for blogs.
   *
   * @param array $config
   *   The block config.
   *
   * @return array
   *   Render array for blogs.
   */
  private function getBlogRenderArray(array $config): array {

    // Load the view.
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('uw_view_blogs')
      ->getExecutable();

    // Set the view display.
    $view->setDisplay('blogs_listing_block');

    $filters_to_use = [
      'tags' => 'field_uw_blog_tags_target_id',
    ];

    // Set the filters for the view.
    $this->setViewFilters(
      $config,
      $filters_to_use,
      $view
    );

    // Add all items to the view.
    $this->addAllItemsToView(
      $config,
      $view
    );

    // Set the items per page.
    $view->setItemsPerPage($config['limit']);

    // Execute the view.
    $view->execute();

    // Get the render array.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => [
        'view' => $view->render(),
        'type' => 'view',
      ],
    ];
  }

  /**
   * Function to get render array for catalogs.
   *
   * @param array $config
   *   The block config.
   *
   * @return array
   *   Render array for catalogs.
   */
  private function getCatalogRenderArray(array $config): array {

    // Load the view.
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('uw_view_catalogs')
      ->getExecutable();

    // Set the view display.
    $view->setDisplay('catalog_embed');

    // Get the nid field from the view.
    $nid_field = $view->getHandler('catalog_embed', 'field', 'nid');

    // Set the element type to the heading selector
    // from the block config.
    $nid_field['element_type'] = $config['heading_selector'];

    // Set the field in the view.
    $view->setHandler('catalog_embed', 'field', 'nid', $nid_field);

    // Execute the view.
    $view->execute();

    // Remove the exposed filters.
    unset($view->exposed_widgets);

    // Set the variables for the template.
    $items['type'] = 'view';
    $items['view'] = $view->render();

    // Return custom template with variable.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => $items,
    ];
  }

  /**
   * Function to get render array for catalog items.
   *
   * @param array $config
   *   The block config.
   *
   * @return array
   *   Render array for catalogs.
   */
  private function getCatalogItemRenderArray(array $config): array {

    // Load the view.
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('uw_view_catalog_search')
      ->getExecutable();

    // The flag for all items. i.e. no filters.
    $all_items_flag = TRUE;

    // Load the correct view display, based on if a
    // catalog was entered.
    if (
      isset($config['catalog']) &&
      is_array($config['catalog']) &&
      count($config['catalog'])
    ) {

      // Set the display unset the all items flag.
      $view->setDisplay('search_specific_embed');
      $all_items_flag = FALSE;

      // Set the catalog argument list.
      $catalog_arg_list = $this->uwBlockService->getTermList($config['catalog']);

      // Get the catalog filter, set the value and set the handler.
      $filter = $view->getHandler(
        'search_specific_embed',
        'filter',
        'field_uw_catalog_catalog_target_id'
      );
      $filter['value'] = explode(',', $catalog_arg_list);
      $view->setHandler(
        'search_specific_embed',
        'filter',
        'field_uw_catalog_catalog_target_id',
        $filter
      );
    }

    // If there are tags, add them to the tag list
    // for contextual arguments.
    if (
      isset($config['tags']) &&
      is_array($config['tags']) &&
      count($config['tags'])
    ) {

      // Set the display unset the all items flag.
      $view->setDisplay('search_specific_embed');
      $all_items_flag = FALSE;

      // Set the catalog argument list.
      $tag_arg_list = $this->uwBlockService->getTermList($config['tags']);

      // Get the tag filter, set the value and set the handler.
      $filter = $view->getHandler(
        'search_specific_embed',
        'filter',
        'field_uw_catalog_category_target_id'
      );
      $filter['value'] = explode(',', $tag_arg_list);
      $view->setHandler(
        'search_specific_embed',
        'filter',
        'field_uw_catalog_category_target_id',
        $filter
      );
    }

    // If the all items flag is set, then set the display,
    // so that the correct view is used.
    if ($all_items_flag) {

      // Set the display.
      $view->setDisplay('catalog_search_all');
    }

    // If show all items is unchecked, meaning only show promoted
    // items, then add the filter for promote.
    if (
      isset($config['show_all_items']) &&
      !$config['show_all_items']
    ) {

      // Set the promote filter.
      $view->display_handler->options['filters']['promote'] = [
        'id' => 'promote',
        'table' => 'node_field_data',
        'field' => 'promote',
        'relationship' => 'none',
        'group_type' => 'group',
        'ui_name' => '',
        'operator' => '=',
        'value' => 1,
        'group' => 1,
        'exposed' => FALSE,
        'is_grouped' => FALSE,
      ];
    }

    // Set the number items per page from the config
    // items per block.
    $view->setItemsPerPage($config['limit']);

    // Cache keys can only be manipulated before pre_render.
    // Generate a cache key based on the block configuration.
    // Check: https://uwaterloo.atlassian.net/browse/ISTWCMS-6691
    if (empty($this->element['#pre_rendered'])) {
      $cache_keys = [
        'uw_cbl_catalog_item_listing',
        implode(',', $config['catalog'] ?? []),
        implode(',', $config['tags'] ?? []),
        $config['show_all_items'],
      ];
      $cache_key = implode(':', $cache_keys);
      $view->element['#cache']['keys'][] = $cache_key;
    }

    // Execute the view.
    $view->execute();

    // Remove the exposed filters and pager.
    unset($view->exposed_widgets, $view->pager);

    // Set the variables for the template.
    $items['type'] = 'view';
    $items['view'] = $view->render();

    // Return custom template with variable.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => $items,
    ];
  }

  /**
   * Function to get render array for contacts.
   *
   * @param array $config
   *   The block config.
   *
   * @return array
   *   Render array for contacts.
   */
  private function getContactRenderArray(array $config): array {

    // Get the items and type needed for the template.
    $items['items'] = $this->uwBlockService->getAutomaticItems(
      'contact',
      $config,
      'groups'
    );
    $items['type'] = 'contact';

    // Get the render array.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => $items,
    ];
  }

  /**
   * Function to get render array for events.
   *
   * @param array $config
   *   The block config.
   *
   * @return array
   *   Render array for contacts.
   */
  private function getEventRenderArray(array $config): array {

    // Load the view.
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('uw_view_events')
      ->getExecutable();

    // Set the view display based on date selection.
    if (isset($config['date'])) {
      $date = $config['date'];
      if ($date === 'now') {

        // Use event_block_current block display.
        $display_id = 'event_block_current';
      }
      elseif ($date === 'past') {

        // Use event_block_past block display.
        $display_id = 'event_block_past';
      }
      elseif ($date === 'all') {

        // Use event_block_all block display.
        $display_id = 'event_block_all';
      }
      elseif ($date === 'range') {
        if (isset($config['from']) && isset($config['to'])) {

          // Use event_block_range block display.
          $display_id = 'event_block_range';

          // Get 'from' and 'to' inputs.
          $from = gmdate("Y-m-d", $config['from']->getTimestamp());
          $to = gmdate("Y-m-d", $config['to']->getTimestamp());

          // Use field_uw_event_date_end_value as filter.
          $filter = $view->getHandler(
            $view->current_display,
            'filter',
            'field_uw_event_date_end_value'
          );

          // Set filter values.
          $filter['value']['min'] = $from;
          $filter['value']['max'] = $to;

          // Set filter values.
          $view->setHandler(
            $view->current_display,
            'filter',
            'field_uw_event_date_end_value',
            $filter
          );
        }
      }
    }
    // For existing site without date field.
    else {
      $display_id = 'events_listing_block';
    }

    // Set the display id for the view.
    $view->setDisplay($display_id);

    // The filters to be used in the view.
    $filters_to_use = [
      'tags' => 'field_uw_event_tags_target_id',
      'type' => 'field_uw_event_type_target_id',
    ];

    // Set the filters in the view.
    $this->setViewFilters(
      $config,
      $filters_to_use,
      $view
    );

    // Add all items to the view.
    $this->addAllItemsToView(
      $config,
      $view
    );

    // Set the number of items per page.
    $view->setItemsPerPage($config['limit']);

    // Sort by desc if selecting 'descending' for both all and range date.
    if ((isset($config['sort']) && $config['sort'] === 'descending') ||
        (isset($config['range_sort']) && $config['range_sort'] === 'descending')) {
      $sorts = $view->getDisplay()->getOption('sorts');
      $sorts['field_uw_event_date_value']['order'] = 'DESC';
      $view->getDisplay()->setOption('sorts', $sorts);
    }

    // If this is an agenda style, then add the css class
    // to the view.
    if (
      isset($config['style']) &&
      $config['style'] == 'agenda'
    ) {
      $view->display_handler->setOption('css_class', 'uw-agenda');
    }

    // Execute the view.
    $view->execute();

    // Return the render array for the view.
    return [
      '#type' => 'view',
      '#name' => 'view_name',
      '#view' => $view,
      '#display_id' => $display_id,
      '#embed' => TRUE,
      '#cache' => $view->getCacheTags(),
    ];
  }

  /**
   * Function to get render array for news.
   *
   * @param array $config
   *   The block config.
   *
   * @return array
   *   Render array for contacts.
   */
  private function getNewsItemRenderArray(array $config): array {

    // Load the view.
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('uw_view_news_items')
      ->getExecutable();

    // Set the view display.
    $view->setDisplay('news_items_listing_block');

    // The filters that are used in the view.
    $filters_to_use = [
      'tags' => 'field_uw_news_tags_target_id',
    ];

    // Set the filters in the view.
    $this->setViewFilters(
      $config,
      $filters_to_use,
      $view
    );

    // Set the number of items per page.
    $view->setItemsPerPage($config['limit']);

    // Add the all items to the view.
    $this->addAllItemsToView(
      $config,
      $view
    );

    // Execute the view.
    $view->execute();

    // Get the render array.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => [
        'view' => $view->render(),
        'type' => 'view',
      ],
    ];
  }

  /**
   * Function to get render array for opportunities.
   *
   * @param array $config
   *   The block config.
   *
   * @return array
   *   Render array for contacts.
   */
  private function getOpportunityRenderArray(array $config): array {

    // Load the view.
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('uw_view_opportunities')
      ->getExecutable();

    // Set the view display.
    $view->setDisplay('block_1');

    // The filters to use for this view.
    $filters_to_use = [
      'type' => 'field_uw_opportunity_type_target_id',
      'employment' => 'field_uw_opportunity_employment_target_id',
      'rate_of_pay' => 'field_uw_opportunity_pay_type_target_id',
    ];

    // Set the filters.
    $this->setViewFilters(
      $config,
      $filters_to_use,
      $view
    );

    // Set the number of items per page.
    $view->setItemsPerPage($config['limit']);

    // Add all items to the view.
    $this->addAllItemsToView(
      $config,
      $view
    );

    // Execute the actual view.
    $view->execute();

    // Get the render array.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => [
        'view' => $view->render(),
        'type' => 'view',
      ],
    ];
  }

  /**
   * Function to get render array for profiles.
   *
   * @param array $config
   *   The block config.
   *
   * @return array
   *   Render array for profiles.
   */
  private function getProfileRenderArray(array $config): array {

    // Get the items and type needed for the template.
    $items['items'] = $this->uwBlockService->getAutomaticItems(
      'profile',
      $config,
      'types'
    );
    $items['type'] = 'items';

    // Get the render array.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => $items,
    ];
  }

  /**
   * Function to get render array for projects.
   *
   * @param array $config
   *   The block config.
   *
   * @return array
   *   Render array for profiles.
   */
  private function getProjectRenderArray(array $config): array {

    // Load the view.
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('uw_view_projects')
      ->getExecutable();

    // Set the view display.
    $view->setDisplay('projects_block');

    // The config names to could be filters.
    $filters_to_use = [
      'role' => 'field_uw_project_role_target_id',
      'status' => 'field_uw_project_status_target_id',
      'topic' => 'field_uw_project_topics_target_id',
    ];

    // Set the filters for the view.
    $this->setViewFilters(
      $config,
      $filters_to_use,
      $view
    );

    // Set the number of items per page.
    $view->setItemsPerPage($config['limit']);

    // Add all items to the view.
    $this->addAllItemsToView(
      $config,
      $view
    );

    // Execute the view.
    $view->execute();

    // Get the render array.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => [
        'view' => $view->render(),
        'type' => 'view',
      ],
    ];
  }

  /**
   * Function to get render array for publication references.
   *
   * @param array $config
   *   The block config.
   *
   * @return array
   *   Render array for profiles.
   */
  private function getPublicationReferenceRenderArray(array $config): array {

    // Load the view.
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('bibcite_reference')
      ->getExecutable();

    // Set the view display.
    $view->setDisplay('publication_embed');

    // Set the number of items per page from config.
    $view->setItemsPerPage($config['limit']);

    // Have at least empty array of exposed filters,
    // so we do not get an undefined index error.
    $exposed_filters = [];

    // If there is search config, add the
    // exposed filters.
    if ($config['search'] !== '') {
      $exposed_filters['search'] = $config['search'];
    }

    // If there are keywords, add them to the
    // exposed filters.
    if (!empty($config['keywords'])) {

      // Set the keywords exposed to blank string.
      $exposed_filters['keywords'] = '';

      // Counter to add the comma.
      $count = 0;

      // Step through each keyword and add to the
      // keywords exposed filter.
      foreach ($config['keywords'] as $keyword) {

        // Load the keyword entity.
        $entity = $this->entityTypeManager->getStorage('bibcite_keyword')->load($keyword);

        // If there is more than one keyword add the
        // comma the keyword exposed filter.
        if ($count > 0) {
          $exposed_filters['keywords'] .= ',';
        }

        // Add the keyword entity label to the
        // keyword exposed filter.
        $exposed_filters['keywords'] .= $entity->label();

        // Increment the counter so we add the comma correctly.
        $count++;
      }
    }

    // If there are reference types, add them to
    // the exposed filters.
    if (!empty($config['type'])) {
      $exposed_filters['type'] = $config['type'];
    }

    // If there is a year, add to exposed filters.
    if ($config['year'] !== '') {
      $exposed_filters['year'] = $config['year'];
    }

    // If there are exposed filters, add to view.
    if (!empty($exposed_filters)) {
      $view->setExposedInput($exposed_filters);
    }

    // Execute the view.
    $view->execute();

    // Remove the exposed filters.
    unset($view->exposed_widgets);

    // Set the variables for the template.
    $items['type'] = 'view';
    $items['view'] = $view->render();

    // Return custom template with variable.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => $items,
    ];
  }

  /**
   * Function to get render array for services.
   *
   * @param array $config
   *   The block config.
   *
   * @return array
   *   Render array for profiles.
   */
  private function getServiceRenderArray(array $config): array {

    // Load the view.
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('uw_view_service_show_nodes')
      ->getExecutable();

    // Set the view display.
    $view->setDisplay('services_embed');

    // The filters to use in the view.
    $filters_to_use = [
      'categories' => 'field_uw_service_category_target_id',
    ];

    $this->setViewFilters(
      $config,
      $filters_to_use,
      $view
    );

    // Add all items to the view.
    $this->addAllItemsToView(
      $config,
      $view
    );

    // Execute the view.
    $view->execute();

    // Remove the exposed filters.
    unset($view->exposed_widgets);

    // Set the variables for the template.
    $items['type'] = 'view';
    $items['view'] = $view->render();

    // Return custom template with variable.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => $items,
    ];
  }

  /**
   * Function to set the filters in a view.
   *
   * @param array $config
   *   The array of block config.
   * @param array $filters_to_use
   *   Array of filters to use.
   * @param \Drupal\views\ViewExecutable $view
   *   The actual view.
   */
  private function setViewFilters(
    array $config,
    array $filters_to_use,
    ViewExecutable $view
  ) {

    // Step through each of the filters and add them
    // if they are in the block config.
    foreach ($filters_to_use as $config_name => $field_name) {

      // If the filter is in the block config add it.
      if (isset($config[$config_name])) {

        // Get the filter, set the value and set the handler.
        $filter = $view->getHandler(
          $view->current_display,
          'filter',
          $field_name
        );
        $filter['value'] = $config[$config_name];
        $view->setHandler(
          $view->current_display,
          'filter',
          $field_name,
          $filter
        );
      }
    }
  }

  /**
   * Function to add all items to the view.
   *
   * @param array $config
   *   The config for the block.
   * @param \Drupal\views\ViewExecutable $view
   *   The view.
   */
  private function addAllItemsToView(
    array $config,
    ViewExecutable &$view
  ): void {

    // If there is a show all items config, and it is not
    // set then add the filter to show only promoted.
    if (
      isset($config['show_all_items']) &&
      !$config['show_all_items']
    ) {

      // Set the all items on the view.
      $view->display_handler->options['filters']['promote'] = [
        'id' => 'promote',
        'table' => 'node_field_data',
        'field' => 'promote',
        'relationship' => 'none',
        'group_type' => 'group',
        'ui_name' => '',
        'operator' => '=',
        'value' => 1,
        'group' => 1,
        'exposed' => FALSE,
        'is_grouped' => FALSE,
      ];
    }
  }

}
