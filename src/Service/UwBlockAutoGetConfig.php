<?php

namespace Drupal\uw_custom_blocks\Service;

/**
 * Class UwBlockAutoGetConfig.
 *
 * UW Service that gets config from form state.
 *
 * @package Drupal\uw_custom_common\Service
 */
class UwBlockAutoGetConfig {

  /**
   * Function to get the selected config.
   *
   * @param array $values
   *   The array of form state values.
   *
   * @return array
   *   Array of selected config.
   */
  public function getConfig(array $values): array {

    return match ($values['content_type']) {
      'blog' => $this->getBlogConfig($values),
      'catalog' => $this->getCatalogConfig($values),
      'catalog_item' => $this->getCatalogItemConfig($values),
      'contact' => $this->getContactConfig($values),
      'event' => $this->getEventConfig($values),
      'news_item' => $this->getNewsItemConfig($values),
      'opportunity' => $this->getOpportunityConfig($values),
      'profile' => $this->getProfileConfig($values),
      'project' => $this->getProjectConfig($values),
      'publication_reference' => $this->getPublicationReferenceConfig($values),
      'service' => $this->getServiceConfig($values)
    };
  }

  /**
   * Get the config for blogs.
   *
   * @param array $values
   *   The array for form state values.
   *
   * @return array
   *   Array of config.
   */
  private function getBlogConfig(array $values): array {

    // Set the items per page in the config.
    $config['limit'] = $values['blog']['limit'];

    // If there are tags, set them in the config.
    if (
      isset($values['blog']['tags']) &&
      !empty($values['blog']['tags'])
    ) {

      // Step through each of the tags and add them
      // to the tags config.
      foreach ($values['blog']['tags'] as $tag) {
        $config['tags'][] = $tag['target_id'];
      }
    }

    // Add the show all items to the config.
    $config['show_all_items'] = $values['blog']['show_all_items'];

    return $config;
  }

  /**
   * Get the config for blogs.
   *
   * @param array $values
   *   The array for form state values.
   *
   * @return array
   *   Array of config.
   */
  private function getCatalogConfig(array $values): array {

    // Get the heading selector.
    $config['heading_selector'] = $values['catalog']['heading_selector'];

    return $config;
  }

  /**
   * Get the config for catalog items.
   *
   * @param array $values
   *   The array for form state values.
   *
   * @return array
   *   Array of config.
   */
  private function getCatalogItemConfig(array $values): array {

    // Set the catalog in the config.
    $config['catalog'] = $this->getTidsFromConfig($values['catalog_item']['catalog'] ?? []);

    // Reset the tags config.
    $config['tags'] = $this->getTidsFromConfig($values['catalog_item']['tags'] ?? []);

    // Set the items per block config.
    $config['limit'] = $values['catalog_item']['limit'];

    // Set the show all items config.
    $config['show_all_items'] = $values['catalog_item']['show_all_items'];

    return $config;
  }

  /**
   * Get the config for contacts.
   *
   * @param array $values
   *   The array for form state values.
   *
   * @return array
   *   Array of config.
   */
  private function getContactConfig(array $values): array {

    // Store the config for this contact block.
    $config['groups'] = $this->getTidsFromConfig($values['contact']['groups'] ?? []);
    $config['show_photos'] = $values['contact']['show_photos'];
    $config['sort'] = $values['contact']['sort'];
    $config['limit'] = $values['contact']['limit'];

    // Add the show all items to the config.
    $config['show_all_items'] = $values['contact']['show_all_items'];

    return $config;
  }

  /**
   * Get the config for events.
   *
   * @param array $values
   *   The array for form state values.
   *
   * @return array
   *   Array of config.
   */
  private function getEventConfig(array $values): array {

    // Set the event types.
    $config['type'] = $values['event']['type'];

    // Set the event tags.
    $config['tags'] = $this->getTidsFromConfig(
      $this->getTaxonomyTermIdsArray(
        $values['event'],
        'tags'
      )
    );

    // Set the items per block config.
    $config['limit'] = $values['event']['limit'];

    // Add the show all items to the config.
    $config['show_all_items'] = $values['event']['show_all_items'];

    // Set the time period.
    $config['date'] = $values['event']['date'];

    // Set from and to fields.
    $config['from'] = $values['event']['range']['from'];
    $config['to'] = $values['event']['range']['to'];

    // Set sort value selecting 'Present to past'.
    if ($values['event']['date'] === 'all' && $values['event']['sort'] === 'descending') {
      $config['sort'] = 'descending';
    }
    if ($values['event']['date'] === 'range' && $values['event']['range']['sort'] === 'descending') {
      $config['range_sort'] = 'descending';
    }

    // Get the style for the event.
    $config['style'] = $values['event']['style'];

    return $config;
  }

  /**
   * Get the config for news.
   *
   * @param array $values
   *   The array for form state values.
   *
   * @return array
   *   Array of config.
   */
  private function getNewsItemConfig(array $values): array {

    // Set the event tags.
    $config['tags'] = $this->getTidsFromConfig($values['news_item']['tags'] ?? []);

    // Set the items per block config.
    $config['limit'] = $values['news_item']['limit'];

    // Add the show all items to the config.
    $config['show_all_items'] = $values['news_item']['show_all_items'];

    return $config;
  }

  /**
   * Get the config for opportunities.
   *
   * @param array $values
   *   The array for form state values.
   *
   * @return array
   *   Array of config.
   */
  private function getOpportunityConfig(array $values): array {

    // Set the items per block config.
    $config['limit'] = $values['opportunity']['limit'];

    // Get the opportunity, employment and rate of pay
    // type config.
    $config['type'] = $values['opportunity']['type'] == '' ? NULL : $values['opportunity']['type'];
    $config['employment'] = $values['opportunity']['employment'] == '' ? NULL : $values['opportunity']['employment'];
    $config['rate_of_pay'] = $values['opportunity']['rate_of_pay'] == '' ? NULL : $values['opportunity']['rate_of_pay'];

    // Add the show all items to the config.
    $config['show_all_items'] = $values['opportunity']['show_all_items'];

    return $config;
  }

  /**
   * Get the config for profiles.
   *
   * @param array $values
   *   The array for form state values.
   *
   * @return array
   *   Array of config.
   */
  private function getProfileConfig(array $values): array {

    // Set the items per block and show image config.
    $config['limit'] = $values['profile']['limit'];
    $config['show_photos'] = $values['profile']['show_photos'];

    // Get the profile types and sort config.
    $config['types'] = $this->getTidsFromConfig($values['profile']['types'] ?? []);
    $config['sort'] = $values['profile']['sort'];

    // Add the show all items to the config.
    $config['show_all_items'] = $values['profile']['show_all_items'];

    return $config;
  }

  /**
   * Get the config for projects.
   *
   * @param array $values
   *   The array for form state values.
   *
   * @return array
   *   Array of config.
   */
  private function getProjectConfig(array $values): array {

    // Set the items per block and show image config.
    $config['limit'] = $values['project']['limit'];

    // Get the role, status and topic from config.
    $config['role'] = $values['project']['role'] == '' ? NULL : $values['project']['role'];
    $config['status'] = $values['project']['status'] == '' ? NULL : $values['project']['status'];
    $config['topic'] = $values['project']['topic'] == '' ? NULL : $values['project']['topic'];

    // Add the show all items to the config.
    $config['show_all_items'] = $values['project']['show_all_items'];

    return $config;
  }

  /**
   * Get the config for publication references.
   *
   * @param array $values
   *   The array for form state values.
   *
   * @return array
   *   Array of config.
   */
  private function getPublicationReferenceConfig(array $values): array {

    // Set the items per block config.
    $config['limit'] = $values['publication_reference']['limit'];

    // Get the heading level.
    $config['heading_level'] = $values['publication_reference']['heading_level'];

    // Set the search from the config.
    $config['search'] = $values['publication_reference']['search'] == '' ? NULL : $values['publication_reference']['search'];

    // If there are keywords, add them to the config.
    if ($values['publication_reference']['keywords']) {

      // Step through each of the keywords and set
      // the keywords config.
      foreach ($values['publication_reference']['keywords'] as $keyword) {
        $config['keywords'][] = $keyword['target_id'];
      }
    }
    else {
      $config['keywords'] = NULL;
    }

    // Step through each of the reference types and
    // add to the type config only if a value is
    // checked.
    foreach ($values['publication_reference']['type_wrapper']['type'] as $type) {
      if ($type !== 0) {
        $config['type'][] = $type;
      }
    }

    // If there is no types selected, set it to null.
    if (!isset($config['type'])) {
      $config['type'] = NULL;
    }

    // Get the year from the config.
    $config['year'] = $values['publication_reference']['year'] == '' ? NULL : $values['publication_reference']['year'];

    return $config;
  }

  /**
   * Get the config for services.
   *
   * @param array $values
   *   The array for form state values.
   *
   * @return array
   *   Array of config.
   */
  private function getServiceConfig(array $values): array {

    // Get the categories from the config.
    $config['categories'] = $this->getTidsFromConfig($values['service']['categories'] ?? []);

    // Add the show all items to the config.
    $config['show_all_items'] = $values['service']['show_all_items'];

    return $config;
  }

  /**
   * Function to get just the tids when using target_ids.
   *
   * @param array $target_ids
   *   Array with target ids.
   *
   * @return array
   *   Array of tids.
   */
  private function getTidsFromConfig(array $target_ids): array {

    // Have at least blank array to return.
    $tids = [];

    // Step through each of the target ids and
    // get just the tid.
    foreach ($target_ids as $target_id) {
      $tids[] = $target_id['target_id'];
    }

    return $tids;
  }

  /**
   * Function to get the tids from array.
   *
   * @param array $config
   *   The block config.
   * @param string $config_name
   *   The name of the config.
   *
   * @return array
   *   Array of tids.
   */
  private function getTaxonomyTermIdsArray(
    array $config,
    string $config_name
  ): array {

    if (
      isset($config[$config_name]) &&
      !empty($config[$config_name])
    ) {

      return $config[$config_name];
    }

    return [];
  }

}
