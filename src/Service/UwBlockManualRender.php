<?php

namespace Drupal\uw_custom_blocks\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class UwBlockManualRender.
 *
 * UW Service that gets render arrays for manual blocks.
 *
 * @package Drupal\uw_custom_common\Service
 */
class UwBlockManualRender {

  /**
   * UW block service.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockService
   */
  protected $uwBlockService;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Default constructor.
   *
   * @param \Drupal\uw_custom_blocks\Service\UwBlockService $uwBlockService
   *   The UW block service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    UwBlockService $uwBlockService,
    EntityTypeManagerInterface $entityTypeManager
  ) {

    $this->uwBlockService = $uwBlockService;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Function to get the render array based on content type.
   *
   * @param string $content_type
   *   The content type.
   * @param array $config
   *   The block config.
   *
   * @return array
   *   The render array.
   */
  public function getRenderArray(
    string $content_type,
    array $config
  ): array {

    // Have at least blank array to return.
    $render = [];

    // Get the render array based on the content type.
    switch ($content_type) {

      case 'catalog':
        $render = $this->getCatalogRender($config);
        break;

      case 'catalog_item':
        $render = $this->getCatalogItemRender($config);
        break;

      case 'event':
        $render = $this->getEventRender($config);
        break;

      default:
        $render = $this->getDefaultRender($config, $content_type);
        break;
    }

    return $render;
  }

  /**
   * Function to get the default render array.
   *
   * @param array $config
   *   The config for the block.
   * @param string $content_type
   *   The content type.
   *
   * @return array
   *   The render array.
   */
  private function getDefaultRender(array $config, string $content_type): array {

    // The content type with images.
    $ct_with_images = [
      'contact',
      'profile',
    ];

    // The content type with titles.
    $ct_with_title = [
      'contact',
    ];

    // Set show image and title to defaults.
    $show_image = 1;
    $show_title = 1;

    // Set the show image based on the content type.
    if (in_array($content_type, $ct_with_images)) {
      $show_image = $config[$content_type]['show_image'];
    }

    // Set teh show title based on the content type.
    if (in_array($content_type, $ct_with_title)) {
      $show_title = $config[$content_type]['show_title'];
    }

    // Get the nids.
    $nids = $config[$content_type][$content_type . '_ids'];

    // Set the items and the type.
    $items['items'] = $this->uwBlockService->getTeasers(
      $nids,
      $show_image ?? 1,
      $show_title ?? 1
    );

    // Set the type based on the content type, some need to
    // have the name changed so that it works on the template.
    if ($content_type == 'event') {
      $items['type'] = 'events';
      $items['style'] = $config['event']['style'] == 'agenda' ? 'uw-agenda' : '';
    }
    elseif ($content_type == 'catalog_item') {
      $items['type'] = 'catalog_item';
      $items['in_catalog'] = $config['catalog_item']['in_catalog'];
    }
    else {
      $items['type'] = $content_type;
    }

    // Return custom template with variable.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => $items,
    ];
  }

  /**
   * Function to get the catalog render array.
   *
   * @param array $config
   *   The config for the block.
   *
   * @return array
   *   The render array.
   */
  private function getCatalogRender(array $config): array {

    // Get the view for catalog items.
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('uw_view_catalogs')
      ->getExecutable();

    // Set the display to the embed view.
    $view->setDisplay('catalog_embed_filter');

    // Set the counter, so that we can add the plus sign
    // for multiple filters.
    $counter = 0;

    // Set the list of the argument.
    $argument = '';

    // Step through each of the catalogs and add id to
    // the argument.
    foreach ($config['catalog']['catalog_ids'] as $catalog) {

      // If the counter is greater than zero, add the plus sign
      // so that we can use multiple filters.
      if ($counter > 0) {
        $argument .= '+';
      }

      // Add the term id to the argument.
      $argument .= $catalog['id'];

      // Increment the counter so we get the plus sign.
      $counter++;
    }

    // Set the argument.
    $view->setArguments([$argument]);

    // Add the class to ensure that manual catalogs,
    // look like that automatic.
    $view->display_handler->setOption('css_class', 'path-catalogs');

    // Execute the view.
    $view->execute();

    // Get the results from the view.
    $results = $view->result;

    // Array of new sorted results.
    $new_results = [];

    // Step through the block config and put the results in
    // the order from the block config.
    $index = 0;
    foreach ($config['catalog']['catalog_ids'] as $catalog_id) {
      foreach ($results as $result) {
        if ($catalog_id['id'] == $result->tid) {
          $result->index = $index++;
          $new_results[] = $result;
          break;
        }
      }
    }

    // Set the results to the new results.
    $view->result = $new_results;

    // Remove the exposed filters and pager.
    unset($view->exposed_widgets, $view->pager);

    // Set the variables for the template.
    $items['type'] = 'view';
    $items['view'] = $view->render();

    // Return custom template with variable.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => $items,
    ];
  }

  /**
   * Function to get the catalog item render array.
   *
   * @param array $config
   *   The config for the block.
   *
   * @return array
   *   The render array.
   */
  private function getCatalogItemRender(array $config): array {

    // Load the view.
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('uw_view_catalog_search')
      ->getExecutable();

    // Load the correct view display, based on if a
    // catalog was entered.
    if (
      isset($config['catalog_item']['in_catalog']) &&
      $config['catalog_item']['in_catalog'] == 1
    ) {

      // Set the display unset the all items flag.
      $view->setDisplay('search_specific_embed');

      // Set the catalog argument list.
      $catalog_arg_list = $this->uwBlockService->getTermList($config['catalog']);

      // Get the catalog filter, set the value and set the handler.
      $filter = $view->getHandler(
        'search_specific_embed',
        'filter',
        'field_uw_catalog_catalog_target_id'
      );
      $filter['value'] = explode(',', $catalog_arg_list);
      $view->setHandler(
        'search_specific_embed',
        'filter',
        'field_uw_catalog_catalog_target_id',
        $filter
      );
    }
    else {

      // Set the display to the embed view.
      $view->setDisplay('catalog_search_all_embed');

      // If there are catalog items, add them to the
      // contextual filter of the view.
      if (
        isset($config['catalog_item']['catalog_item_ids']) &&
        count($config['catalog_item']['catalog_item_ids']) > 0
      ) {

        // The catalog id contextual filter value.
        $catalog_ids = '';

        // Counter used to add the + sign in between filters.
        $counter = 0;

        // Step through each of the catalog item ids and add them
        // to the catalog ids filter value.
        foreach ($config['catalog_item']['catalog_item_ids'] as $catalog_item_id) {

          // If we are past the first value add the plus sign so
          // that the contextual filters work correctly.
          if ($counter > 0) {
            $catalog_ids .= '+';
          }

          // Add the value to the filters.
          $catalog_ids .= $catalog_item_id['id'];

          // Increment the counter so we get the plus sign if required.
          $counter++;
        }

        // If there are catalog id filters, add to the view.
        if ($catalog_ids) {
          $view->setArguments([$catalog_ids]);
        }
      }
    }

    // Execute the view.
    $view->execute();

    // Get the results from the view.
    $results = $view->result;

    // Array of new sorted results.
    $new_results = [];

    // Step through the block config and put the results in
    // the order from the block config.
    $index = 0;
    foreach ($config['catalog_item']['catalog_item_ids'] as $catalog_item_id) {
      foreach ($results as $result) {
        if ($catalog_item_id['id'] == $result->nid) {
          $result->index = $index++;
          $new_results[] = $result;
          break;
        }
      }
    }

    // Set the results to the new results.
    $view->result = $new_results;

    // Remove the exposed filters and pager.
    unset($view->exposed_widgets, $view->pager);

    // Set the variables for the template.
    $items['type'] = 'view';
    $items['view'] = $view->render();

    // Return custom template with variable.
    return [
      '#theme' => 'uw_block_views_list',
      '#items' => $items,
    ];
  }

  /**
   * Function to get the event render array.
   *
   * @param array $config
   *   The config for the block.
   *
   * @return array
   *   The render array.
   */
  private function getEventRender(array $config): array {

    if (isset($config['event']['sort_order'])) {
      $sort_order = $config['event']['sort_order'];
    }
    else {
      $sort_order = 'default';
    }

    if (isset($config['event']['style'])) {
      $style = $config['event']['style'];
    }
    else {
      $style = 'default';
    }

    // If this is an agenda, use the view.
    if ($sort_order == 'chronological') {

      // Get the view for catalog items.
      $view = $this->entityTypeManager
        ->getStorage('view')
        ->load('uw_view_events')
        ->getExecutable();

      // Set the display to the embed view.
      $view->setDisplay('events_embed');

      // If there are catalog items, add them to the
      // contextual filter of the view.
      if (
        isset($config['event']['event_ids']) &&
        count($config['event']['event_ids']) > 0
      ) {

        // The event id contextual filter value.
        $event_ids = '';

        // Counter used to add the + sign in between filters.
        $counter = 0;

        // Step through each of the event item ids and add them
        // to the event ids filter value.
        foreach ($config['event']['event_ids'] as $event_item_id) {

          // If we are past the first value add the plus sign so
          // that the contextual filters work correctly.
          if ($counter > 0) {
            $event_ids .= '+';
          }

          // Add the value to the filters.
          $event_ids .= $event_item_id['id'];

          // Increment the counter so we get the plus sign if required.
          $counter++;
        }

        // If there are event id filters, add to the view.
        if ($event_ids) {
          $view->setArguments([$event_ids]);
        }

        // If this is an agenda, set the css class so that
        // the block will display correctly.
        if ($config['event']['style'] == 'agenda') {
          $view->display_handler->setOption('css_class', 'uw-agenda');
        }

        // Execute the view.
        $view->execute();

        // Set the variables for the template.
        $items['type'] = 'view';
        $items['view'] = $view->render();
        $items['style'] = $config['event']['style'] ?? 'default';

        // Return custom template with variable.
        return [
          '#theme' => 'uw_block_views_list',
          '#items' => $items,
        ];

      }
    }

    // For the agenda, ensure repeating items are listed individually.
    elseif ($style == 'agenda') {

      // Create a copy of the array that has no items.
      $events = $this->getDefaultRender($config, 'event');
      $individual_events = $events;
      unset($individual_events['#items']['items']);
      // Loop through the original array for items.
      foreach ($events['#items']['items'] as $event) {
        // Loop through the dates in the original array.
        foreach ($event['header']['date'] as $date) {
          // Create a copy of the event array that has no dates.
          $individual_event = $event;
          unset($individual_event['header']['date']);
          // Assign one date to this array.
          $individual_event['header']['date'][0] = $date;
          // Add this individual event to the full array.
          $individual_events['#items']['items'][] = $individual_event;
        }
      }
      return ($individual_events);
    }

    return $this->getDefaultRender($config, 'event');
  }

}
