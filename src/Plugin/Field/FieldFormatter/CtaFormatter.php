<?php

namespace Drupal\uw_custom_blocks\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'example_formatter_type' formatter.
 *
 * @FieldFormatter(
 *   id = "uw_cta_formatter",
 *   label = @Translation("UW CTA formatter"),
 *   field_types = {
 *     "uw_cta_item"
 *   }
 * )
 */
class CtaFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    /**
     * @var int $delta
     * @var  \Drupal\uw_custom_blocks\Plugin\Field\FieldType\CtaItem $item
     */
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#style' => $item->getCtaStyle(),
        '#text_value' => $item->getCtaText(),
        '#icon' => $item->getCtaIcon(),
      ];
    }

    return $elements;
  }

}
