<?php

namespace Drupal\uw_custom_blocks\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'fact_figures_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "uw_fact_item_formatter",
 *   label = @Translation("Facts and figures item formatter"),
 *   field_types = {
 *     "uw_fact_item"
 *   }
 * )
 */
class FactFiguresFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#style' => $item->style,
        '#text_value' => $item->text_value,
        '#info_type' => $item->info_type,
        '#info_text' => $item->info_text,
        '#info_suffix' => $item->info_suffix,
        '#icon' => $item->icon,
      ];
    }

    return $elements;
  }

}
