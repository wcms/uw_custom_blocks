<?php

namespace Drupal\uw_custom_blocks\Plugin\Field\FieldWidget;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'fact_text_widget' widget.
 *
 * @FieldWidget(
 *   id = "uw_fact_item_widget",
 *   label = @Translation("Fact and figures item widget"),
 *   field_types = {
 *     "uw_fact_item"
 *   }
 * )
 */
class FactTextWidget extends WidgetBase {

  /**
   * Service for random uuid.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $random;

  /**
   * {@inheritDoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    UuidInterface $uuid
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition,
      $settings, $third_party_settings);

    $this->random = $uuid;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings'], $container->get('uuid'));
  }

  /**
   * Defines available sizes for fact text.
   */
  const FACT_TEXT_SIZES = [
    'big' => 'Big',
    'medium' => 'Medium',
    'small' => 'Small highlight',
    'icon' => 'Icon',
    'infographic' => 'Infographic',
  ];

  /**
   * Defines infographics types.
   */
  const FACT_INFOGRAPHIC_TYPES = [
    'circle' => 'Circle chart',
    'half_circle' => 'Half circle chart',
    'horizontal' => 'Horizontal bar',
    'number' => 'Number animation',
    'vertical' => 'Vertical bar',
  ];

  /**
   * {@inheritDoc}
   */
  public static function defaultSettings() {
    return [
      'style' => 'big',
      'text_value' => '',
      'icon' => NULL,
      'info_type' => 'number',
      'info_text' => '',
      'info_fact_suffix' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\uw_custom_blocks\Plugin\Field\FieldType\FactTextItem $item */
    $item = $items[$delta];

    $values = $item->getFactDetails();

    $element += [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#element_validate' => [
        [static::class, 'validate'],
      ],
    ];

    // Getting random uuid to handle form/field states.
    $random_value = $this->random->generate();

    $element['style'] = [
      '#type' => 'select',
      '#default_value' => $values['style'],
      '#options' => self::FACT_TEXT_SIZES,
      '#title' => $this->t('Type'),
      '#id' => 'style-id-' . $random_value,
    ];

    $element['info_description'] = [
      '#type' => 'item',
      '#markup' => '<br/>Displays an infographic based on a data point. Five options are available:
<ul>
<li>Circle chart: Displays a rising circle chart in the chosen faculty colour based on a percentage, fraction, or ratio. Infographic fact will show as a percentage within the circle.</li>
<li>Half circle chart: Displays a rising half circle chart in the chosen faculty colour based on a percentage, fraction, or ratio. Infographic fact will show as a percentage under the half circle.</li>
<li>Horizontal bar: Displays a rising horizontal bar in the chosen faculty colour based on a percentage, fraction, or ratio. Infographic fact will show as a percentage to the right of the horizontal bar.</li>
<li>Number animation: Displays a rising number count based on a simple number (can include a decimal).</li>
<li>Vertical bar: Displays a rising vertical bar in the chosen faculty colour based on a percentage, fraction, or ratio. Infographic fact will show as a percentage to the right of the vertical bar.</li>
</ul>',
      '#states' => [
        'visible' => [
          'select[id="style-id-' . $random_value . '"]' => ['value' => 'infographic'],
        ],
      ],
    ];

    $element['info_type'] = [
      '#type' => 'select',
      '#default_value' => $values['info_type'],
      '#options' => self::FACT_INFOGRAPHIC_TYPES,
      '#title' => $this->t('Infographic type') . '<span class="form-required"></span>',
      '#empty_option' => $this->t('- Select a value -'),
      '#empty_value' => '_none',
      '#id' => 'info-id-' . $random_value,
      '#states' => [
        'visible' => [
          'select[id="style-id-' . $random_value . '"]' => ['value' => 'infographic'],
        ],
      ],
      '#attributes' => [
        'class' => [
          'form-required',
        ],
      ],
    ];

    $element['text_value'] = [
      '#type' => 'textfield',
      '#default_value' => $values['text_value'],
      '#size' => 50,
      '#maxlength' => 255,
      '#title' => $this->t('Text'),
      '#states' => [
        'invisible' => [
          ['select[id="style-id-' . $random_value . '"]' => ['value' => 'icon']],
          ['select[id="style-id-' . $random_value . '"]' => ['value' => 'infographic']],
        ],
      ],
    ];

    $element['info_text'] = [
      '#type' => 'textfield',
      '#description' => $this->t("For circle or bar charts, enter a percentage (90%), a ratio (3:4), or a fraction (3/4) that doesn't exceed 100%. Do not include spaces. For number animations, enter a simple number only (can include a decimal or comma). Do not include spaces."),
      '#default_value' => $values['info_text'],
      '#size' => 50,
      '#maxlength' => 255,
      '#title' => $this->t('Infographic fact'),
      '#states' => [
        'visible' => [
          'select[id="info-id-' . $random_value . '"]' => ['!value' => '_none'],
        ],
      ],
    ];

    $element['info_suffix'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Suffix added to the end of the Infographic fact (i.e. nd, years, etc.).'),
      '#default_value' => $values['info_suffix'],
      '#size' => 50,
      '#maxlength' => 255,
      '#title' => $this->t('Infographic fact suffix'),
      '#states' => [
        'visible' => [
          'select[id="style-id-' . $random_value . '"]' => ['value' => 'infographic'],
          'select[id="info-id-' . $random_value . '"]' => ['value' => 'number'],
        ],
      ],
    ];

    $element['icon'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => ['uw_mt_icon'],
      '#title' => $this->t('Icon'),
      '#default_value' => $values['icon'],
      '#description' => $this->t('Upload or select your profile image.'),
      '#states' => [
        'visible' => [
          ':input[id="style-id-' . $random_value . '"]' => ['value' => 'icon'],
        ],
      ],
    ];

    return $element;
  }

  /**
   * Converts infographics value to actual percent.
   *
   * @param string $fact_value
   *   Input value as string. It may be (4/5), or (5:6), or (4%5).
   *
   * @return int
   *   Percent.
   */
  public static function convertInfographicsValueToPercent(string $fact_value): int {
    $fact_percent = 0;

    if (strrpos($fact_value, '%') !== FALSE) {
      $fact_percent = rtrim($fact_value, '%');
    }
    elseif (strrpos($fact_value, ':') !== FALSE) {
      $ratio_parts = explode(':', $fact_value);
      $fact_percent = (int) $ratio_parts[0] / (int) $ratio_parts[1] * 100;
    }
    elseif (strrpos($fact_value, '/') !== FALSE) {
      $fraction_parts = explode('/', $fact_value);
      $fact_percent = (int) $fraction_parts[0] / (int) $fraction_parts[1] * 100;
    }

    return $fact_percent;
  }

  /**
   * Validation for infographics.
   *
   * @param mixed $element
   *   Entire FF is used as element, with all sub-fields.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   */
  public static function validate(&$element, FormStateInterface $formState): void {
    $value = trim($element['info_text']['#value']);

    $type = $element['info_type']['#value'];

    if (!empty($value)) {
      $error_message = NULL;

      if ($type === 'number' && !(is_numeric($value) || preg_match('/^\d{1,3}(,\d{3})*(\.\d+)?$/', $value))) {
        $error_message = 'The value for the Infographic fact does not match the accepted formats for a number animation. It must be a simple number or number with thousands marker (e.g. 50,000)';
      }
      elseif ($type !== 'number' && !(preg_match('/\d*(%|:\d|\/\d)/', $value))) {
        $error_message = 'The value for the Infographic fact does not match the accepted formats.';
      }
      elseif (substr_count($value, '%') + substr_count($value, '/') + substr_count($value, ':') > 1) {
        $error_message = 'The value for the Infographic fact does not match the accepted formats.';
      }
      elseif (preg_match('/\d*(%|:\d|\/\d)/', $value)) {
        $fact_percent = self::convertInfographicsValueToPercent($value);

        if ($fact_percent > 100) {
          $error_message = 'Percentage value is greater than 100%. For graphics to work properly, converted values must be less than 100%';
        }
      }

      if ($error_message) {
        $formState->setError($element['info_text'], $error_message);
      }
    }
  }

}
