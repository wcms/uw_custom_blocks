<?php

namespace Drupal\uw_custom_blocks\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'example_widget_type' widget.
 *
 * @FieldWidget(
 *   id = "uw_cta_widget",
 *   module = "uw_custom_blocks",
 *   label = @Translation("UW CTA Text widget"),
 *   field_types = {
 *     "uw_cta_item"
 *   }
 * )
 */
class CtaWidget extends WidgetBase {

  /**
   * Defines available sizes for cta text.
   */
  const CTA_TEXT_SIZES = [
    'small' => 'Small',
    'medium' => 'Medium',
    'big' => 'Big',
  ];

  /**
   * Random service.
   *
   * @var \Drupal\Component\Utility\Random
   */
  protected $random;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->random = new Random();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    /** @var \Drupal\uw_custom_blocks\Plugin\Field\FieldType\CtaItem $item */
    $item = $items[$delta];

    // Getting random set of chars to handle form/field states.
    $random_value = $this->random->name(10, TRUE);

    $values = $item->getCtaDetails();

    $element += [
      '#type' => 'fieldset',
      '#tree' => TRUE,
    ];

    $element['style'] = [
      '#type' => 'select',
      '#default_value' => $values['style'] ?? 'big',
      '#options' => self::CTA_TEXT_SIZES,
      '#field_prefix' => $this->t('Type'),
      '#id' => 'cta_style_' . $random_value,
    ];

    $element['text_value'] = [
      '#type' => 'textfield',
      '#default_value' => $values['text_value'] ?? '',
      '#size' => 50,
      '#maxlength' => 255,
      '#field_prefix' => $this->t('Text'),
      '#states' => [
        'invisible' => [
          ':input[id="cta_style_' . $random_value . '"]' => ['value' => 'icon'],
        ],
      ],
    ];
    return $element;
  }

}
