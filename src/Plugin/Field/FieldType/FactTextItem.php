<?php

namespace Drupal\uw_custom_blocks\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\uw_custom_blocks\Plugin\Field\FieldWidget\FactTextWidget;

/**
 * Plugin implementation of the 'fact_text_field_type' field type.
 *
 * @FieldType(
 *   id = "uw_fact_item",
 *   label = @Translation("UW Fact item"),
 *   description = @Translation("Facts and figures item includes style, text and/or icon."),
 *   default_widget = "uw_fact_item_widget",
 *   default_formatter = "uw_fact_item_formatter"
 * )
 */
class FactTextItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['style'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Type'))
      ->setRequired(TRUE);

    $properties['text_value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Text'));

    $properties['icon'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Icon'));

    $properties['info_type'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Infographic type'));

    $properties['info_text'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Infographic fact'));

    $properties['info_suffix'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Infographic fact suffix'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'text_value' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'style' => [
          'type' => 'varchar',
          'length' => 32,
        ],
        'icon' => [
          'description' => 'The ID of the file entity.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'info_type' => [
          'type' => 'varchar',
          'length' => 50,
          'description' => 'Infographic type.',
        ],
        'info_text' => [
          'type' => 'varchar',
          'length' => 20,
          'description' => 'Infographic fact.',
        ],
        'info_suffix' => [
          'type' => 'varchar',
          'length' => 25,
          'description' => 'Infographic suffix for number type.',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['style'] = 'medium';
    $values['text_value'] = $random->word(mt_rand(1, 10));
    $values['icon'] = NULL;
    $values['info_type'] = NULL;
    $values['info_text'] = NULL;
    $values['info_suffix'] = NULL;

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('text_value')->getValue();
    $icon = $this->get('icon')->getValue();
    $info = $this->get('info_text')->getValue();

    // For field to be empty both text, icon, and info fact need to be empty.
    return ($value === NULL || $value === '') && ($icon === NULL || $icon === '') && ($info === NULL || $info === '');
  }

  /**
   * Returns all data for Fact item.
   *
   * @return array
   *   Returns one fact style and text or icon.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   *   Values not found.
   */
  public function getFactDetails(): array {
    return [
      'style' => $this->getFactStyle(),
      'text_value' => $this->getFactText(),
      'icon' => $this->getFactIcon(),
      'info_type' => $this->getFactInfographicsType(),
      'info_text' => $this->getFactInfographicsText(),
      'info_suffix' => $this->getFactInfographicsTextSuffix(),
    ];
  }

  /**
   * Returns fact item style that may be big|medium|small|icon.
   *
   * @return string|null
   *   Style used for fact item.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getFactStyle(): ?string {
    return $this->get('style')->getValue();
  }

  /**
   * Returns fact text value.
   *
   * @return string|null
   *   User entered text value.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getFactText(): ?string {
    return $this->get('text_value')->getValue();
  }

  /**
   * Returns icon file id (fid) or null.
   *
   * @return string|null
   *   File id (fid) or null.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getFactIcon(): ?string {
    return $this->get('icon')->getValue();
  }

  /**
   * Returns infographic type.
   *
   * @return string|null
   *   Infographic type value.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getFactInfographicsType(): ?string {
    return $this->get('info_type')->getValue();
  }

  /**
   * Returns infographic fact suffix.
   *
   * @return string|null
   *   Infographic text suffix value.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getFactInfographicsTextSuffix(): ?string {
    return $this->get('info_suffix')->getValue();
  }

  /**
   * Returns infographic fact.
   *
   * @return string|null
   *   Infographic text suffix value.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getFactInfographicsText(): ?string {
    return $this->get('info_text')->getValue();
  }

  /**
   * Returns updated value for facts and figures.
   *
   * @return string|null
   *   Calculated number for chart.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getFactInfographicsTextTransformed(): ?string {
    $value = $this->getFactInfographicsText();
    $type = $this->getFactInfographicsType();

    if ($type && $type !== 'number') {
      $value = FactTextWidget::convertInfographicsValueToPercent($value);
    }

    return $value;
  }

}
