<?php

namespace Drupal\uw_custom_blocks\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'uw_cta_item' field type.
 *
 * @FieldType(
 *   id = "uw_cta_item",
 *   label = @Translation("UW CTA text"),
 *   description = @Translation("Call to action text"),
 *   default_widget = "uw_cta_widget",
 *   default_formatter = "uw_cta_formatter"
 * )
 */
class CtaItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {

    $properties['style'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Type'))
      ->setRequired(TRUE);

    $properties['text_value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Text'));

    $properties['icon'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Icon'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'text_value' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'style' => [
          'type' => 'varchar',
          'length' => 32,
        ],
        'icon' => [
          'description' => 'The ID of the file entity.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $random = new Random();

    $values['style'] = array_rand(['small', 'medium', 'big', 'icon']);
    $values['text_value'] = $random->word(mt_rand(1, 10));
    $values['icon'] = NULL;

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('text_value')->getValue();
    $icon = $this->get('icon')->getValue();

    // For field to be empty both text and icon need to be empty.
    return ($value === NULL || $value === '') && ($icon === NULL || $icon === '');
  }

  /**
   * Get CTA details.
   *
   * @return array
   *   Array of values for cta.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getCtaDetails(): array {
    return [
      'style' => $this->getCtaStyle(),
      'text_value' => $this->getCtaText(),
      'icon' => $this->getCtaIcon(),
    ];
  }

  /**
   * Returns CTA style.
   *
   * @return string|null
   *   Style if available or NULL.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getCtaStyle(): ?string {
    return $this->get('style')->getValue();
  }

  /**
   * Returns CTA text.
   *
   * @return string|null
   *   Text value.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getCtaText(): ?string {
    return $this->get('text_value')->getValue();
  }

  /**
   * Returns icon file id (fid) or null.
   *
   * @return string|null
   *   File id (fid) or null.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getCtaIcon(): ?string {
    return $this->get('icon')->getValue();
  }

}
