<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\uw_cfg_common\Service\UWService;
use Drupal\uw_cfg_common\Service\UWServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * UW custom block content moderation.
 *
 * @Block(
 *   id = "uw_cbl_content_moderation",
 *   admin_label = @Translation("UW content moderation"),
 * )
 */
class UwCblContentModeration extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UW service.
   *
   * @var \Drupal\uw_cfg_common\Service\UWServiceInterface
   */
  protected $uwService;

  /**
   * Node entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The variable for route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The variable for current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\uw_cfg_common\Service\UWServiceInterface $uwService
   *   UW service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The routeMatch.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   The currentPath.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, UWServiceInterface $uwService, RouteMatchInterface $routeMatch, CurrentPathStack $currentPath) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->uwService = $uwService;
    $this->routeMatch = $routeMatch;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->currentPath = $currentPath;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('entity_type.manager'), $container->get('uw_cfg_common.uw_service'), $container->get('current_route_match'), $container->get('path.current'));
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {

    // Set the mods variable to NULL so that we have a fresh start.
    $mods = [];

    // If this is not a node page, we do not even
    // want to display a content moderation block,
    // so just return the empty array.
    if ($this->routeMatch->getRouteName() !== 'entity.node.canonical') {
      return $mods;
    }

    // Flag to tell if this is a revision page.
    $is_revision = FALSE;

    // Get the current path.
    $current_path = explode('/', $this->currentPath->getPath());

    // Ensure that we are not in layout builder.
    if (end($current_path) !== 'layout') {

      // If we have a node revision in the parameters, then this is a
      // revisions page, so we can get the vid from the route parameter
      // and then load the node from the string.
      if ($vid = $this->routeMatch->getParameter('node_revision')) {

        // Now get the node which is from the route parameters.
        $node = $this->entityTypeManager->getStorage('node')->load(
          $this->routeMatch->getParameter('node')
        );

        // Set the flag that this is a revision page.
        $is_revision = TRUE;
      }
      else {

        // The node will be from the route parameters.
        $node = $this->routeMatch->getParameter('node');

        // Get the vid from the node object.
        $vid = $node->vid->value;
      }

      // Make sure we got a VID. At some point we may want to find out
      // why this is called from places that don't have VIDs.
      if ($vid) {
        // Get all the revisions for the current node.
        $revision_ids = $this->nodeStorage->revisionIds($node);

        // Get the nid from the node object.
        $nid = $node->nid->value;

        // Get the status of the node from node object.
        $status = $node->status->value;

        // Set the moderation link to NULL in case we do not have any link
        // to be displayed.
        $moderation_link = NULL;

        // If we are on a revisions page set the URL back to the revisions
        // page and set the link text.
        if ($is_revision) {
          $moderation_link = Url::fromRoute('entity.node.version_history', ['node' => $nid]);
          $link_text = $this->t('View all revisions');
        }

        // If we are on the node view page, set the moderation URL to the
        // moderation control custom page and set the link text.
        else {

          // Get the URL of the moderation page or NULL if the user
          // has no access.
          $moderation_url = [
            'nid' => $nid,
            'vid' => $vid,
            'status' => $status,
          ];

          // Set the moderation link to the content moderation form.
          $moderation_link = Url::fromRoute('uw_content_moderation.form', $moderation_url);

          // Set the link text based on the status of the node.
          if ($status) {
            $link_text = $this->t('Unpublish this content');
          }
          else {
            $link_text = $this->t('Publish this content');
          }
        }

        // Ensure that the user has access to the moderation form.
        if ($moderation_link->access() && !UWService::nodeIsHomePage($nid)) {

          // Set the moderation URL using the correct URL setup and
          // the link text that we set above.
          $moderation_link = Link::fromTextAndUrl($link_text, $moderation_link);
        }
        else {
          $moderation_link = NULL;
        }

        // Set the variables for the template.
        $mods = [
          // 'nid' is needed by
          // uw_fdsu_theme_resp/templates/node/node.html.twig.
          'nid' => $nid,
          'latest' => end($revision_ids) == $vid ? 'Yes' : 'No',
          'status' => $status,
          'moderation_link' => $moderation_link,
        ];
      }

      // Return the theme with the variables.
      return [
        '#theme' => 'uw_block_content_moderation',
        '#mods' => $mods,
      ];
    }

    // For some pages a blank build array is required to be returned
    // or you get the white screen of death.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {

    // Set the access for this block to false, it will
    // decide later if there is access based on
    // permissions from the user.......
    $access = FALSE;

    // Check if we are on a node, the only way this block
    // will show is if it is on a node, ensuring that we
    // do not get this block on things like views that have
    // nodes on it.
    if ($this->routeMatch->getRouteName() == 'entity.node.canonical') {

      // Get the node from the router parameter.
      $node = $this->routeMatch->getParameter('node');

      // Return the access based on the node bundle.
      // ISTWCMS-5132: We only have to check the "edit any"
      // permission for this content type, since we don't
      // give anyone "edit own" without also giving them
      // "edit any".
      $access = $account->hasPermission('edit any ' . $node->bundle() . ' content');
    }

    return $access;
  }

}
