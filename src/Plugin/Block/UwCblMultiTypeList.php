<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\uw_cfg_common\Service\UwNodeContent;
use Drupal\uw_cfg_common\Service\UWServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UW Custom Block Multi Type List block.
 *
 * @Block(
 *   id = "uw_cbl_multi_type_list",
 *   admin_label = @Translation("Multi-type list"),
 * )
 */
class UwCblMultiTypeList extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Cache max-age set to 6 hours.
   *
   * @var int
   */
  public static $cacheMaxAge = 21600;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UW service.
   *
   * @var \Drupal\uw_cfg_common\Service\UWServiceInterface
   */
  protected $uwService;

  /**
   * Node entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * UW node content service.
   *
   * @var \Drupal\uw_cfg_common\Service\UwNodeContent
   */
  protected $uwNodeContent;

  /**
   * Request unix timestamp.
   *
   * @var int
   */
  protected $requestTime;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\uw_cfg_common\Service\UWServiceInterface $uwService
   *   UW service.
   * @param \Drupal\uw_cfg_common\Service\UwNodeContent $uwNodeContent
   *   UW node content service.
   * @param \Drupal\Component\Datetime\TimeInterface $dateTime
   *   Date time service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, UWServiceInterface $uwService, UwNodeContent $uwNodeContent, TimeInterface $dateTime) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->uwService = $uwService;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->uwNodeContent = $uwNodeContent;
    $this->requestTime = $dateTime->getRequestTime();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uw_cfg_common.uw_service'),
      $container->get('uw_cfg_common.uw_node_content'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Setting empty array as defaults for lists and nodes.
    $lists = $nodes = [];

    // Ensure that we have default config for the block.
    // If there is not default config for this block, load in.
    if ($this->configuration['content_to_include'] === NULL) {

      // Load in default config.
      $content_to_include = $this->defaultConfiguration()['content_to_include'];
    }
    else {

      // Load in the block config.
      $content_to_include = $this->configuration['content_to_include'];
    }

    // Sort the content to include by weight.
    $content_to_include = $this->sortContentToIncludeByWeight($content_to_include);

    // Step through each of the content to get and get the node objects.
    foreach ($content_to_include as $node_type => $content) {

      // Load only if shown parameter has been set to yes.
      if ($content['shown'] == '1') {

        // Set the node objects the content.
        $nodes[$node_type] = $this->getNodes($node_type, $content['number_of_nodes'], (bool) ($this->configuration['show_all_items'] ?? FALSE));
      }
    }

    // Step through each of the nodes and set the variables to be displayed.
    foreach ($nodes as $node_type => $content) {

      // Counter to be used to know which node we are on.
      $counter = 1;

      // Step through each node and get the content.
      foreach ($content as $node) {

        // Get the content for the node from the uwService.
        $content = $this->uwNodeContent->getNodeContent(
          $node['node'],
          'teaser',
          'all'
        );

        // If this is an event, we have to substitute all the dates that
        // come from the node, to the date that is from the view.
        if ($content && $node_type == 'Events') {

          // Step through all the dates and remove ones that are not
          // from the view.
          foreach ($content['header']['date'] as $index => $date) {
            if ($date['start_date'] !== $node['date']['value']) {
              unset($content['header']['date'][$index]);
            }
          }
        }

        // If the node type is 'Opportunity', opportunity tab title
        // in multi-type list should be 'Opportunities'.
        if ($node_type === 'Opportunity') {
          $node_type = 'Opportunities';
        }

        // If there are images, we need to set extra options so that
        // the new sized images work properly.
        if (isset($content['image']) && !empty($content['image'])) {
          $content['image']['uw_index'] = $counter;
          $content['image']['uw_row_count'] = count($nodes[$node_type]);
          $content['image']['block_type'] = 'multi_type_list';
        }

        // Increment the counter so we know which node we are on.
        $counter++;

        // Now set the list content.
        $lists[$node_type][] = $content;
      }
    }

    // Show view all links if it was never configured (sites that existed
    // before this was a setting), or if it was configured to be shown.
    if (!isset($this->configuration['show_view_all']) || $this->configuration['show_view_all']) {
      $view_all['Blog'] = [
        'url' => Url::fromUri('internal:/blog')->toString(),
        'text' => 'View all blog posts',
      ];
      $view_all['Events'] = [
        'url' => Url::fromUri('internal:/events')->toString(),
        'text' => 'All upcoming events',
      ];
      $view_all['News'] = [
        'url' => Url::fromUri('internal:/news')->toString(),
        'text' => 'Read all news',
      ];
      $view_all['Opportunities'] = [
        'url' => Url::fromUri('internal:/opportunities')->toString(),
        'text' => 'View all opportunities',
      ];
    }
    else {
      $view_all = NULL;
    }

    // Setup the variable for the template.
    $lists['lists'] = $lists;
    $lists['view_all'] = $view_all;

    return [
      '#theme' => 'uw_block_multi_type_list',
      '#lists' => $lists,
    ];
  }

  /**
   * Get the nodes of a certain content type.
   *
   * @param string $node_type
   *   The type of node to get.
   * @param int $num_of_nodes
   *   Number of nodes to return.
   * @param bool $show_all_items
   *   For TRUE, return all items; otherwise, only items promoted to front page.
   *
   * @return array
   *   An array of the nodes for the specified type.
   */
  public function getNodes(string $node_type, int $num_of_nodes, bool $show_all_items = FALSE): array {

    $view_name  = '';
    $display_id = '';
    $results = [];

    // Set the view name and display id to get view's results.
    switch ($node_type) {
      case 'Blog':
        $view_name = 'uw_view_blogs';
        $display_id = 'blog_page';
        break;

      case 'Events':
        $view_name = 'uw_view_events';
        $display_id = 'event_page';
        break;

      case 'News':
        $view_name = 'uw_view_news_items';
        $display_id = 'news_page';
        break;

      case 'Opportunity':
        $view_name = 'uw_view_opportunities';
        $display_id = 'opportunities_page';
        break;
    }

    if ($view_name && $display_id) {

      // Load view and execute display id to get same results.
      $view = $this->entityTypeManager
        ->getStorage('view')
        ->load($view_name)
        ->getExecutable();

      // Show only nodes promoted to front page.
      if (!$show_all_items) {
        $view->addHandler(
          $display_id,
          'filter',
          'node_field_data',
          'promote',
          [
            'operator' => '=',
            'value' => '1',
          ]
        );
      }
      $view->getDisplay()->getHandlers('filter');

      // Set the items per page.
      $view->setItemsPerPage($num_of_nodes);

      // Applies only to Events, to show only upcoming events.
      // If other than events, remove all other exposed inputs.
      if ($node_type === 'Events') {

        // Get today's date in the correct format.
        $exposed['date']['value'] = date('m/d/Y', time());

        // Set the default todays date.
        $view->setExposedInput($exposed);
      }
      else {

        // Get all the handlers (filters) for the view.
        $handlers = $view->getHandlers('filter');

        // Step through each filter and remove it if we need to.
        // We need to do this so that exposed filters are not
        // applied on the multi-type lists.
        foreach ($handlers as $index => $handler) {

          // If the index is status, force that we are only getting
          // published nodes, even if there is a exposed filter in the
          // query string.
          if ($index == 'status') {
            $handler['value'] = 1;
            $view->setHandler($display_id, 'filter', $index, $handler);
          }
          else {

            // Remove all the rest except for the type and the
            // opportunity date in the filters.
            if (
              $index !== 'type' &&
              $index !== 'field_uw_opportunity_date_value'
            ) {
              $view->removeHandler($display_id, 'filter', $index);
            }
          }
        }
      }

      // Execute the view.
      $view->execute($display_id);

      // Step through the results and set the node in the array.
      // If it is an event add the date field that comes from
      // the view so that it can be substituted later.
      foreach ($view->result as $item) {

        // If we are on an opportunity node, ensure that the
        // application deadline has not passed.
        if ($node_type === 'Opportunity') {

          // Get the application deadline from the node.
          $app_deadline = $item->_entity->field_uw_opportunity_deadline->date;

          // If there is an application deadline, ensure
          // that it has not passed.
          if ($app_deadline) {

            // Get the application deadline in a format we
            // can compare.
            $app_date = $app_deadline->getTimestamp();

            // If the application deadline is less than the current
            // date, then skip out of the loop, not adding it to the
            // list of nodes.
            if ($app_date < $this->requestTime) {
              continue;
            }
          }
        }

        // Set the node into the array.
        $data['node'] = $item->_entity;

        // If this is an event, setup the date array so that we can
        // substitute it later.
        if ($node_type == 'Events') {

          // Set the date array, all we need is start/end date and
          // the duration, so that when we send to uwService function
          // it will get the correct date format for us.
          $data['date'] = [
            'value' => $item->node__field_uw_event_date_field_uw_event_date_value,
            'end_value' => $item->node__field_uw_event_date_field_uw_event_date_end_value ?? NULL,
            'duration' => $item->node__field_uw_event_date_field_uw_event_date_duration ?? NULL,
          ];
        }

        // Set the results to the data, so that it can be passed along.
        $results[] = $data;
      }
    }

    return $results;
  }

  /**
   * Sort the content to include for the multi type list by weight.
   *
   * @param array $content_to_include
   *   The array of content to include.
   *
   * @return array
   *   The sorted array of content to include.
   */
  private function sortContentToIncludeByWeight(array $content_to_include): array {

    // Sort the associative array of content to include.
    uasort($content_to_include, function ($item1, $item2) {
      return $item1['weight'] <=> $item2['weight'];
    });

    // Return the sorted array of content to include.
    return $content_to_include;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $items = [];

    // Get the content to include from config. Use defaults if not found.
    // We are also going to sort the content to include by weight.
    $content_to_include = $this->sortContentToIncludeByWeight($this->configuration['content_to_include']);

    // Step through each of the content to include and setup the
    // items for the table.
    foreach ($content_to_include as $key => $include) {

      // Each item for the table.
      $items[$key] = [
        'weight' => $include['weight'],
        'number_of_nodes' => $include['number_of_nodes'],
        'shown' => $include['shown'],
      ];
    }

    // The class to be used for groups.
    $group_class = 'group-order-weight';

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // Build the table.
    $form['items'] = [
      '#type' => 'table',
      '#caption' => $this->t('Items'),
      '#header' => [
        $this->t('Listing type'),
        $this->t('Number to display'),
        $this->t('Shown'),
        $this->t('Weight'),
      ],
      '#empty' => $this->t('No items.'),
      '#tableselect' => FALSE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $group_class,
        ],
      ],
    ];

    // Step through each of the items and build the rows.
    foreach ($items as $key => $item) {

      // Make each row draggable.
      $form['items'][$key]['#attributes']['class'][] = 'draggable';

      // Set the weight of the row.
      $form['items'][$key]['#weight'] = $item['weight'];

      // Listing type col.
      $form['items'][$key]['listing_type'] = [
        '#plain_text' => $key,
      ];

      // Setup the number of nodes options.
      for ($i = 3; $i <= 10; $i++) {
        $options[$i] = $i;
      }

      // The number of nodes to be displayed col.
      $form['items'][$key]['number_of_nodes'] = [
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $item['number_of_nodes'],
      ];

      // The shown col.
      $form['items'][$key]['shown'] = [
        '#type' => 'select',
        '#options' => [1 => 'Yes', 0 => 'No'],
        '#default_value' => $item['shown'],
      ];

      // Weight col.
      $form['items'][$key]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $key]),
        '#title_display' => 'invisible',
        '#default_value' => $item['weight'],
        '#attributes' => ['class' => [$group_class]],
        '#delta' => count($items[$key]),
      ];
    }

    // Configure whether to show items promoted to the front page or all.
    $form['show_all_items'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include all eligible items, not just those promoted to the home page'),
      '#default_value' => !empty($this->configuration['show_all_items']),
    ];

    // The setting for the view all buttons.
    $form['show_view_all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show "view all" buttons'),
      '#default_value' => !empty($this->configuration['show_view_all']),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Set the config for content to include.
    $this->configuration['content_to_include'] = $values['items'];

    // Configure whether to show items promoted to the front page or all.
    $this->configuration['show_all_items'] = $values['show_all_items'];

    // Set the view all button.
    $this->configuration['show_view_all'] = $values['show_view_all'];
  }

  /**
   * Returns default configuration.
   *
   * @return array
   *   The default config for this block.
   */
  public function defaultConfiguration() {
    return [
      'content_to_include' => [
        'Blog' => [
          'weight' => 3,
          'number_of_nodes' => 3,
          'shown' => 1,
        ],
        'Events' => [
          'weight' => 2,
          'number_of_nodes' => 3,
          'shown' => 1,
        ],
        'News' => [
          'weight' => 1,
          'number_of_nodes' => 3,
          'shown' => 1,
        ],
        'Opportunity' => [
          'weight' => 4,
          'number_of_nodes' => 3,
          'shown' => 1,
        ],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTags() {

    // Adding cache tags dependencies that allow everytime CRUD happens on
    // event, or news item, or blog node, cache for this block should be
    // invalidated. If a new content type is added to multi type list, then
    // this list of tags needs to be updated as well.
    return Cache::mergeTags(parent::getCacheTags(), [
      'node_list:uw_ct_blog',
      'node_list:uw_ct_event',
      'node_list:uw_ct_news_item',
      'node_list:uw_ct_opportunity',
      // Custom cache tags that can be used in cron.
      'uw_custom_blocks_cache',
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheMaxAge(): int {

    // Number of seconds cache to be considered valid. Unfortunately, max-age
    // does not work for anonymous users and the Drupal core Page Cache module.
    // @see https://www.drupal.org/docs/drupal-apis/cache-api/cache-max-age#s-limitations-of-max-age
    return self::$cacheMaxAge;
  }

}
