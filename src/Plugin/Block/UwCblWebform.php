<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\Block\WebformBlock;

/**
 * Extends the webform block.
 *
 * @Block(
 *   id = "webform_block",
 *   admin_label = @Translation("Webform"),
 *   category = @Translation("Webform")
 * )
 */
class UwCblWebform extends WebformBlock {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Get the parent form.
    $form = parent::blockForm($form, $form_state);

    // Get the position of the label display element.
    // We want to put the heading level after this.
    $keys = array_keys($form);
    $index = array_search('label_display', $keys);
    $pos = FALSE === $index ? count($form) : $index + 1;

    // Get the heading level form element.
    $new['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // Put the heading level form element before the
    // label display.
    $form = array_merge(array_slice($form, 0, $pos), $new, array_slice($form, $pos));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Submit the parent form.
    parent::blockSubmit($form, $form_state);

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Set the heading level into the block config.
    $this->configuration['heading_level'] = $values['heading_level'];
  }

}
