<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\uw_custom_blocks\Service\UwBlockAutoElements;
use Drupal\uw_custom_blocks\Service\UwBlockAutoRender;
use Drupal\uw_custom_blocks\Service\UwBlockAutoGetConfig;
use Drupal\uw_custom_blocks\Service\UwBlockService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UW Automatic listing block.
 *
 * @Block(
 *   id = "uw_cbl_automatic_list",
 *   admin_label = @Translation("Automatic list"),
 * )
 */
class UwCblAutomaticList extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * UW block service.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockService
   */
  protected $uwBlockService;

  /**
   * UW block automatic form elements.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockAutoElements
   */
  protected $uwBlockAutoElements;

  /**
   * UW block automatic render array.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockAutoRender
   */
  protected $uwBlockAutoRender;

  /**
   * UW block automatic get config.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockAutoGetConfig
   */
  protected $uwBlockAutoGetConfig;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\uw_custom_blocks\Service\UwBlockService $uwBlockService
   *   The UW block service.
   * @param \Drupal\uw_custom_blocks\Service\UwBlockAutoElements $uwBlockAutoElements
   *   The UW form elements for automatic blocks.
   * @param \Drupal\uw_custom_blocks\Service\UwBlockAutoRender $uwBlockAutoRender
   *   The UW render arrays for automatic blocks.
   * @param \Drupal\uw_custom_blocks\Service\UwBlockAutoGetConfig $uwBlockAutoGetConfig
   *   The UW get config for automatic blocks.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    UwBlockService $uwBlockService,
    UwBlockAutoElements $uwBlockAutoElements,
    UwBlockAutoRender $uwBlockAutoRender,
    UwBlockAutoGetConfig $uwBlockAutoGetConfig
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->uwBlockService = $uwBlockService;
    $this->uwBlockAutoElements = $uwBlockAutoElements;
    $this->uwBlockAutoRender = $uwBlockAutoRender;
    $this->uwBlockAutoGetConfig = $uwBlockAutoGetConfig;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('uw_custom_blocks.uw_block_service'),
      $container->get('uw_custom_blocks.uw_block_auto_elements'),
      $container->get('uw_custom_blocks.uw_block_auto_render'),
      $container->get('uw_custom_blocks.uw_block_auto_get_config')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    // Return the form_id.
    return 'uw_cbl_automatic_list';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Have at least empty build array to return.
    $build = [];

    // The block config.
    $config = $this->configuration;

    $build = $this->uwBlockAutoRender->getRenderArray(
      $config['content_type'],
      $config,
    );

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm(
    $form,
    FormStateInterface $form_state
  ) {

    // The list of form elements.
    $form = [];

    // Ensure that we can use ajax.
    $form['#tree'] = TRUE;

    // Get the config.
    $config = $this->configuration;

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($config);

    // The content type form element.
    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of content'),
      '#options' => $this->uwBlockService->getAutomaticContentTypes(),
      '#description' => $this->t('Select the content to list'),
      '#required' => TRUE,
      '#empty_option' => '-- Select --',
      '#default_value' => $config['content_type'] ?? '',
    ];

    // Get the rest of the form elements.
    $this->uwBlockAutoElements->getAutoFormElements($form, $config);

    // Add library to the form.
    $form['#attached']['library'][] = 'uw_custom_blocks/uw_cbl_automatic_list';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit(
    $form,
    FormStateInterface $form_state
  ) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Get the content type form the form state values.
    $content_type = $values['content_type'];

    // Step through each of the content types and reset
    // the block config for each content type.
    foreach (array_keys($this->uwBlockService->getAutomaticContentTypes()) as $ct) {
      $this->configuration[$ct] = [];
    }

    // Set the content type and heading level
    // in the block config.
    $this->configuration['content_type'] = $content_type;
    $this->configuration['heading_level'] = $values['heading_level'];

    // Get the block settings config.
    $this->configuration[$content_type] = $this->uwBlockAutoGetConfig->getConfig($values);
  }

}
