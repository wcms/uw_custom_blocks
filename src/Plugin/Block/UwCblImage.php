<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\Config;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\uw_cfg_common\Service\UWService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UW custom block image.
 *
 * @Block(
 *   id = "uw_cbl_image",
 *   admin_label = @Translation("Image"),
 * )
 */
class UwCblImage extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UW node content.
   *
   * @var \Drupal\uw_cfg_common\Service\UwService
   */
  protected $uwService;

  /**
   * ImageFactory instance.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * The system file config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $systemFileConfig;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file url generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\uw_cfg_common\Service\UwService $uwService
   *   UW Service.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   Image Factory.
   * @param \Drupal\Core\Config\Config $system_file_config
   *   The system file configuration.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file system.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    UwService $uwService,
    ImageFactory $image_factory,
    Config $system_file_config,
    StreamWrapperManagerInterface $stream_wrapper_manager,
    FileSystemInterface $file_system,
    FileUrlGeneratorInterface $file_url_generator
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->uwService = $uwService;
    $this->imageFactory = $image_factory;
    $this->systemFileConfig = $system_file_config;
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->fileSystem = $file_system;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uw_cfg_common.uw_service'),
      $container->get('image.factory'),
      $container->get('config.factory')->get('system.file'),
      $container->get('stream_wrapper_manager'),
      $container->get('file_system'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];

    // Load the media entity from the image selected.
    $media = $this->entityTypeManager->getStorage('media')->load($this->configuration['image']);
    // Check if media exits.
    if ($media == NULL) {

      // Return if media is empty.
      return $build;
    }
    // Load in values for the full width image.
    $output = [
      'caption' => $media->field_uw_image_caption->value,
      'alt' => $media->field_media_image->alt,
      'type' => $this->configuration['type_of_image'],
    ];

    // If we have a full image, get the variables for it.
    if ($this->configuration['type_of_image'] == 'full') {

      // Load in values for the full width image.
      $output['image'] = $this->uwService->prepareResponsiveImage($media, 'uw_ris_media');
    }
    else {

      // Load in values for the sized image.
      $output['image'] = $this->configuration['scaled_image'];
      $output['alignment'] = $this->configuration['image_alignment'];
      $output['sized_type'] = $this->configuration['type_of_sized_image'];

    }
    // Set caption values to output.
    $output['caption_format'] = $this->configuration['caption_format'] ?? 'underneath_photo';
    $output['caption_alignment'] = $this->configuration['caption_alignment'] ?? 'left';

    // If the uri is not set, then set it.
    if (!isset($output['image']['uri'])) {
      $output['image']['uri'] = $this->fileUrlGenerator->generate($output['image']['dirname'] . '/' . $output['image']['basename'])->toString();
    }

    // Load values in build array.
    $build = [
      '#theme' => 'uw_block_image',
      '#image' => $output,
    ];

    // Return the build array.
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // Fieldset for the image settings.
    $form['image_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Image settings'),
    ];

    // Select list for type of sized image.
    $form['image_settings']['type_of_image'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of image'),
      '#options' => [
        '' => $this->t('-- Select --'),
        'full' => $this->t('Full width'),
        'sized' => $this->t('Sized image'),
      ],
      '#default_value' => $this->configuration['type_of_image'] ?? '',
      '#required' => TRUE,
    ];

    // The type of sized image.
    $form['image_settings']['type_of_sized_image'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of sized image'),
      '#options' => [
        '' => $this->t('-- Select --'),
        'original' => $this->t('Original'),
        'custom' => $this->t('Custom'),
      ],
      '#default_value' => $this->configuration['type_of_sized_image'] ?? '',
      '#states' => [
        'visible' => [
          'select[name="settings[image_settings][type_of_image]"]' => ['value' => 'sized'],
        ],
        'required' => [
          'select[name="settings[image_settings][type_of_image]"]' => ['value' => 'sized'],
        ],
      ],
    ];

    // Select list for width of image.
    $form['image_settings']['scale_by'] = [
      '#type' => 'select',
      '#title' => $this->t('Dimension to scale'),
      '#options' => [
        '' => $this->t('-- Select --'),
        'height' => $this->t('Height'),
        'width' => $this->t('Width'),
      ],
      '#default_value' => $this->configuration['scale_by'] ?? '',
      '#states' => [
        'visible' => [
          'select[name="settings[image_settings][type_of_sized_image]"]' => ['value' => 'custom'],
        ],
        'required' => [
          'select[name="settings[image_settings][type_of_sized_image]"]' => ['value' => 'custom'],
        ],
      ],
    ];

    // The width to scale to.
    $form['image_settings']['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width to scale'),
      '#description' => $this->t('Note: images will not be scaled larger than their actual dimensions.'),
      '#default_value' => $this->configuration['width'] ?? '',
      '#states' => [
        'visible' => [
          'select[name="settings[image_settings][type_of_sized_image]"]' => ['value' => 'custom'],
          'select[name="settings[image_settings][scale_by]"]' => ['value' => 'width'],
        ],
        'required' => [
          'select[name="settings[image_settings][type_of_sized_image]"]' => ['value' => 'custom'],
          'select[name="settings[image_settings][scale_by]"]' => ['value' => 'width'],
        ],
      ],
    ];

    // The height to scale to.
    $form['image_settings']['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height to scale'),
      '#description' => $this->t('Note: images will not be scaled larger than their actual dimensions.'),
      '#default_value' => $this->configuration['height'] ?? '',
      '#states' => [
        'visible' => [
          'select[name="settings[image_settings][type_of_sized_image]"]' => ['value' => 'custom'],
          'select[name="settings[image_settings][scale_by]"]' => ['value' => 'height'],
        ],
        'required' => [
          'select[name="settings[image_settings][type_of_sized_image]"]' => ['value' => 'custom'],
          'select[name="settings[image_settings][scale_by]"]' => ['value' => 'height'],
        ],
      ],
    ];

    // The alignment of the image.
    $form['image_settings']['image_alignment'] = [
      '#type' => 'select',
      '#title' => $this->t('Image alignment'),
      '#options' => [
        'center' => $this->t('Center'),
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
      ],
      '#default_value' => $this->configuration['image_alignment'] ?? '',
      '#states' => [
        'invisible' => [
          'select[name="settings[image_settings][type_of_sized_image]"]' => ['value' => ''],
        ],
        'required' => [
          ['select[name="settings[image_settings][type_of_sized_image]"]' => ['value' => 'original']],
          'or',
          ['select[name="settings[image_settings][type_of_sized_image]"]' => ['value' => 'custom']],
        ],
      ],
    ];

    // Fieldset for the caption settings.
    $form['caption_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Caption settings'),
    ];

    // Select list for caption format.
    $form['caption_settings']['caption_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Caption format'),
      '#options' => [
        'underneath_photo' => $this->t('Underneath photo'),
        'hidden' => $this->t('Hidden'),
      ],
      '#default_value' => $this->configuration['caption_format'] ?? 'underneath_photo',
      '#required' => TRUE,
    ];

    // Select list for caption alignment.
    $form['caption_settings']['caption_alignment'] = [
      '#type' => 'select',
      '#title' => $this->t('Caption alignment'),
      '#options' => [
        'left' => $this->t('Left'),
        'center' => $this->t('Center'),
        'right' => $this->t('Right'),
      ],
      '#default_value' => $this->configuration['caption_alignment'] ?? 'left',
      '#required' => TRUE,
      '#states' => [
        'invisible' => [
          'select[name="settings[caption_settings][caption_format]"]' => ['value' => 'hidden'],
        ],
      ],
    ];

    // The media library for image.
    $form['image'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => ['uw_mt_image'],
      '#title' => $this->t('Upload your image'),
      '#default_value' => $this->configuration['image'] ?? '',
      '#states' => [
        'invisible' => [
          'select[name="settings[image_settings][type_of_image]"]' => ['value' => ''],
        ],
      ],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // If the type of image is sized and the type
    // of sized image is blank, set the error.
    if (
      $values['image_settings']['type_of_image'] == 'sized' &&
      $values['image_settings']['type_of_sized_image'] == ''
    ) {

      $form_state->setError(
        $form['image_settings']['type_of_sized_image'],
        $this->t('You must select a type of sized image.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Set the values that are needed and required.
    $this->configuration['image'] = $values['image'];
    $this->configuration['type_of_image'] = $values['image_settings']['type_of_image'];

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // If this is sized image, set the config.
    if ($this->configuration['type_of_image'] == 'sized') {

      // Set the type of sized image.
      $this->configuration['type_of_sized_image'] = $values['image_settings']['type_of_sized_image'];

      // Set the image alignment.
      $this->configuration['image_alignment'] = $values['image_settings']['image_alignment'];

      // If this is a custom sized image, set the config.
      if ($this->configuration['type_of_sized_image'] == 'custom') {

        // Set the scaled by, either height or width.
        $this->configuration['scale_by'] = $values['image_settings']['scale_by'];

        // Set the config for either height or width and the scaled image.
        if ($this->configuration['scale_by'] == 'width') {

          // Get the new image info.
          $image = $this->getImageInfo($values['image'], 'width', $values['image_settings']['width']);

          // Set the variables.
          $this->configuration['width'] = $values['image_settings']['width'];
          $this->configuration['scaled_image'] = $this->createSizedFile($image);
        }
        else {

          // Get the new image info.
          $image = $this->getImageInfo($values['image'], 'height', $values['image_settings']['height']);

          // Set the variables.
          $this->configuration['height'] = $values['image_settings']['height'];
          $this->configuration['scaled_image'] = $this->createSizedFile($image);
        }
      }
      else {

        // Reset the other config.
        $this->configuration['width'] = NULL;
        $this->configuration['height'] = NULL;

        // Get the info about the image.
        $image_info = $this->getImageInfo($this->configuration['image'], 'original');

        // Create the URL to the original.
        $image_info['uri'] = $this->fileUrlGenerator->generate($image_info['dirname'] . '/' . $image_info['basename'])->toString();

        // Set the config for the image.
        $this->configuration['scaled_image'] = $image_info;
      }
    }
    else {

      // Reset all the other config.
      $this->configuration['scale_by'] = NULL;
      $this->configuration['width'] = NULL;
      $this->configuration['height'] = NULL;
      $this->configuration['image_alignment'] = NULL;
      $this->configuration['scaled_image'] = NULL;
    }

    // Set the values that are needed and required to caption settings.
    $this->configuration['caption_format'] = $values['caption_settings']['caption_format'];
    $this->configuration['caption_alignment'] = $values['caption_settings']['caption_alignment'];

  }

  /**
   * Get info about the image.
   *
   * @param int $mid
   *   The media id of the image.
   * @param string $type
   *   The type of dimension that is going to be scaled.
   * @param int $dimension
   *   The actual value of the dimension to be scaled.
   *
   * @return array
   *   Array of info about the image.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getImageInfo(int $mid, string $type, int $dimension = NULL): array {

    // Load the media entity from the image selected.
    $media = $this->entityTypeManager->getStorage('media')->load($mid);

    // Get the fid.
    $fid = $media->field_media_image->target_id;

    // Load the file.
    $file = $this->entityTypeManager->getStorage('file')->load($fid);

    // Set the local path, which is the file uri.
    $image['local_path'] = $file->getFileUri();

    // Get the image size.
    if (is_file($image['local_path'])) {

      // Get the image size.
      $image_size = @getimagesize($image['local_path']);

      $image['actual_width'] = (int) $image_size[0];
      $image['actual_height'] = (int) $image_size[1];
    }

    // If the type is width, set width can calculate height.
    // If the type is height, set height and calculate width.
    if ($type == 'width') {

      // Set the width.
      $image['scaled_width'] = $dimension;

      // Only calculate height if the actual width is larger
      // than the scaled width.
      // If not just use the actual width, so we do not upscale.
      if ($image['actual_width'] > $image['scaled_width']) {
        $ratio = $image['scaled_width'] / $image['actual_width'];
        $image['scaled_height'] = (int) ($image['actual_height'] * $ratio);
      }
      else {
        $image['scaled_height'] = (int) $image['actual_height'];
        $image['scaled_width'] = (int) $image['actual_width'];
      }
    }
    else {

      // Set the height.
      $image['scaled_height'] = $dimension;

      // Only calculate width if the actual height is larger
      // than the scaled height.
      // If not just use the actual height, so we do not upscale.
      if ($image['actual_height'] > $image['scaled_height']) {
        $ratio = $image['scaled_height'] / $image['actual_height'];
        $image['scaled_width'] = (int) ($image['actual_width'] * $ratio);
      }
      else {
        $image['scaled_height'] = (int) $image['actual_height'];
        $image['scaled_width'] = (int) $image['actual_width'];
      }
    }

    // If this is an original image, just return the path.
    if ($type == 'original') {
      return $this->getPathInfo($image['local_path']);
    }

    // Set the file counter, going to be used to create
    // a distinctive name for the file.
    $file_counter = 0;

    // Keep looping until we get a distinctive file name.
    do {

      // Get all the path info.
      $path_info = $this->getPathInfo($image['local_path']);

      // Get the local file path, which is the same path as the
      // original file, with resize in front, as well as adding
      // the dimensions to the file name.
      // Ex. original file sites/default/files/uploads/toronto.jpg,
      // will now be:
      // uploads/resize/toronto-<width>x<height>_<file_counter>.jpg.
      $local_file_path = 'uploads/resize/' . $path_info['filename'] . '-' . $image['scaled_width'] . 'x' . $image['scaled_height'];

      // If we are not on the first iteration, add the file counter,
      // so that we are going to get a distinctive file name.
      if ($file_counter > 0) {
        $local_file_path .= '_' . $file_counter;
      }

      // Add the file extension.
      $local_file_path .= '.' . $path_info['extension'];

      // Now set the destination path, which is the schema and file path.
      $image['destination'] = $path_info['scheme'] . '://' . $local_file_path;

      // If the file exists, set the flag appropriately.
      // On successive runs, the file name will get distinctive.
      if (file_exists($image['destination'])) {
        $file_exists = TRUE;
        $file_counter++;
      }
      else {
        $file_exists = FALSE;
      }

    } while ($file_exists);

    return $image;
  }

  /**
   * Function to create a resized file.
   *
   * @param array $image
   *   Array of info about an image.
   *
   * @return string
   *   The URL of the resized file.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createSizedFile(array $image): array {

    // If the file does not exist, then create it.
    if (!file_exists($image['destination'])) {

      // Create the resize directory.
      $directory = dirname($image['destination']);
      $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);

      // Copy the file.
      $copy = $this->fileSystem->copy($image['local_path'], $image['destination']);

      // Get the copied file.
      $res = $this->imageFactory->get($copy);

      // If there is a copied file, resize it and save it.
      // If there is no copied file or type doesn't match
      // extension or invalid, keep original file.
      if ($res) {

        // The image has been loaded, resize it.
        $res->resize($image['scaled_width'], $image['scaled_height']);

        // Save the image.
        $res->save();
      }
      else {

        // Keep the original file in the destination.
        $handle = fopen($image['destination'], 'w');
        fwrite($handle, file_get_contents($image['local_path']));
        fclose($handle);
      }
    }

    // Change the mod of the file.
    @chmod($image['destination'], 0664);

    // Return the info of the scaled image.
    return [
      'url' => $this->fileUrlGenerator->generate($image['destination'])->toString(),
      'uri' => $image['destination'],
      'width' => $image['scaled_width'],
      'height' => $image['scaled_height'],
    ];
  }

  /**
   * Function to get the path info.
   *
   * @param string $uri
   *   The file uri.
   *
   * @return array
   *   Array of path info.
   */
  public function getPathInfo(string $uri): array {

    // Get the info about the path.
    $info = pathinfo($uri);

    // Set all the path info variables.
    $info['extension'] = substr($uri, strrpos($uri, '.') + 1);
    $info['basename'] = basename($uri);
    $info['filename'] = basename($uri, '.' . $info['extension']);
    $info['scheme'] = $this->streamWrapperManager->getScheme($uri);

    // Get the stream wrappers.
    $stream_wrappers = $this->streamWrapperManager->getWrappers();

    // If the scheme is empty, we have to set something, so
    // try and get the default.
    if (empty($info['scheme'])) {

      // Step through each of the available stream wrappers,
      // and check if it applies.
      foreach ($stream_wrappers as $scheme => $stream_wrapper) {

        // Get the base path of the scheme.
        $scheme_base_path = $this->streamWrapperManager->getViaScheme($scheme)->getDirectoryPath();

        // Set blank matches, so we have something to check.
        $matches = [];

        // If there are matches for the scheme, set the variables.
        if (preg_match('/^' . preg_quote($scheme_base_path, '/') . '\/?(.*)/', $info['dirname'], $matches)) {
          $info['scheme'] = $scheme;
          $info['dirname'] = $scheme . '://' . $matches[1];
          break;
        }
      }
    }

    // Return the info.
    return $info;
  }

}
