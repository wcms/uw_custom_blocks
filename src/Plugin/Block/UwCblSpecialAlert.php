<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * UW custom block that provides a 'Special alert' block.
 *
 * @Block(
 *    id = "uw_cbl_special_alert",
 *    admin_label = @Translation("Special alert"),
 *  )
 */
class UwCblSpecialAlert extends BlockBase {

  /**
   * Config settings.
   *
   * @var string
   */
  public const SETTINGS = 'uw_custom_blocks.special_alert.settings';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'display_option' => FALSE,
      'display_style' => FALSE,
      'date' => FALSE,
      'time' => FALSE,
      'label_display' => FALSE,
      'message' => [
        'value' => '',
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // The blocks' content is only message field.
    // The rest of the fields the user modifies, is configuration.
    $message = $this->configuration['message'];

    return [
      '#type' => 'processed_text',
      '#text' => $message['value'],
      '#format' => $message['format'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // The display option form element.
    $form['display_option'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display this alert'),
      '#default_value' => $this->configuration['display_option'],
      '#weight' => '0',
    ];

    // The message form element.
    $form['display_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Display style'),
      '#options' => [
        'default' => $this->t('Default'),
        'priority' => $this->t('Priority'),
      ],
      '#default_value' => $this->configuration['display_style'] ?? 'default',
    ];

    $default_date = NULL;
    if (isset($this->configuration['date']) && $this->configuration['date']) {
      $default_date = DrupalDateTime::createFromTimestamp($this->configuration['date']);
    }

    $default_time = NULL;
    if (isset($this->configuration['time']) && $this->configuration['time']) {
      $default_time = DrupalDateTime::createFromTimestamp($this->configuration['time']);
    }

    $form['date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Date'),
      '#default_value' => $default_date,
      '#date_time_format' => 'short',
      '#date_timezone' => 'America/Toronto',
      '#date_date_element' => 'date',
      '#date_time_element' => 'none',
      '#date_year_range' => '-0:+1',
    ];

    $form['time'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Time'),
      '#description' => $this->t('Optionally enter a date and time to display with this alert.'),
      '#default_value' => $default_time,
      '#date_time_format' => 'short',
      '#date_timezone' => 'America/Toronto',
      '#date_date_element' => 'none',
      '#date_time_element' => 'time',
    ];

    $form['label_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display title'),
      '#default_value' => $this->configuration['label_display'] === 'visible',
    ];

    $form['message'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_basic',
      '#allowed_formats' => ['uw_tf_basic'],
      '#title' => $this->t('Special alert message'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['message']['value'],
      '#weight' => '1',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    // Display error when time has a value, but date is empty.
    if ($form_state->getValue('date') === NULL && $form_state->getValue('time')) {
      $form_state->setErrorByName('date', $this->t('Date is required when time is used.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Get the values from the form.
    $values = $form_state->getValues();

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Save the settings to the block configuration that do not get save by
    // default, as they are custom fields.
    $this->configuration['display_option'] = $values['display_option'];
    $this->configuration['message'] = $values['message'];
    $this->configuration['display_style'] = $values['display_style'];

    // Saving date/time as timestamp, it may be easier to compare.
    $this->configuration['date'] = NULL;
    if ($dt = $form_state->getValue('date')) {
      $this->configuration['date'] = $dt->getTimestamp();
    }

    $this->configuration['time'] = NULL;
    if ($dt = $form_state->getValue('time')) {
      $this->configuration['time'] = $dt->getTimestamp();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    // This is what actually controls if special alert gets displayed. If user
    // access is forbidden no block content will be displayed. Checking for
    // permission "content access" is redundant, since anonymous and
    // authenticated users both have that permission.
    if ($this->configuration['display_option']) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

}
