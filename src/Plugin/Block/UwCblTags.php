<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UW custom block tags.
 *
 * @Block(
 *   id = "uw_cbl_tags",
 *   admin_label = @Translation("Tags"),
 * )
 */
class UwCblTags extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // At least a blank tags array.
    $tags = [];

    // Get the tags from the block config.
    $tag_info = $this->configuration['tags'];

    // Step through each tag and get the info.
    foreach ($tag_info as $tag) {

      // Load the taxonomy term.
      $term = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->load($tag['tag']);
      $title = $term->label();
      if ($term->bundle() == 'uw_vocab_faculties_and_schools') {
        $title = uw_get_short_faculty_tag($title);
      };
      // Set the tag info.
      $tags[] = [
        'url' => NULL,
        'title' => $title,
        'faculty' => uw_get_org_class_from_term($term),
        'type' => 'full',
      ];
    }

    // Return the build array.
    return [
      '#theme' => 'uw_block_tags',
      '#items' => $tags,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Array to store the tags.
    $tags = [];

    // Adding the ability to do ajax forms.
    $form['#tree'] = TRUE;

    // Setup the add_more_called flag, so that we know
    // which of the items to load.
    // If we even do one add or remove, we set this flag
    // so that the form will load the values from the
    // form_state, if flag is not set, it will load
    // from the block config.
    if (empty($form_state->get('add_more_called'))) {
      $add_more_called = FALSE;
      $form_state->set('add_more_called', $add_more_called);
    }
    else {
      $add_more_called = $form_state->get('add_more_called');
    }

    // If the add_more_flag is not set, we load from the
    // block config.  If it is set, we load from the form_state.
    if (!$add_more_called) {
      $tags = $this->configuration['tags'] ?? NULL;
      $form_state->set('tags', $tags);
    }
    else {

      $tags = $form_state->get('tags');
    }

    // Get the num_of_rows from the form_state.
    $num_of_rows = $form_state->get('num_of_rows');

    // If the num_of_rows is not set, we first look at
    // the block config and see if we have links, and if
    // so set num_of_rows to the number of links.  If we
    // do not have num_of_rows set, default to 1 (this is
    // the first load of the form).
    if (empty($num_of_rows)) {
      if (isset($tags)) {
        $num_of_rows = count($tags);
      }
      else {
        $num_of_rows = 1;
      }
    }

    // Set the num_of_rows to the form_state so that we
    // can use it in the ajax calls.
    $form_state->set('num_of_rows', $num_of_rows);

    // The class to be used for groups.
    $group_class = 'group-order-weight';

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // The field set for the links groups.
    $form['items_fieldset'] = [
      '#type' => 'container',
      '#prefix' => '<div id="items-wrapper">',
      '#suffix' => '</div>',
    ];

    // Build the table.
    $form['items_fieldset']['items'] = [
      '#type' => 'table',

      '#header' => [
        [
          'data' => $this->t('<h4 class="label">Add tags</h4>'),
          'colspan' => 2,
        ],
        $this->t('Weight'),
      ],
      '#empty' => $this->t('<span class="form-required">You must have at least one tag.</span>'),
      '#tableselect' => FALSE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $group_class,
        ],
      ],
      '#prefix' => '<div class="uw-tags__table">',
      '#suffix' => '</div>',
    ];

    // Step through each of the rows and add tag info.
    for ($i = 0; $i < $num_of_rows; $i++) {

      // Reset the settings array.
      $settings = [];

      // Set the table to be draggable.
      $settings['#attributes']['class'][] = 'draggable';

      // Set the weight.
      $settings['#weight'] = $i;

      // The first column of the table, that will house the arrows
      // for rearranging tags.
      $settings['arrow'] = [
        '#type' => 'markup',
        '#markup' => '',
        '#title_display' => 'invisible',
      ];

      // Ensure that we have a blank term for default value.
      $term = '';

      // If there is a tag, get the term entity for the
      // default value.
      if (isset($tags[$i]['tag']) && $tags[$i]['tag'] !== NULL) {

        // Load the term based on the block config.
        $term = $this->entityTypeManager
          ->getStorage('taxonomy_term')
          ->load($tags[$i]['tag']);
      }

      // The tag element.
      $settings['tag'] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'taxonomy_term',
        '#title' => $this->t('Tag'),
        '#tags' => FALSE,
        '#default_value' => $term,
        '#selection_handler' => 'views',
        '#selection_settings' => [
          'view' => [
            'view_name' => 'uw_view_tags',
            'display_name' => 'tags_reference',
            'arguments' => [],
          ],
          'match_operator' => 'CONTAINS',
        ],
        '#maxlength' => NULL,
      ];

      // The weight element.
      $settings['weight'] = [
        '#type' => 'weight',
        '#title_display' => 'invisible',
        '#default_value' => $i,
        '#attributes' => ['class' => [$group_class]],
        '#delta' => $num_of_rows,
      ];

      // Add the settings to the items array, which is full row
      // in the table.
      $form['items_fieldset']['items'][] = $settings;
    }

    // The add tags group button.
    $form['add_group'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add tag'),
      '#submit' => [[$this, 'tagAddOne']],
      '#ajax' => [
        'callback' => [$this, 'tagCallback'],
        'wrapper' => 'items-wrapper',
      ],
      '#attributes' => [
        'class' => ['button--large'],
      ],
      '#suffix' => '<p>Leave a tag blank to remove it.</p>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Get the tags from the form state.
    $tag_info = $values['items_fieldset']['items'];

    // Have at least blank array of tags.
    $tags = [];

    // Add all the tags that have values.
    foreach ($tag_info as $tag) {
      if ($tag['tag']) {
        $tags[] = $tag;
      }
    }

    // If there are no tags, set the error message.
    if (count($tags) == 0) {

      // Set the error message about having at least one tag.
      $form_state->setError(
        $form['items_fieldset']['items'][0]['tag'],
        $this->t('You must enter at least one tag.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Array of tags, at least blank so no undefined index.
    $tag_info = $values['items_fieldset']['items'];

    // Sort the array by weight.
    $tag_info = uw_sort_array_by_weight($tag_info);

    // Ensure that we are only adding tags that have values.
    foreach ($tag_info as $tag) {
      if ($tag['tag']) {
        $tags[] = $tag;
      }
    }

    // Save the ecs in the block config.
    $this->configuration['tags'] = $tags;
  }

  /**
   * Add one more tag to the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function tagAddOne(array &$form, FormStateInterface $form_state) {

    // Increase by 1 the number of rows and set it in
    // the form_state.
    $num_of_rows = $form_state->get('num_of_rows');
    $num_of_rows++;
    $form_state->set('num_of_rows', $num_of_rows);

    // Set the add_more_called flag, so that we will load
    // the items by form_state, rather than block config.
    $form_state->set('add_more_called', TRUE);

    // Get the current values of the form_state.
    $values = $form_state->getValues();

    // Have at least blank array of tags to return.
    $tags = [];

    // Get the items from the form state, which will contain
    // the tags settings.
    $tag_info = $values['settings']['items_fieldset']['items'];

    // Ensure that we are only adding tags that have values.
    foreach ($tag_info as $tag) {
      if ($tag['tag']) {
        $tags[] = $tag;
      }
    }

    // Set the tags from the items in the form_state, so that we
    // will load newly added values.
    $form_state->set('tags', $tags);

    // Rebuild form with 1 extra row.
    $form_state->setRebuild();
  }

  /**
   * The ajax call back for tag.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The form element to be overwritten.
   */
  public function tagCallback(array &$form, FormStateInterface $form_state) {
    return $form['settings']['items_fieldset'];
  }

}
