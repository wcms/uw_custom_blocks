<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UW custom block ofis expert search.
 *
 * @Block(
 *   id = "uw_cbl_ofis_expert_search",
 *   admin_label = @Translation("OFIS expert search"),
 * )
 */
class UwCblOfisExpertSearch extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Form builder will be used via Dependency Injection.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The uuid service.
   *
   * @var \Drupal\Core\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Component\Uuid\UuidInterface $uuidService
   *   The uuid service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    FormBuilderInterface $form_builder,
    UuidInterface $uuidService
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
    $this->uuidService = $uuidService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
      $container->get('uuid')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Get the form.
    $search['form'] = $this->formBuilder->getForm(
      'Drupal\uw_custom_blocks\Form\OfisExpertSearchForm',
      $this->configuration['departmental_search'],
      $this->configuration['uuid'],
    );

    // Set the pre-search notice.
    $search['presearch_notice'] = $this->configuration['presearch_notice'] ?? NULL;

    // Set the uuid.
    $search['uuid'] = $this->configuration['uuid'];

    // If there is no block title, then set the label so that
    // the template will know to place a visually hidden one.
    if (!$this->configuration['label_display']) {
      $search['label'] = $this->t('OFIS expert search');
    }

    // Return the build array with the template ane variables.
    return [
      '#theme' => 'uw_block_ofis_expert_search',
      '#search' => $search,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // The departmental search form field.
    $form['departmental_search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Departmental search'),
      '#description' => $this->t('Allowing search for specific departments. Example "eng", or "sci".'),
      '#default_value' => $this->configuration['departmental_search'] ?? 'eng',
      '#required' => TRUE,
    ];

    // The pre-search notice form field.
    $form['presearch_notice'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pre-search notice'),
      '#description' => $this->t('Text to display in the search results area before a search is performed.'),
      '#default_value' => $this->configuration['presearch_notice'] ?? 'Start searching to view results.',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Get the departmental search and uuid.
    $this->configuration['departmental_search'] = $form_state->getValue('departmental_search');
    $this->configuration['presearch_notice'] = $form_state->getValue('presearch_notice');
    $this->configuration['uuid'] = $this->uuidService->generate();
  }

}
