<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UW custom block expand collapse.
 *
 * @Block(
 *   id = "uw_cbl_expand_collapse",
 *   admin_label = @Translation("Expand Collapse"),
 * )
 */
class UwCblExpandCollapse extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The list of available modules.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $extensionListModule;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The variable for current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   The currentPath.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The list of available modules.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    CurrentPathStack $currentPath,
    ModuleExtensionList $extension_list_module
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->currentPath = $currentPath;
    $this->extensionListModule = $extension_list_module;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('path.current'),
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Get the ecs from configuration.
    $ecs = $this->configuration['ecs'];

    // The core view builder.
    $view_builder = $this->entityTypeManager->getViewBuilder('node');

    // Step through each of the e/c sections and get out the
    // layout builder from the e/c section node.
    foreach ($ecs as $ec) {

      if (isset($ec['group'])) {

        // Load the node.
        $node = $this->entityTypeManager->getStorage('node')->load($ec['group']);

        // Ensure that we have a node that is loaded, this check
        // is here in case someone deletes a E/C node.
        if ($node && $node->isPublished()) {

          // Set the variables to be used in the template.
          $exp_col['items'][] = [
            'content' => $view_builder->view($node, 'full'),
            'heading_text' => $node->label(),
          ];
        }
      }
    }

    // Set the flag to show the exp/col all buttons.
    if (count($ecs) > 1) {
      $exp_col['show_exp_col_all'] = TRUE;
    }
    else {
      $exp_col['show_exp_col_all'] = FALSE;
    }

    // Set the uuid so that we can handle multiple E/C.
    $exp_col['uuid'] = uniqid();

    // Get the heading selector from the block configuration.
    $exp_col['heading_selector'] = $this->configuration['heading_selector'];

    return [
      '#theme' => 'uw_block_expand_collapse',
      '#exp_col' => $exp_col,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $ecs = [];

    // Adding the ability to do ajax forms.
    $form['#tree'] = TRUE;

    // Setup the add_more_called flag, so that we know
    // which of the items to load.
    // If we even do one add or remove, we set this flag
    // so that the form will load the values from the
    // form_state, if flag is not set, it will load
    // from the block config.
    if (empty($form_state->get('add_more_called'))) {
      $add_more_called = FALSE;
      $form_state->set('add_more_called', $add_more_called);
    }
    else {
      $add_more_called = $form_state->get('add_more_called');
    }

    // If the add_more_flag is not set, we load from the
    // block config.  If it is set, we load from the form_state.
    if (!$add_more_called) {
      $ecs = $this->configuration['ecs'] ?? NULL;
      $form_state->set('ecs', $ecs);
    }
    else {

      $ecs = $form_state->get('ecs');
    }

    // Get the num_of_rows from the form_state.
    $num_of_rows = $form_state->get('num_of_rows');

    // If the num_of_rows is not set, we first look at
    // the block config and see if we have ecs, and if
    // so set num_of_rows to the number of ecs.  If we
    // do not have num_of_rows set, default to 1 (this is
    // the first load of the form).
    if (empty($num_of_rows)) {
      if (isset($ecs)) {
        $num_of_rows = count($ecs);
      }
      else {
        $num_of_rows = 1;
      }
    }

    // Reset the node since we used it above and if we don't
    // we get a node loaded in the e/c section on first form load.
    $node = NULL;

    // Set the num_of_rows to the form_state so that we
    // can use it in the ajax calls.
    $form_state->set('num_of_rows', $num_of_rows);

    // The class to be used for groups.
    $group_class = 'group-order-weight';

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // The field set for the E/C groups.
    $form['items_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Expand/collapse groups'),
      '#prefix' => '<div id="items-wrapper">',
      '#suffix' => '</div>',
    ];

    // The heading selector element.
    $form['items_fieldset']['heading_selector'] = [
      '#title' => $this->t('Heading selector'),
      '#type' => 'select',
      '#options' => [
        'h2' => $this->t('h2'),
        'h3' => $this->t('h3'),
        'h4' => $this->t('h4'),
        'h5' => $this->t('h5'),
        'h6' => $this->t('h6'),
      ],
      '#default_value' => $this->configuration['heading_selector'] ?? '',
      '#required' => TRUE,
    ];

    // Build the table.
    $form['items_fieldset']['items'] = [
      '#type' => 'table',
      '#header' => [
        '',
        $this->t('Group'),
        // @todo Make this work properly with the remove button.
        // phpcs:disable
        // '',
        // phpcs:enable
        $this->t('Weight'),
      ],
      '#empty' => $this->t('<span class="form-required">You must have at least one expand/collapse group.</span>'),
      '#tableselect' => FALSE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $group_class,
        ],
      ],
      '#prefix' => '<div class="uw-exp-col__table">',
      '#suffix' => '</div><p>Leave a group blank to remove it.</p>',
    ];

    for ($i = 0; $i < $num_of_rows; $i++) {

      // Reset the settings array.
      $settings = [];

      // Set the table to be draggable.
      $settings['#attributes']['class'][] = 'draggable';

      // Set the weight.
      $settings['#weight'] = $i;

      // Reset null so that we dont get a value if there is a
      // blank row in the table.
      $node = NULL;

      // If there is an e/c group, load in the node.  If
      // there is not, we are on an add more call with no group
      // set yet.
      if (!empty($ecs[$i]['group']) && ($nid = (int) $ecs[$i]['group']) !== 0) {

        // Load the node based on the nid.
        $node = $this->entityTypeManager->getStorage('node')->load($nid);

        if ($node) {
          $node = EntityAutocomplete::getEntityLabels([$node]);
        }
        // If node is not loaded, skip ec block.
        else {
          continue;
        }
      }

      // The first column of the table, that will house the arrows
      // for rearranging e/c groups.
      $settings['arrow'] = [
        '#type' => 'markup',
        '#markup' => '',
        '#title_display' => 'invisible',
      ];

      // Container for the group.
      $settings['group_info'] = [
        '#type' => 'container',
        '#prefix' => '<div class="uw-exp-col__group-info">',
        '#suffix' => '</div>',
      ];

      // Wrapper id for the operations links.
      $wrapper_id = 'operations-wrapper-' . $i;

      // The group element, which is an autocomplete using the nid
      // of a uw_ct_expand_collapse_group content type.
      $settings['group_info']['group'] = [
        '#type' => 'textfield',
        '#autocomplete_route_name' => 'uw_ct_expand_collapse_group.autocomplete',
        '#title' => '',
        '#attributes' => [
          'class' => ['uw-exp-col__group'],
        ],
        '#default_value' => $node ?? NULL,
        '#ajax' => [
          'callback' => [$this, 'updateOperationsLinks'],
          'disable-refocus' => FALSE,
          'event' => 'change',
          'wrapper' => $wrapper_id,
          'element_id' => $i,
        ],
        '#prefix' => '<div class="form-required">',
        '#suffix' => '</div>',
        '#maxlength' => NULL,
      ];

      // The container for the operations.
      $settings['group_info']['operations_wrapper'] = [
        '#type' => 'container',
        '#prefix' => '<div id="' . $wrapper_id . '">',
        '#suffix' => '</div>',
      ];

      // Get the operations links based on if this has a group.
      if (!empty($ecs[$i]['group']) && ($nid = (int) $ecs[$i]['group']) !== 0) {
        $links = $this->uwExpandCollapseGetOperationsLinks($nid);
      }
      else {
        $links = $this->uwExpandCollapseGetOperationsLinks();
      }

      // The operations links.
      $settings['group_info']['operations_wrapper']['operations'] = [
        '#type' => 'markup',
        '#markup' => $links,
      ];

      // @todo Make this work properly with the remove button.
      // phpcs:disable
      /*
      // The add expand/collapse remove group button.
      $settings['button'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#submit' => [[$this, 'expandCollapseRemoveOne']],
        '#ajax' => [
          'callback' => [$this, 'expandCollapseCallback'],
          'wrapper' => 'items-wrapper',
          'element_id' => $i,
        ],
        '#attributes' => [
          'class' => ['button--small'],
        ],
      ];
      */
      // phpcs:enable

      // The weight element.
      $settings['weight'] = [
        '#type' => 'weight',
        '#title_display' => 'invisible',
        '#default_value' => $i,
        '#attributes' => ['class' => [$group_class]],
        '#delta' => $num_of_rows,
      ];

      // Add the settings to the items array, which is full row
      // in the table.
      $form['items_fieldset']['items'][] = $settings;
    }

    // The add expand/collapse group button.
    $form['add_group'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add expand/collapse group'),
      '#submit' => [[$this, 'expandCollapseAddOne']],
      '#ajax' => [
        'callback' => [$this, 'expandCollapseCallback'],
        'wrapper' => 'items-wrapper',
      ],
      '#attributes' => [
        'class' => ['button--large'],
      ],
    ];

    return $form;
  }

  /**
   * Ajax callback to update the operations links.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   Array of elements to be used for replacement.
   */
  public function updateOperationsLinks(array &$form, FormStateInterface $form_state) {

    // Get the triggering element.
    $triggering_element = $triggering_element = $form_state->getTriggeringElement();

    // From the triggering element, get the element id, which
    // allow us to get the nid.
    $element_id = $triggering_element['#ajax']['element_id'];

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Get the nid from the input.
    $nid = $this->getGroupFromInput($values['settings']['items_fieldset']['items'][$element_id]['group_info']['group']);

    $form['settings']['items_fieldset']['items'][$element_id]['group_info']['operations_wrapper']['operations']['#markup'] = $this->uwExpandCollapseGetOperationsLinks($nid);

    return $form['settings']['items_fieldset']['items'][$element_id]['group_info']['operations_wrapper'];
  }

  /**
   * Add one more e/c section to the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function expandCollapseAddOne(array &$form, FormStateInterface $form_state) {

    // Set the add_more_called flag, so that we will load
    // the items by form_state, rather than block config.
    $form_state->set('add_more_called', TRUE);

    // Get the current values of the form_state.
    $values = $form_state->getValues();

    // Get the items from the form state, which will contain
    // the e/c settings.
    $items = $values['settings']['items_fieldset']['items'];

    // Define a variable ecs.
    $ecs = [];

    // Prepare variable ecs if the items exists.
    if (!empty($items)) {
      // Step through each of the items and set values to be used
      // in the ecs block config.
      foreach ($items as $item) {
        // Get the group from the input.
        $group = $this->getGroupFromInput($item['group_info']['group']);
        $ecs[] = [
          'heading_selector' => isset($item['heading_selector']) ?? '',
          'group' => $group,
        ];
      }
      // Increase by 1 the number of rows and set it in
      // the form_state.
      $form_state->set('num_of_rows', count($items) + 1);
    }
    else {
      $form_state->set('num_of_rows', 1);
    }

    // Set the ecs from the items in the form_state, so that we
    // will load newly added values.
    $form_state->set('ecs', $ecs);

    // Rebuild form with 1 extra row.
    $form_state->setRebuild();
  }

  // @todo Make this work properly with the remove button.
  // phpcs:disable
//  /**
//   * Remove one e/c section from the form.
//   *
//   * @param array $form
//   *   The form array.
//   * @param \Drupal\Core\Form\FormStateInterface $form_state
//   *   The form state.
//   */
//  public function expandCollapseRemoveOne(array &$form, FormStateInterface $form_state) {
//
//    // Get the triggering element, so that we can get
//    // which element we need to remove.
//    $triggering_element = $form_state->getTriggeringElement();
//
//    // Get the element id from the triggering element.
//    $element_id = $triggering_element['#ajax']['element_id'];
//
//    // Get the current values of the form_state.
//    $values = $form_state->getValues();
//
//    // Get the items from the form state.
//    $items = $values['settings']['items_fieldset']['items'];
//
//    // Remove the element that was requested to be removed.
//    unset($items[$element_id]);
//
//    // Step through each of the items and set values to be used
//    // in the ecs block config.
//    foreach ($items as $item) {
//      $ecs[] = [
//        'heading_selector' => $item['heading_selector'],
//        'group' => $item['group_info']['group'][0]['target_id'],
//      ];
//    }
//
//    // Set the ecs from the items in the form_state, so that we
//    // will load newly added values.
//    $form_state->set('ecs', $ecs);
//
//    // Decrement the num_of_rows, since we are removing one.
//    $num_of_rows = $form_state->get('num_of_rows');
//    $num_of_rows--;
//    $form_state->set('num_of_rows', $num_of_rows);
//
//    // Set the add_more_called flag, so that we will load
//    // the items by form_state, rather than block config.
//    $form_state->set('add_more_called', TRUE);
//
//    // Rebuild form with 1 extra row.
//    $form_state->setRebuild();
//  }
  // phpcs:enable

  /**
   * The ajax call back for expand/collapse remove one.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The form element to be overwritten.
   */
  public function expandCollapseCallback(array &$form, FormStateInterface $form_state) {
    return $form['settings']['items_fieldset'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    // Have at least blank ecs.
    $ecs = [];

    // Get the triggering element.
    $trigger_element = $form_state->getTriggeringElement();

    // Get the complete form, so that we can get the values.
    $cf = $form_state->getCompleteForm();

    // For adding new and editing the existing ec block.
    if (isset($cf['settings']['items_fieldset']['items'][0])) {
      // Get 'UPDATE' button trigger element ID.
      // the trigger element id is like edit-settings-add-group--MGVzEL99gS4
      // if 'ADD EXPAND/COLLAPSE GROUP' button, and the id is like
      // edit-actions-submit--sXT6mfnCVGg if 'UPDATE' button.
      $update_button_id = substr($trigger_element['#id'], 0, strpos($trigger_element['#id'], '--'));

      // Get the values from the form state.
      $values = $form_state->getValues();

      // Get the items from the form state.
      $items = $values['items_fieldset']['items'];

      // Step through each item and get only the ecs
      // that are not empty.
      foreach ($items as $index => $item) {

        // Get the value of the group.
        $group = $this->getGroupFromInput($item['group_info']['group']);

        // If the group is null, just continue on, the block
        // submit will take care of removing it later.
        if ($group === NULL) {
          continue;
        }

        // Validate group is numeric (node id), and also if it is, that
        // it points to correct content type node (expand collapse group).
        if (($nid = (int) $group)) {
          $node = $this->entityTypeManager->getStorage('node')->loadByProperties([
            'type' => 'uw_ct_expand_collapse_group',
            'nid' => $nid,
          ]);

          if (!$node) {
            $error = $this->t('There are no matching expand/collapse groups found.');
            $form_state->setError($cf['settings']['items_fieldset']['items'][$index]['group_info']['group'], $error);
            return;
          }
        }
        else {
          $error = $this->t('There are no content items matching "@item"', ['@item' => $item['group_info']['group']]);
          $form_state->setError($cf['settings']['items_fieldset']['items'][$index]['group_info']['group'], $error);
          return;
        }

        // Get the actual entered value.
        $item = $item['group_info']['group'];

        // Check if we need to add an error message.
        // If there is an item and no group, it means
        // that either there is only one nid or there
        // are multiple nids.
        // If not there was nothing entered.
        if ($item !== "" && $group == NULL) {

          // Get all the nids that could from the
          // entered value.
          $nids = $this->getExpColNodes($item);

          // If there are no nids, throw a no content error.
          if (count($nids) == 0) {
            $error = 'There are no content items matching "' . $item . '".';
          }
          // If there is more than one nid, we have to add
          // all of them to the error message.
          // If there is only one nid, just use that one.
          elseif (count($nids) > 1) {

            // Setup the error message.
            $error = 'Multiple content items match this reference; ';

            // Counter to track the comma in the message.
            $count = 0;

            // Step through each of the nids to the message.
            foreach ($nids as $nid) {

              // If there is more than one nid add the comma.
              if ($count > 0) {
                $error .= ', ';
              }

              // Load the node.
              $node = $this->entityTypeManager->getStorage('node')->load($nid);

              // Add the actual nid to the message.
              $error .= '"' . $node->label() . ' (' . $nid . ')"';

              // Increment the counter to track the comma.
              $count++;
            }
            // Add the rest of the message.
            $error .= '  Specify the one you want by appending the id in parentheses, like "';
            $error .= $item . ' (' . $nid . ')".';
          }
          else {
            // There is only one nid, so we are going to use
            // that nid and its values and no error message.
            $group = current($nids);
            $error = NULL;
            $item = $item . ' (' . current($nids) . ')';
          }
        }
        else {
          $error = NULL;
        }

        // Set the values of the ecs.
        $ecs[] = [
          'group' => $group,
          'item' => $item,
          'error' => $error,
        ];
      }

      // Flag for there is an item.
      $has_item = FALSE;

      // Step through each of the ecs and see if
      // there is at least one item and set the
      // has items flag.
      foreach ($ecs as $ec) {
        if ($ec['item'] !== '') {
          $has_item = TRUE;
          break;
        }
      }

      // If click 'UPDATE' button, and there is no any items, we set message.
      if (
        $update_button_id == 'edit-actions-submit' ||
        $update_button_id == 'edit-settings-add-group'
      ) {

        // If there are no items set the error message on the first ec.
        // If there are items ensure that there are no error messages.
        if (!$has_item) {
          $form_state->setError($cf['settings']['items_fieldset']['items'][0]['group_info']['group'], $this->t('You must have at least one expand/collapse group.'));
        }
        else {
          // Counter for the error messages.
          $count = 0;

          // Step through the ecs and see if there are error messages.
          foreach ($ecs as $ec) {
            // If there is an error message output it.
            if ($ec['error']) {
              $form_state->setError($cf['settings']['items_fieldset']['items'][$count]['group_info']['group'], $this->t('@message', ['@message' => $ec['error']]));
            }

            // Increment the counter so we get the correct
            // ec element.
            $count++;
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Get the items from the form_state.
    $items = $values['items_fieldset']['items'];

    // Sort the array by the weight.
    $items = uw_sort_array_by_weight($items);

    // Set the heading selector.
    $this->configuration['heading_selector'] = $values['items_fieldset']['heading_selector'];

    // Setup ecs array, we need to default to null in case
    // nothing is submitted in the form.
    $ecs = [];

    // Step through each of the items and set values to be used
    // in the ecs block config.
    foreach ($items as $item) {

      // Ensure that we are only adding groups that actually,
      // have a value.
      $group = $this->getGroupFromInput($item['group_info']['group']);
      if ($group !== NULL) {
        $ecs[] = [
          'group' => $group,
        ];
      }
    }

    // Save the ecs in the block config.
    $this->configuration['ecs'] = $ecs;
  }

  /**
   * Function to get the links for the operations of e/c.
   *
   * @param int|null $nid
   *   The nid of the section, if there is one.
   *
   * @return string
   *   The HTML string containing the links.
   */
  public function uwExpandCollapseGetOperationsLinks(int $nid = NULL): string {

    // Start the HTML for the operations links.
    $links = '<ul class="uw-exp-col__operations">';

    // Path to module (see https://www.drupal.org/node/2940438
    // for replacement for drupal_get_path).
    $module_path = $this->extensionListModule->getPath('uw_custom_blocks');

    // If we have a nid, then add edit and view links.
    // If there is no nid, only use add link.
    if ($nid) {

      // Get the correct URL to the node, no matter where we are.
      $url = Url::fromRoute('entity.node.canonical', ['node' => $nid])->toString();

      // The URL to the pencil icon.
      $pencil_icon = '/' . $module_path . '/images/pencil.png';
      $pencil_icon = Url::fromUri('internal:' . $pencil_icon, ['absolute' => TRUE])->toString();

      // The URL for the view icon.
      $view_icon = '/' . $module_path . '/images/view.png';
      $view_icon = Url::fromUri('internal:' . $view_icon, ['absolute' => TRUE])->toString();

      $links .= '<li>';
      $links .= '<a href="' . $url . '/layout" target="_blank">';
      $links .= '<img src="' . $pencil_icon . '" alt="edit expand/collapse group" />';
      $links .= '</a>';
      $links .= '</li>';

      $links .= '<li>';
      $links .= '<a class="use-ajax" data-dialog-options="{&quot;width&quot;:800}" data-dialog-type="modal" href="' . $url . '">';
      $links .= '<img src="' . $view_icon . '" alt="add new expand/collapse group" />';
      $links .= '</a>';
      $links .= '</li>';
    }
    else {

      // The URL for the plus icon.
      $plus_icon = '/' . $module_path . '/images/plus.png';
      $plus_icon = Url::fromUri('internal:' . $plus_icon, ['absolute' => TRUE])->toString();

      // Get the correct URL to the add node, no matter where we are.
      $url = Url::fromRoute('node.add', ['node_type' => 'uw_ct_expand_collapse_group'])->toString();

      $links .= '<li>';
      $links .= '<a href="' . $url . '" target="_blank">';
      $links .= '<img src="' . $plus_icon . '" alt="add new expand/collapse group" />';
      $links .= '</a>';
      $links .= '</li>';
    }

    // Close the ul in the HTML.
    $links .= '</ul>';

    // Return the HTML for the operations links.
    return $links;
  }

  /**
   * Function to get all the nids from the title.
   *
   * @param string $title
   *   The title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getExpColNodes(string $title) {

    // Get the nid(s) from the title.
    $nids = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('title', $title)
      ->condition('type', 'uw_ct_expand_collapse_group')
      ->condition('status', NodeInterface::PUBLISHED)
      ->execute();

    return $nids;
  }

  /**
   * Function to get the group (nid) from the entered input.
   *
   * @param string $item
   *   The entered input.
   */
  private function getGroupFromInput(string $item) {

    // Get the nid from the entity autocomplete.
    $nid = EntityAutocomplete::extractEntityIdFromAutocompleteInput($item);

    // If the nid is null, then try and get it from the
    // value that is entered.
    if ($nid == NULL) {

      // Get the nids based on what was entered.
      $nids = $this->getExpColNodes($item);

      // If there is only one nid, use it.
      // Otherwise there are no nids, so set to null.
      if (count($nids) == 1) {
        $nid = current($nids);
      }
      else {
        $nid = NULL;
      }
    }

    return $nid;
  }

}
