<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Custom block publication search.
 *
 * @Block(
 *   id = "uw_cbl_publication_search",
 *   admin_label = @Translation("Publication reference search"),
 * )
 */
class UwCblPublicationSearch extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entityTypeBundleInfo
   *   The entity type bundle info.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfo $entityTypeBundleInfo,
    FormBuilderInterface $formBuilder
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Set the uuid.
    $ref_search['uuid'] = $this->configuration['uuid'] = uniqid();

    // Get the reference search form using the options selected.
    $ref_search['form'] = $this->formBuilder->getForm(
      'Drupal\uw_bibcite_reference\Form\PublicationSearchForm',
      $this->configuration,
    );

    // If there is no block title, then set the label so that
    // the template will know to place a visually hidden one.
    if (!$this->configuration['label_display']) {
      $ref_search['label'] = $this->t('Publication reference search');
    }

    return [
      '#theme' => 'uw_block_form_search',
      '#form_search' => $ref_search,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // Details/summary for the search settings.
    $form['search_settings'] = [
      '#type' => 'details',
      '#title' => $this->getSettingTitle('Settings'),
    ];

    // The description for the reference search.
    $form['search_settings']['search_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Search description'),
      '#description' => $this->t('Enter text for the reference search description. Leave blank to have no description. (Note: text cannot contain HTML, and line breaks.)'),
      '#default_value' => $this->configuration['search_description'] ?? NULL,
    ];

    // The placeholder for the reference search.
    $form['search_settings']['search_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search placeholder'),
      '#description' => $this->t('Enter text for the reference search placeholder. Leave blank to have no placeholder.'),
      '#default_value' => $this->configuration['search_placeholder'] ?? NULL,
    ];

    // Details/summary for keywords of reference.
    $form['keyword_settings'] = [
      '#type' => 'details',
      '#title' => $this->getSettingTitle('Keywords'),
    ];

    // Get the options for the keywords.
    $keywords_options = $this->getKeywordsOptions();

    // Setup the select options to use keywords.
    $keyword_select_options['all'] = $this->t('All keywords');

    // Only add specific keywords, if we have keywords.
    if (count($keywords_options) > 0) {
      $keyword_select_options['specific'] = $this->t('Specific keywords');
    }

    // Element for keyword settings.
    $form['keyword_settings']['keyword'] = [
      '#type' => 'select',
      '#options' => $keyword_select_options,
      '#default_value' => $this->configuration['keyword'] ?? 'all',
      '#description' => $this->t('Select whether this will search all the reference keywords or only specific reference keywords.'),
    ];

    // Only add the keywords to the form if there are keywords.
    if (count($keywords_options) > 0) {

      // Element for specific keywords.
      $form['keyword_settings']['keywords'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Keywords'),
        '#options' => $keywords_options,
        '#states' => [
          'visible' => [
            'select[name="settings[keyword_settings][keyword]"]' => [
              'value' => 'specific',
            ],
          ],
        ],
        '#description' => $this->t('Select which reference keywords to restrict the search to.'),
        '#default_value' => $this->configuration['keywords'],
      ];
    }

    // Details/summary for type of reference.
    $form['type_settings'] = [
      '#type' => 'details',
      '#title' => $this->getSettingTitle('Type'),
    ];

    // Element for search settings.
    $form['type_settings']['type'] = [
      '#type' => 'select',
      '#options' => [
        'all' => $this->t('All types'),
        'specific' => $this->t('Specific types'),
      ],
      '#default_value' => $this->configuration['type'] ?? 'all',
      '#description' => $this->t('Select whether this will search all the reference types or only specific reference types.'),
    ];

    // The field for type of reference, so we can
    // be specific to only certain types of references.
    $form['type_settings']['types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Types'),
      '#options' => $this->getTypesOptions(),
      '#states' => [
        'visible' => [
          'select[name="settings[type_settings][type]"]' => [
            'value' => 'specific',
          ],
        ],
      ],
      '#description' => $this->t('Select which reference types to restrict the search to.'),
      '#default_value' => $this->configuration['types'] ?? [],
    ];

    // Details/summary for year of reference.
    $form['year_settings'] = [
      '#type' => 'details',
      '#title' => $this->getSettingTitle('Year'),
    ];

    // Element to restrict search to specific year.
    $form['year_settings']['year'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Enter a year to restrict the search, leave blank for all years.'),
    ];

    // Details/summary for the sort settings.
    $form['sort_settings'] = [
      '#type' => 'details',
      '#title' => $this->getSettingTitle('Sort'),
    ];

    // The sort by element.
    $form['sort_settings']['sort_by'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort by'),
      '#options' => [
        'year' => $this->t('Year'),
        'author' => $this->t('Author'),
        'type' => $this->t('Type'),
      ],
      '#default_value' => $this->configuration['sort_by'] ?? 'year',
    ];

    // The sort order element.
    $form['sort_settings']['sort_order'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort order'),
      '#options' => [
        'DESC' => $this->t('Desc'),
        'ASC' => $this->t('Asc'),
      ],
      '#default_value' => $this->configuration['sort_order'] ?? 'desc',
    ];

    return $form;
  }

  /**
   * Get the options for the keywords.
   *
   * @return array
   *   The array of options for the keywords.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getKeywordsOptions(): array {

    // Setup at least an empty array in case there are
    // no keywords to use as options.
    $options = [];

    // Get any keywords.
    $keywords = $this->entityTypeManager->getStorage('bibcite_keyword')->loadMultiple();

    // Step through and get the keywords for options.
    foreach ($keywords as $keyword) {
      $options[$keyword->id()] = $keyword->getName();
    }

    return $options;
  }

  /**
   * Get the options for the type of reference.
   *
   * @return array
   *   Array of options for the type of reference.
   */
  private function getTypesOptions(): array {

    // Setup options array for reference type.
    $options = [];

    // Step through each type of references and
    // add to options array.
    foreach ($this->entityTypeBundleInfo->getBundleInfo('bibcite_reference') as $index => $ref) {
      $options[$index] = $ref['label'];
    }

    return $options;
  }

  /**
   * Get the title for a setting.
   *
   * @param string $type
   *   The type of setting.
   *
   * @return string
   *   The title for the setting.
   */
  private function getSettingTitle(string $type): string {

    // Start the settings message.
    $settings_title = $type . '<br /><span class="description">';

    // Get the title based on the type of title.
    switch ($type) {

      case 'Keywords':
        $settings_title .= $this->getKeywordsSettingTitle();
        break;

      case 'Settings':
        $settings_title .= $this->getSettingsSettingTitle();
        break;

      case 'Sort':
        $settings_title .= $this->getSortSettingTitle();
        break;

      case 'Type':
        $settings_title .= $this->getTypeSettingTitle();
        break;

      case 'Year':
        $settings_title .= $this->getYearSettingTitle();
        break;
    }

    // Add the closing span tag to the title.
    $settings_title .= '</span>';

    return $settings_title;
  }

  /**
   * Get the title for years.
   *
   * @return string
   *   The title for years.
   */
  private function getYearSettingTitle(): string {

    // The title for the setting.
    $settings_title = '';

    // If there is a year, add specific year to title.
    // If not add the default of all years.
    if (
      isset($this->configuration['year']) &&
      $this->configuration['year']
    ) {
      $settings_title .= 'Specific year: ' . $this->configuration['year'];

    }
    else {
      $settings_title .= 'All years';
    }

    return $settings_title;
  }

  /**
   * Get the title for sorting.
   *
   * @return string
   *   The title for sorting.
   */
  private function getSortSettingTitle(): string {

    // The title for the setting.
    $settings_title = '';

    // If there is sort by add to title.
    // If not add the default Year.
    if (isset($this->configuration['sort_by'])) {
      $settings_title .= ucfirst($this->configuration['sort_by']);
    }
    else {
      $settings_title .= 'Year';
    }

    // If there is a sort order add it.
    // If not add the default DESC.
    if (isset($this->configuration['sort_order'])) {
      $settings_title .= ' (' . $this->configuration['sort_order'] . ')';
    }
    else {
      $settings_title .= ' (DESC)';
    }

    return $settings_title;
  }

  /**
   * Get the title for keywords.
   *
   * @return string
   *   The title for keywords.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getKeywordsSettingTitle(): string {

    // The title for the setting.
    $settings_title = '';

    // If there are specific keywords, add to the title.
    // If not, then add the default.
    if (
      isset($this->configuration['keyword']) &&
      $this->configuration['keyword'] == 'specific'
    ) {

      $settings_title .= 'Specific keywords: ';
    }
    else {

      $settings_title .= 'All keywords';
    }

    // If there are actual keywords, add to the title.
    if (isset($this->configuration['keywords'])) {

      // If there are more than 5 keywords, add a message
      // for more than 5.  If not add the actual keywords.
      if (count($this->configuration['keywords']) > 5) {
        $settings_title .= ' more than 5 keywords specified';
      }
      else {

        // The count for the keywords, to add the comma.
        $keyword_count = 0;

        // Step through and get the keywords for options.
        foreach ($this->configuration['keywords'] as $keyword) {

          // If there is a keyword, add it.
          if ($keyword) {

            // If there is more than one keyword, add the comma.
            if ($keyword_count > 0) {
              $settings_title .= ', ';
            }

            // Get the keyword entity.
            $keyw = $this->entityTypeManager->getStorage('bibcite_keyword')->load($keyword);

            // Add the keyword to the title.
            $settings_title .= $keyw->getName();

            // Increment the counter so we get the comma.
            $keyword_count++;
          }
        }
      }
    }

    return $settings_title;
  }

  /**
   * Get the title for the search settings.
   *
   * @return string
   *   The title for the search settings.
   */
  private function getSettingsSettingTitle(): string {

    // The title for the setting.
    $settings_title = '';

    // If there is config, process it.
    if (
      isset($this->configuration) &&
      isset($this->configuration['search_description']) &&
      isset($this->configuration['search_placeholder'])
    ) {

      // If there is both description and placeholder, add
      // to title message about both.
      if (
        $this->configuration['search_description'] &&
        $this->configuration['search_placeholder']
      ) {

        $settings_title .= 'Description and placeholder set';
      }
      // If there is only description, add that to title.
      elseif (
        $this->configuration['search_description'] &&
        !$this->configuration['search_placeholder']
      ) {

        $settings_title .= 'Only description set';
      }
      // If there is only placeholder, add that to title.
      elseif (
        !$this->configuration['search_description'] &&
        $this->configuration['search_placeholder']
      ) {

        $settings_title .= 'Only placeholder set';
      }

      // Add the default that there is no description or
      // placeholder set.
      else {

        $settings_title .= 'No description or placeholder set';
      }
    }
    else {

      $settings_title .= 'No description or placeholder set';
    }

    return $settings_title;
  }

  /**
   * Get the title for the type of reference.
   *
   * @return string
   *   The title for the type of reference.
   */
  private function getTypeSettingTitle(): string {

    // The title for the setting.
    $settings_title = '';

    // If the type is specific, add that to the title.
    // If not then add the default of all types.
    if (
      isset($this->configuration['type']) &&
      $this->configuration['type'] == 'specific'
    ) {

      // Add specific types to the title.
      $settings_title .= 'Specific types: ';

      // Get all the types of references.
      $ref_types = $this->entityTypeBundleInfo->getBundleInfo('bibcite_reference');

      // Step through the types and get only the ones
      // that are set.
      foreach ($this->configuration['types'] as $type) {

        // If there is a type, then add to types array, using
        // the ref_types array, that contains all the types
        // with the correct labels.
        if ($type) {
          $types[] = $ref_types[$type]['label'];
        }
      }

      // If there are more than 5 types selected, add that
      // to the title.
      // If less than 5, add the actual types to the title.
      if (count($types) > 5) {
        $settings_title .= 'more than 5 types selected';
      }
      else {

        // Counter to get the comma in the right place.
        $types_count = 0;

        // Step through each of the types and add to title.
        foreach ($types as $type) {

          // If there is more than one type, add the comma.
          if ($types_count > 0) {
            $settings_title .= ', ';
          }

          // Add the type to the title.
          $settings_title .= $type;

          // Increment the counter so we get the comma correctly.
          $types_count++;
        }
      }
    }
    else {

      // Add the defauly of all types to the title.
      $settings_title .= 'All types';
    }

    return $settings_title;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // If selected to use specific reference types, ensure that there
    // are reference types selected.
    if (
      $values['type_settings']['type'] == 'specific'
    ) {

      // Flag to set the form error.
      $type_flag = TRUE;

      // Step through each of the types, and check if at least one
      // of the reference types is selected.
      foreach ($values['type_settings']['types'] as $type) {

        // If one of the types is selected, set the flag and break the loop.
        if ($type) {
          $type_flag = FALSE;
          break;
        }
      }

      // If the type flag is set, set the form error.
      if ($type_flag) {
        $form_state->setError(
          $form['type_settings']['types'],
          $this->t('You must select at least one type')
        );
      }
    }

    // If selected to use specific keywords, ensure that there
    // are keywords selected.
    if (
      $values['keyword_settings']['keyword'] == 'specific'
    ) {

      // Flag to set the form error.
      $keyword_flag = TRUE;

      // Step through each of the keywords, and check if at least one
      // of the keywords selected.
      foreach ($values['keyword_settings']['keywords'] as $keyword) {

        // If one of the keywords is selected, set the flag and break the loop.
        if ($keyword) {
          $keyword_flag = FALSE;
          break;
        }
      }

      // If the keyword flag is set, set the form error.
      if ($keyword_flag) {
        $form_state->setError(
          $form['keyword_settings']['keywords'],
          $this->t('You must select at least one keyword')
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Set the config for the search settings.
    $this->configuration['search_description'] = $values['search_settings']['search_description'] ?? NULL;
    $this->configuration['search_placeholder'] = $values['search_settings']['search_placeholder'] ?? NULL;

    // Set config if to use all or specific keywords.
    $this->configuration['keyword'] = $values['keyword_settings']['keyword'] ?? NULL;
    $this->configuration['keywords'] = $values['keyword_settings']['keywords'] ?? NULL;

    // Set config if to use all or specific reference types.
    $this->configuration['type'] = $values['type_settings']['type'] ?? NULL;
    $this->configuration['types'] = $values['type_settings']['types'] ?? NULL;

    // Set the config for the year.
    $this->configuration['year'] = $values['year_settings']['year'] ?? NULL;

    // Set the config for the sort.
    $this->configuration['sort_by'] = $values['sort_settings']['sort_by'] ?? NULL;
    $this->configuration['sort_order'] = $values['sort_settings']['sort_order'] ?? NULL;
  }

}
