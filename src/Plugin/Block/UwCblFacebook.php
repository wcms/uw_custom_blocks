<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * UW Custom Block Facebook block.
 *
 * @Block(
 *   id = "uw_cbl_facebook",
 *   admin_label = @Translation("Facebook"),
 * )
 */
class UwCblFacebook extends BlockBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'fb_feed_type' => NULL,
      'fb_page' => NULL,
      'fb_url' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Setup variables for template from block config.
    $facebook['feed_type'] = $this->configuration['fb_feed_type'];
    $facebook['username'] = $this->configuration['fb_page'];
    $facebook['url'] = $this->configuration['fb_url'];

    // Return custom template with variable.
    return [
      '#theme' => 'uw_block_facebook',
      '#facebook' => $facebook,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // The Facebook type of feed form element.
    $form['fb_feed_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Facebook feed type'),
      '#description' => $this->t('Select the type of Facebook feed.'),
      '#options' => [
        'singlepost' => $this->t('Single post'),
        'timeline' => $this->t('Timeline'),
      ],
      '#default_value' => $this->configuration['fb_feed_type'],
      '#empty_option' => $this->t('- Select a value -'),
      '#required' => TRUE,
    ];

    // The Facebook URL form element.
    $form['fb_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook URL'),
      '#description' => $this->t('Enter the text of the Facebook URL.'),
      '#default_value' => $this->configuration['fb_url'],
      '#states' => [
        'visible' => [
          ['select[name="settings[fb_feed_type]"]' => ['value' => 'singlepost']],
        ],
        'required' => [
          ['select[name="settings[fb_feed_type]"]' => ['value' => 'singlepost']],
        ],
      ],
    ];

    // The Facebook page form element.
    $form['fb_page'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook page'),
      '#description' => $this->t('Enter the Facebook page name: facebook.com/'),
      '#default_value' => $this->configuration['fb_page'],
      '#states' => [
        'visible' => [
          ['select[name="settings[fb_feed_type]"]' => ['value' => 'timeline']],
        ],
        'required' => [
          ['select[name="settings[fb_feed_type]"]' => ['value' => 'timeline']],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Check that the feed type has been set.  The feed type
    // is a required feed so we do not need to handle that in this
    // form validation.
    if ($values['fb_feed_type'] !== '') {

      // If the user has selected singlepost facebook type, check
      // that the value is not null and passes regular expression tests.
      if ($values['fb_feed_type'] == 'singlepost') {

        // If the facebook URL is blank, the set error about required field.
        if ($values['fb_url'] == '') {
          $form_state->setErrorByName('fb_url', $this->t('Facebook URL is required.'));
        }

        // Facebook URL is not blank now check that it is in correct format.
        else {

          // Check if we do not have a valid facebook page.
          if (!preg_match('/^https?:\/\/(?:www\.)?facebook\.com\/.+/', $values['fb_url'])) {

            // Set the form error.
            $form_state->setErrorByName('fb_url', 'FaceBook URL contains errors.');
          }
        }
      }

      // If the feed type is timeline, check that not null and passes
      // regular expression tests.
      if ($values['fb_feed_type'] == 'timeline') {

        // If the facebook page is blank, the set error about required field.
        if ($values['fb_page'] == '') {
          $form_state->setErrorByName('fb_page', $this->t('Facebook page is required.'));
        }

        // Facebook page is not null, check that it is valid.
        else {

          // Check if we do not have a valid facebook page.
          if (!preg_match('/[a-zA-Z]/', $values['fb_page']) ||
            !preg_match('/^(?:[-.a-zA-Z0-9]{1,75}|pages\/[-.a-zA-Z0-9]+\/[0-9]+)$/', $values['fb_page'])) {

            // Set the form error.
            $form_state->setErrorByName('fb_page', 'The FaceBook page contains errors.');
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Set the config for facebook block.
    $this->configuration['fb_feed_type'] = $values['fb_feed_type'];
    $this->configuration['fb_url'] = $values['fb_url'];
    $this->configuration['fb_page'] = $values['fb_page'];
  }

}
