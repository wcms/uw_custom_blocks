<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\uw_cfg_common\Service\UwNodeContent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UW custom block teaser.
 *
 * @Block(
 *   id = "uw_cbl_teaser",
 *   admin_label = @Translation("Content teaser"),
 * )
 */
class UwCblTeaser extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * UW node content.
   *
   * @var \Drupal\uw_cfg_common\Service\UwNodeContent
   */
  protected UwNodeContent $uwNodeContent;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\uw_cfg_common\Service\UwNodeContent $uwNodeContent
   *   UW Service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    UwNodeContent $uwNodeContent
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->uwNodeContent = $uwNodeContent;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uw_cfg_common.uw_node_content')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {

    return [
      'nid' => NULL,
      'content_type' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build():array {

    // Getting the entity using new extracted method.
    $entity = $this->getEntity();

    // If there is no nid return empty build, probably something went wrong when
    // inserting the block.
    if ($entity === NULL) {
      return [];
    }

    // Bibcite references are displayed differently.
    if ($entity->getEntityType()->id() === 'bibcite_reference') {

      // Get the view builder, so we can render the entity.
      $view_builder = $this->entityTypeManager->getViewBuilder('bibcite_reference');

      // Get the teaser data from the view builder.
      $teaser['teaser'] = $view_builder->view($entity, 'table');
      $teaser['type'] = 'bibcite';
    }
    // Catalogs need to use a view.
    elseif ($this->configuration['content_type'] == 'catalog') {

      // Set the display id for the view.
      $display_id = 'catalog_embed';

      // Load the view.
      $view = $this->entityTypeManager
        ->getStorage('view')
        ->load('uw_view_catalog_embed')
        ->getExecutable();

      // Set the view display.
      $view->setDisplay($display_id);

      // Set the arguements for the view.
      $view->setArguments([$this->configuration['nid']]);

      // Execute the view.
      $view->execute();

      // Return the render array for the view.
      return [
        '#type' => 'view',
        '#name' => 'view_name',
        '#view' => $view,
        '#display_id' => $display_id,
        '#embed' => TRUE,
        '#cache' => $view->getCacheTags(),
      ];
    }
    else {
      // Get the teaser data from the uw service.
      $teaser = $this->uwNodeContent->getNodeContent(
        $entity,
        'teaser',
        'all'
      );
    }

    // If this is contact or a profile and there is
    // image is selected not to show, then remove
    // the image from the teaser content.
    if (
      $this->configuration['content_type'] == 'contact' ||
      $this->configuration['content_type'] == 'profile'
    ) {

      // Get the config.
      $config = $this->configuration;

      // Ensure that null is set for image if there is
      // none or error will occur.
      if (!isset($config['show_image'])) {
        $teaser['image'] = NULL;
      }
      elseif (!$this->configuration['show_image']) {
        $teaser['image'] = NULL;
      }
    }

    // Set the render array.
    return [
      '#theme' => 'uw_block_teaser',
      '#teaser' => $teaser,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm(
    $form,
    FormStateInterface $form_state
  ): array {

    // Get the current entity.
    $entity = $this->getEntity();

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // The content type form element.
    $this->createContentTypeFormElement($form);

    // Get the content types and machine names.
    $contentTypes = $this->getTeaserContentTypes();

    // Step through each of the content types and create the
    // node type form elements.
    foreach ($contentTypes as $contentType => $value) {

      // Get the node form element.
      $this->createNodeTypeFormElement(
        $form,
        $contentType,
        $entity,
        $this->configuration
      );
    }

    // Add library to the form.
    $form['#attached']['library'][] = 'uw_custom_blocks/uw_cbl_teaser';

    return $form;
  }

  /**
   * Creates the form element for a specific node type.
   *
   * @param array &$form
   *   The form array.
   * @param string $contentType
   *   The machine name of the content type.
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The entity object if it exists.
   * @param array $config
   *   The configuration settings for the node type.
   */
  protected function createNodeTypeFormElement(
    array &$form,
    string $contentType,
    ?EntityInterface $entity,
    array $config
  ): void {

    // Get the proper title for the content type.
    $title = ucfirst(str_replace('_', ' ', $contentType));

    // Get the machine names of the content types.
    $machine_names = $this->getCtWithMachineNames();

    // The content type form element.
    $form[$contentType] = [
      '#type' => 'fieldset',
      '#title' => $this->t('@title settings', ['@title' => $title]),
      '#states' => [
        'visible' => [
          'select[name="settings[content_type]"]' => ['value' => $contentType],
        ],
      ],
    ];

    // If this is a contact or profile, set the show
    // image form element.
    if (
      $contentType == 'contact' ||
      $contentType == 'profile'
    ) {

      // The form element for show image.
      $form[$contentType]['show_image'] = [
        '#type' => 'select',
        '#title' => $this->t(
          'Show @content_type image?',
          ['@content_type' => $contentType]
        ),
        '#description' => $this->t(
          'Select if you want to show the image of the @content_type.',
          ['@content_type' => $contentType]
        ),
        '#options' => [
          1 => 'Yes',
          0 => 'No',
        ],
        '#default_value' => $config['show_image'] ?? 1,
      ];
    }

    if ($contentType == 'catalog') {
      if ($config['content_type'] == 'catalog') {
        $default_value = $config['nid'] ? $entity : NULL;
      }
      else {
        $default_value = NULL;
      }
      // The node id form element.
      $form[$contentType]['nid'] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'taxonomy_term',
        '#title' => $this->t('@title', ['@title' => $title]),
        '#tags' => TRUE,
        '#default_value' => $default_value,
        '#label_attributes' => ['class' => ['form-required']],
        '#selection_settings' => [
          'target_bundles' => ['uw_vocab_catalogs'],
        ],
      ];
    }
    // Override settings for bibcite references.
    elseif ($contentType === 'publication_reference') {
      $form[$contentType]['nid'] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'bibcite_reference',
        '#title' => $this->t('@title', ['@title' => $title]),
        '#default_value' => ($entity && $config['content_type'] === $contentType) ? $entity : '',
        '#label_attributes' => ['class' => ['form-required']],
        '#selection_handler' => 'views',
        '#selection_settings' => [
          'view' => [
            'view_name' => 'bibcite_reference',
            'display_name' => 'publication_entity_reference',
            'arguments' => [],
          ],
          'match_operator' => 'CONTAINS',
        ],
      ];
    }
    else {

      // The node id form element.
      $form[$contentType]['nid'] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'node',
        '#title' => $this->t('@title', ['@title' => $title]),
        '#tags' => TRUE,
        '#default_value' => ($entity && $config['content_type'] === $contentType) ? $entity : '',
        '#selection_settings' => ['target_bundles' => [$machine_names[$contentType]]],
        '#label_attributes' => ['class' => ['form-required']],
      ];
    }
  }

  /**
   * Creates the form element for selecting the type of content.
   *
   * @param array $form
   *   The form to add the element to.
   */
  protected function createContentTypeFormElement(array &$form): void {

    // The form element for the content type.
    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of content'),
      '#options' => $this->getTeaserContentTypes(),
      '#description' => $this->t('Select the content to list'),
      '#required' => TRUE,
      '#empty_option' => '-- Select --',
      '#default_value' => $this->configuration['content_type'],
    ];
  }

  /**
   * Retrieves the entity based on the configured nid.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The loaded node entity, or NULL if no nid is found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntity(): ?EntityInterface {

    // Get the entity id from the block config.
    $entity_id = $this->configuration['nid'];

    // Return early as no nid found.
    if ($entity_id === NULL) {
      return NULL;
    }

    // If bibcite reference is used, make sure to load bibcite entity.
    if ($this->configuration['content_type'] === 'publication_reference') {
      return $this->entityTypeManager->getStorage('bibcite_reference')->load($entity_id);
    }

    if ($this->configuration['content_type'] === 'catalog') {
      return $this->entityTypeManager->getStorage('taxonomy_term')->load($entity_id);
    }

    // Get the node.
    return $this->entityTypeManager
      ->getStorage('node')
      ->load($entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Get the content type from the form state.
    $content_type = $values['content_type'];

    // Set the heading level.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Set the config for teaser.
    $this->configuration['content_type'] = $content_type;
    if ($content_type == 'publication_reference') {
      $this->configuration['nid'] = $values[$content_type]['nid'];
    }
    else {
      $this->configuration['nid'] = $values[$content_type]['nid'][0]['target_id'];
    }

    // If this is a contact or profile, set the show image.
    // If not then unset the show image block config.
    if (
      $content_type == 'profile' ||
      $content_type == 'contact'
    ) {

      $this->configuration['show_image'] = $values[$content_type]['show_image'];
    }
    else {
      if (isset($this->configuration['show_image'])) {
        unset($this->configuration['show_image']);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate(
    $form,
    FormStateInterface $form_state
  ): void {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Get the content type from the values.
    if ($content_type = $values['content_type']) {

      // Get the title so that it displays properly.
      $title = str_replace('_', ' ', $values['content_type']);

      // If there is an empty nid, set the form error.
      if ($values[$content_type]['nid'] === NULL) {
        $form_state->setError(
          $form[$content_type]['nid'],
          $this->t(
            'You must select a @title',
            ['@title' => $title]
          )
        );
      }
    }
  }

  /**
   * Function to get the content types used in teasers.
   *
   * @return array
   *   Array of content types that have teasers.
   */
  public function getTeaserContentTypes(): array {

    return [
      'blog' => $this->t('Blog post'),
      'catalog' => $this->t('Catalog'),
      'catalog_item' => $this->t('Catalog item'),
      'contact' => $this->t('Contact'),
      'event' => $this->t('Event'),
      'news_item' => $this->t('News item'),
      'opportunity' => $this->t('Opportunity'),
      'profile' => $this->t('Profile'),
      'project' => $this->t('Project'),
      'publication_reference' => $this->t('Publication reference'),
      'service' => $this->t('Service'),
    ];
  }

  /**
   * Function to get the manual list block content types.
   *
   * @return array
   *   Array of content types.
   */
  public function getCtWithMachineNames(): array {

    return [
      'blog' => 'uw_ct_blog',
      'catalog' => 'uw_ct_catalog',
      'catalog_item' => 'uw_ct_catalog_item',
      'contact' => 'uw_ct_contact',
      'event' => 'uw_ct_event',
      'news_item' => 'uw_ct_news_item',
      'opportunity' => 'uw_ct_opportunity',
      'profile' => 'uw_ct_profile',
      'project' => 'uw_ct_project',
      'publication_reference' => 'bibcite_reference',
      'service' => 'uw_ct_service',
    ];
  }

}
