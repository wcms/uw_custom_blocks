<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * UW Custom Block X (formerly Twitter) block.
 *
 * @Block(
 *   id = "uw_cbl_twitter",
 *   admin_label = @Translation("X (formerly Twitter)"),
 * )
 */
class UwCblTwitter extends BlockBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'twitter_feed_type' => NULL,
      'twitter_username' => NULL,
      'twitter_tweet_code' => NULL,
      'twitter_listname' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Setup variables for template from block config.
    $twitter = [
      'feed_type' => $this->configuration['twitter_feed_type'],
      'username' => $this->configuration['twitter_username'],
      'tweet_code' => $this->configuration['twitter_tweet_code'],
      'list_name' => $this->configuration['twitter_listname'],
    ];

    // Return custom template with variable.
    return [
      '#theme' => 'uw_block_twitter',
      '#twitter' => $twitter,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // The type of X (formerly Twitter) feed form element.
    $form['twitter_feed_type'] = [
      '#type' => 'select',
      '#title' => $this->t('X (formerly Twitter) feed type'),
      '#description' => $this->t('Select the type of X (formerly Twitter) feed.'),
      '#required' => TRUE,
      '#options' => [
        'embed-tweet' => $this->t('Embedded tweet'),
        'list' => $this->t('List'),
        'profile' => $this->t('Profile'),
      ],
      '#empty_option' => $this->t('- Select a value -'),
      '#default_value' => $this->configuration['twitter_feed_type'],
    ];

    // The X (formerly Twitter) username form element.
    $form['twitter_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('X (formerly Twitter) username'),
      '#description' => $this->t('Enter the X (formerly Twitter) username, without the @.'),
      '#default_value' => $this->configuration['twitter_username'],
      '#states' => [
        'visible' => [
          [
            'select[name="settings[twitter_feed_type]"]' => [
              ['value' => 'profile'],
              ['value' => 'list'],
            ],
          ],
        ],
        'required' => [
          [
            'select[name="settings[twitter_feed_type]"]' => [
              ['value' => 'profile'],
              ['value' => 'list'],
            ],
          ],
        ],
      ],
    ];

    // The X (formerly Twitter) tweet code form element.
    $form['twitter_tweet_code'] = [
      '#type' => 'textarea',
      '#rows' => 3,
      '#title' => $this->t('Tweet code'),
      '#description' => $this->t('Enter the Embedded Tweet code from X (formerly Twitter) or individual Tweet URL.'),
      '#default_value' => $this->configuration['twitter_tweet_code'],
      '#states' => [
        'visible' => [
          [
            'select[name="settings[twitter_feed_type]"]' => [
              ['value' => 'embed-tweet'],
            ],
          ],
        ],
        'required' => [
          [
            'select[name="settings[twitter_feed_type]"]' => [
              ['value' => 'embed-tweet'],
            ],
          ],
        ],
      ],
    ];

    // The X (formerly Twitter) list name form element.
    $form['twitter_listname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List name'),
      '#description' => $this->t('Enter the name of the list.'),
      '#default_value' => $this->configuration['twitter_listname'],
      '#states' => [
        'visible' => [
          [
            'select[name="settings[twitter_feed_type]"]' => [
              ['value' => 'list'],
            ],
          ],
        ],
        'required' => [
          [
            'select[name="settings[twitter_feed_type]"]' => [
              ['value' => 'list'],
            ],
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    if (!empty($values['twitter_feed_type'])) {
      // Switch on the type of facebook feed and setup elements to validate.
      switch ($values['twitter_feed_type']) {

        case 'profile':
          $twitter_parts_to_validate = [
            'twitter_username',
          ];
          break;

        case 'embed-tweet':
          $twitter_parts_to_validate = [
            'twitter_tweet_code',
          ];
          break;

        case 'list':
          $twitter_parts_to_validate = [
            'twitter_listname',
          ];
          break;
      }
      // Perform the actual validation.
      $this->validateTwitterPartsAndSetErrors($twitter_parts_to_validate, $values, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Set the config for the X (formerly Twitter) block.
    $this->configuration['twitter_feed_type'] = $values['twitter_feed_type'];
    $this->configuration['twitter_username'] = $values['twitter_username'];
    $this->configuration['twitter_listname'] = $values['twitter_listname'];
    $this->configuration['twitter_tweet_code'] = $values['twitter_tweet_code'];
  }

  /**
   * Validate X (formerly Twitter) parts of form.
   *
   * @param array $twitter_parts_to_validate
   *   The array of form elements to validate.
   * @param array $values
   *   The values from the form state.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state object.
   */
  private function validateTwitterPartsAndSetErrors(array $twitter_parts_to_validate, array $values, FormStateInterface &$form_state): void {

    // The X (formerly Twitter) username validation.
    $twitter_parts['twitter_username'] = [
      'name' => 'username',
      'reg_expression' => ' /^[a-zA-Z0-9_]{1,15}$/',
    ];

    // The X (formerly Twitter) listname validation.
    $twitter_parts['twitter_listname'] = [
      'name' => 'listname',
      'reg_expression' => '/^[a-zA-Z][a-zA-Z0-9_-]{0,24}$/',
    ];

    // The X (formerly Twitter) tweet validation.
    $twitter_parts['twitter_tweet_code'] = [
      'name' => 'tweet code',
      'reg_expression' => '/.*twitter\.com\/([^"\/]+\/status\/[0-9]+)([\s\S]+$|)/',
    ];

    // Step through each value to validate and perform the validation.
    foreach ($twitter_parts_to_validate as $twitter_part_to_validate) {

      // Check if the value is null and if so, set error.
      if ($values[$twitter_part_to_validate] == NULL) {

        // Set the form error for not having a null value.
        $form_state->setErrorByName(
          $twitter_part_to_validate,
          $this->t('You must enter a X (formerly Twitter) @part.',
            ['@part' => $twitter_parts[$twitter_part_to_validate]['name']]
          )
        );
      }

      // Check the format for the supplied twitter part and input, if not pass,
      // then set error message.
      elseif (!preg_match(
        $twitter_parts[$twitter_part_to_validate]['reg_expression'],
        $values[$twitter_part_to_validate]
      )) {

        // Set the error message.
        $form_state->setErrorByName(
          $twitter_part_to_validate,
          $this->t('There was an error in the X (formerly Twitter) @part.',
            ['@part' => $twitter_parts[$twitter_part_to_validate]['name']]
          )
        );
      }
    }
  }

}
