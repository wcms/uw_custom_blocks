<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\uw_cfg_common\Service\UwService;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UW Waterloo Events block.
 *
 * @Block(
 *   id = "uw_cbl_waterloo_events",
 *   admin_label = @Translation("Waterloo Events"),
 * )
 */
class UwCblWaterlooEvents extends BlockBase implements ContainerFactoryPluginInterface {

  // The url to the news site.
  const EVENTSURL = 'https://uwaterloo.ca/events';

  // The url to the news api.
  const EVENTSURLAPI = self::EVENTSURL . '/api/v3.0/event';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * An http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * UW node content.
   *
   * @var \Drupal\uw_cfg_common\Service\UwService
   */
  protected $uwService;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('http_client'),
      $container->get('uw_cfg_common.uw_service')
    );
  }

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   An HTTP client.
   * @param \Drupal\uw_cfg_common\Service\UwService $uwService
   *   UW Service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    ClientInterface $httpClient,
    UwService $uwService
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->httpClient = $httpClient;
    $this->uwService = $uwService;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Get the uw news from the block config.
    $events = $this->configuration['uwevents'];

    // Step through each of the items and get the info.
    foreach ($events['items'] as $item) {

      // Get the alias from the full url.
      $alias = str_replace(self::EVENTSURL, '', $item['url']);

      // Get the url for the waterloo events item.
      $url = self::EVENTSURLAPI . '?path=' . $alias;

      // Make the call to the API.
      $event = $this->httpClient->get($url, ['verify' => FALSE]);

      // Decode the API call.
      $event = json_decode($event->getBody(), TRUE);

      // If there is an event from the api, set the info.
      if (isset($event['data'][0]) && $event = $event['data'][0]) {

        // Reset the event date so that we can get a value.
        $event_date = NULL;

        // Set the info about the listed date.
        $event_date = [
          'month' => date('M', $event['next_date']['start_timestamp']),
          'day' => date('j', $event['next_date']['start_timestamp']),
        ];

        // If the all day flag is set and true,
        // identify the event as all day.
        if (!empty($event['next_date']['all_day'])) {
          $event_date['time'] = 'All day';
        }
        else {
          // Set the info about the listed date.
          $event_date['time'] = $event['next_date']['start_time'];
        }

        // If there is no event date, it means that all the event
        // dates have passed, so use the last date entry.
        if (!$event_date) {

          // Get the last event date timestamp.
          $event_date_timestamp = $event['all_dates'][count($event['all_dates']) - 1]['start_timestamp'];

          // Set the info about the last event date.
          $event_date = [
            'month' => date('M', $event_date_timestamp),
            'day' => date('j', $event_date_timestamp),
            'time' => date('g:i a', $event_date_timestamp),
          ];
        }

        // Reset the summary.
        $summary = NULL;

        // If there is a summary, set the render array.
        if (isset($event['summary'])) {

          // Get the summary and remove any links.
          $text = $event['summary'];
          $text = preg_replace('/<a.*?>/', '', $text);
          $text = preg_replace('/<\/a.*?>/', '', $text);

          // Render array for summary.
          $summary = [
            '#type' => 'processed_text',
            '#format' => 'uw_tf_standard',
            '#text' => $text,
          ];
        }

        // Set all the info about the date in the array
        // used for the template.
        $uwevents['items'][] = [
          'title' => $event['title'],
          'date' => $event_date,
          'summary' => $summary,
          'url' => $event['path'],
        ];
      }
    }

    // Set the show all button config.
    $uwevents['show_all'] = $events['show_all'];

    // Use Events url and text for button.
    $uwevents['show_all_url'] = self::EVENTSURL;
    $uwevents['show_all_text'] = $this->t('View all Waterloo Events');

    // Return the render array.
    return [
      '#theme' => 'uw_block_waterloo_events',
      '#uwevents' => $uwevents,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Adding the ability to do ajax forms.
    $form['#tree'] = TRUE;

    // Setup the add_more_called flag, so that we know
    // which of the items to load.
    // If we even do one add or remove, we set this flag
    // so that the form will load the values from the
    // form_state, if flag is not set, it will load
    // from the block config.
    if (empty($form_state->get('add_more_called'))) {
      $add_more_called = FALSE;
      $form_state->set('add_more_called', $add_more_called);
    }
    else {
      $add_more_called = $form_state->get('add_more_called');
    }

    // If the add_more_flag is not set, we load from the
    // block config.  If it is set, we load from the form_state.
    if (!$add_more_called) {
      $uwevents = $this->configuration['uwevents'] ?? NULL;
      $form_state->set('uwevents', $uwevents);
    }
    else {
      $uwevents = $form_state->get('uwevents');
    }

    // Get the num_of_rows from the form_state.
    $num_of_rows = $form_state->get('num_of_rows');

    // If the num_of_rows is not set, we first look at
    // the block config and see if we have events, and if
    // so set num_of_rows to the number of events.  If we
    // do not have num_of_rows set, default to 1 (this is
    // the first load of the form).
    if (empty($num_of_rows)) {
      if (isset($uwevents)) {
        $num_of_rows = count($uwevents['items']);
      }
      else {
        $num_of_rows = 1;
      }
    }

    // Set the num_of_rows to the form_state so that we
    // can use it in the ajax calls.
    $form_state->set('num_of_rows', $num_of_rows);

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // The details to hold the events items.
    $form['items_fieldset'] = [
      '#type' => 'details',
      '#title' => $this->t('Items'),
      '#description' => $this->t('You must enter at least one Waterloo Events item.'),
      '#open' => TRUE,
      '#prefix' => '<div id="items-wrapper">',
      '#suffix' => '</div>',
    ];

    // The class to be used for groups.
    $group_class = 'group-order-weight';

    // Build the table.
    $form['items_fieldset']['items'] = [
      '#type' => 'table',

      '#header' => [
        [
          'data' => $this->t('<h4 class="label form-required">Add Waterloo Events items</h4>'),
          'colspan' => 2,
        ],
        // @todo Make this work properly with the remove button.
        // phpcs:disable
        // '',
        // phpcs:enable
        $this->t('Weight'),
      ],
      '#tableselect' => FALSE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $group_class,
        ],
      ],
      '#prefix' => '<div class="uw-events__table">',
      '#suffix' => '</div><p>Leave a URL blank to remove it.</p>',
    ];

    // Step through and events item.
    for ($i = 0; $i < $num_of_rows; $i++) {

      // Reset the settings array.
      $settings = [];

      // Set the table to be draggable.
      $settings['#attributes']['class'][] = 'draggable';

      // Set the weight.
      $settings['#weight'] = $i;

      // The first column of the table, that will house the arrows
      // for rearranging events urls.
      $settings['arrow'] = [
        '#type' => 'markup',
        '#markup' => '',
        '#title_display' => 'invisible',
      ];

      // The link element.
      $settings['url'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Waterloo Events item URL'),
        '#description' => $this->t('Enter a URL starting with https://uwaterloo.ca/events/.'),
        '#default_value' => $uwevents['items'][$i]['url'] ?? '',
      ];

      // If we are on the first events url add the
      // form required class.
      if ($i == 0) {
        $settings['url']['#label_attributes'] = [
          'class' => ['form-required'],
        ];
      }

      // The weight element.
      $settings['weight'] = [
        '#type' => 'weight',
        '#title_display' => 'invisible',
        '#default_value' => $i,
        '#attributes' => ['class' => [$group_class]],
        '#delta' => $num_of_rows,
      ];

      // Add the settings to the items array, which is full row
      // in the table.
      $form['items_fieldset']['items'][] = $settings;
    }

    // The add expand/collapse group button.
    $form['add_group'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Waterloo Events item'),
      '#submit' => [[$this, 'waterlooEventsAddOne']],
      '#ajax' => [
        'callback' => [$this, 'waterlooEventsCallback'],
        'wrapper' => 'items-wrapper',
      ],
      '#attributes' => [
        'class' => ['button--large'],
      ],
    ];

    // The show all details element.
    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('"View all" button'),
      '#open' => TRUE,
    ];

    // Show all form element.
    $form['options']['show_all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the "View all events" button'),
      '#default_value' => $uwevents['show_all'] ?? FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    // Get the triggering element form the form state.
    $triggering_element = $form_state->getTriggeringElement();

    // If we are on an add waterloo events, do not perform the
    // validation, so that we save computation time and do not
    // ping the api too many times.
    if ($triggering_element['#value']->__toString() == 'Add Waterloo Events item') {
      return;
    }

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Have at least an empty array, so that
    // we can check for no urls entered and
    // do not get undefined index.
    $uwevents = [];

    // Step through each of the urls and validate them.
    foreach ($values['items_fieldset']['items'] as $index => $item) {

      // Ensure that we have a url.
      if (
        isset($item['url']) &&
        $item['url'] !== ''
      ) {

        // If the url is not in the proper format, starting with
        // uwaterloo.ca/events, then set the error.
        if (
          strncmp(
            $item['url'],
            self::EVENTSURL,
            strlen(self::EVENTSURL
            )
          ) !== 0) {

          $form_state->setError(
            $form['items_fieldset']['items'][$index]['url'],
            $this->t('Invalid Waterloo Events URL.'),
          );
        }
        // If the URL is set, check that it is a valid one.
        else {

          // Remove the https://.
          $url = str_replace(self::EVENTSURL, '', $item['url']);

          // Get the url for the waterloo events item.
          $url = self::EVENTSURLAPI . '?path=' . $url;

          // Make the call to the API.
          $event = $this->httpClient->get($url, ['verify' => FALSE]);

          // Decode the API call.
          $event = json_decode($event->getBody(), TRUE);

          if (isset($event['data'][0])) {

            // Get the actual news data.
            $event = $event['data'][0];

            // If the event is empty, set the error about invalid url.
            if (empty($event)) {
              $form_state->setError(
                $form['items_fieldset']['items'][$index]['url'],
                $this->t('Invalid Waterloo Events URL.'),
              );
            }
            // Add the url to the array, so that we can ensure
            // that we have some urls.
            else {
              $uwevents[] = [
                'url' => $item['url'],
              ];
            }
          }
          else {
            $form_state->setError(
              $form['items_fieldset']['items'][$index]['url'],
              $this->t('Invalid Waterloo Events URL.'),
            );
          }
        }
      }
    }

    // If there are no events URL, then set the form error.
    if (empty($uwevents)) {
      $form_state->setError(
        $form['items_fieldset']['items'][0]['url'],
        $this->t('You must enter at least one Waterloo Events URL.'),
      );
    }

    // Validate unique URLs.
    // Step through each of the urls.
    foreach ($uwevents as $uwevent) {

      // The count of urls.
      $url_count = 0;

      // Step through each of the urls again, and check
      // if there are any duplicates.
      foreach ($uwevents as $index => $uwevent2) {

        // If there is the same url, increment the count of urls.
        if ($uwevent['url'] == $uwevent2['url']) {
          $url_count++;
        }

        // If there is more than one url, break out of the loop,
        // to save computational tim, and we will handle the form
        // error and break out of the next loop, since we have
        // found a duplicate.
        if ($url_count > 1) {
          break;
        }
      }

      // If there is more than one url, set the form error and
      // break out of the loop.
      if ($url_count > 1) {
        $form_state->setError(
          $form['items_fieldset']['items'][$index]['url'],
          $this->t('You must enter unique Waterloo Events URLs.')
        );
        break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Have at least an empty array.
    $uwevents = [];

    // Step through each of the urls and add to the
    // block config.
    foreach ($values['items_fieldset']['items'] as $item) {

      // If there is a url add it to the array.
      if (
        isset($item['url']) &&
        $item['url'] !== ''
      ) {
        $uwevents['items'][] = [
          'url' => $item['url'],
        ];
      }
    }

    // Set the show all.
    $uwevents['show_all'] = $values['options']['show_all'];

    // Use Events url and text for button.
    $uwevents['show_all_url'] = self::EVENTSURL;
    $uwevents['show_all_text'] = $this->t('View all Waterloo Events');

    // Set the uwevents config for the block.
    $this->configuration['uwevents'] = $uwevents;
  }

  /**
   * Add one more events url to the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function waterlooEventsAddOne(array &$form, FormStateInterface $form_state) {

    // Increase by 1 the number of rows and set it in
    // the form_state.
    $num_of_rows = $form_state->get('num_of_rows');
    $num_of_rows++;
    $form_state->set('num_of_rows', $num_of_rows);

    // Set the add_more_called flag, so that we will load
    // the items by form_state, rather than block config.
    $form_state->set('add_more_called', TRUE);

    // Get the current values of the form_state.
    $values = $form_state->getValues();

    // Get the items from the form state, which will contain
    // the events settings.
    $items = $values['settings']['items_fieldset']['items'];

    // Step through each of the items and set values to be used
    // in the events block config.
    foreach ($items as $item) {

      $uwevents['items'][] = [
        'url' => isset($item['url']) ?? '',
      ];
    }

    // Set the events from the items in the form_state, so that we
    // will load newly added values.
    $form_state->set('uwevents', $uwevents);

    // Rebuild form with 1 extra row.
    $form_state->setRebuild();
  }

  /**
   * The ajax call back for events add one.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The form element to be overwritten.
   */
  public function waterlooEventsCallback(array &$form, FormStateInterface $form_state) {
    return $form['settings']['items_fieldset'];
  }

}
