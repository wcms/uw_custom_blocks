<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UW calendar block.
 *
 * @Block(
 *   id = "uw_cbl_multi_type_calendar",
 *   admin_label = @Translation("Multi type calendar"),
 * )
 */
class UwCblMultiTypeCalendar extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Get the block config.
    $config = $this->configuration;

    // Load the view.
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('uw_view_calendar')
      ->getExecutable();

    // Get the display id.
    $display_id = 'embed_calendar_' . $config['calendar_type'];

    // Set the display of the view based on the config.
    $view->setDisplay($display_id);

    // If there is a default date, add it to the view.
    if ($config['default_date']) {

      // Get the display handler from the view.
      $display_handler = $view->display_handler;

      // Get the style option from the display handler.
      $style = $display_handler->getOption('style');

      // Set the default date in the style.
      $style['options']['calendar_timestamp'] = $config['default_date'];

      // Set the option back in the view.
      $display_handler->setOption('style', $style);
    }

    // Reset the exposed filters array.
    $exposed_filters[] = [];

    $view->setExposedInput($exposed_filters);

    // Step through the content types and set the type filter.
    foreach ($config['content_types'] as $content_type) {
      $exposed_filters['type'][] = $content_type;
    }

    // Set the exposed inputs.
    $view->setExposedInput($exposed_filters);

    // If the show all items is selected in the block
    // config, then remove the promoted filter.
    if (
      isset($config['show_all_items']) &&
      $config['show_all_items']
    ) {

      // Remove the promoted filter.
      $view->removeHandler($display_id, 'filter', 'promote');
    }

    // Execute the view.
    $view->execute();

    // If the allow navigation option is not set, then
    // remove the pager from the view.
    if (!$config['allow_navigation']) {
      unset($view->pager);
    }

    return [
      '#type' => 'view',
      '#name' => 'view_name',
      '#view' => $view,
      '#display_id' => $display_id,
      '#embed' => TRUE,
      '#cache' => $view->getCacheTags(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Get the block config.
    $config = $this->configuration;

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // The content types.
    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content to display'),
      '#options' => [
        'uw_ct_blog' => $this->t('Blog posts'),
        'uw_ct_event' => $this->t('Events'),
        'uw_ct_news_item' => $this->t('News items'),
      ],
      '#multiple' => TRUE,
      '#default_value' => $config['content_types'] ?? [],
      '#required' => TRUE,
    ];

    // The type of calendar to display.
    $form['calendar_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Calendar type'),
      '#options' => [
        'month' => $this->t('Monthly'),
        'week' => $this->t('Weekly'),
      ],
      '#default_value' => $config['calendar_type'] ?? 'month',
    ];

    // The default date element.
    $form['default_date'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default date'),
      '#description' => $this->t('Enter a date in any machine-readable format, or a relative date (e.g. “next month”) for an automatically updating calendar. Leave blank to start with the date for the first piece of content.'),
      '#default_value' => $config['default_date'] ?? 'today',
      '#required' => FALSE,
    ];

    // The allow navigation option.
    $form['allow_navigation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow navigation'),
      '#description' => $this->t('Disable to prevent users from navigating to other dates.'),
      '#default_value' => $config['allow_navigation'] ?? TRUE,
    ];

    // Configure whether to show items promoted to the front page or all.
    $form['show_all_items'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include all eligible items, not just those promoted to the home page'),
      '#default_value' => $this->configuration['show_all_items'] ?? TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Step through each of the content types and get
    // the values.
    foreach ($values['content_types'] as $content_type) {
      if ($content_type !== 0) {
        $content_types[] = $content_type;
      }
    }

    // Set the content type values.
    $this->configuration['content_types'] = $content_types;

    // Set the calendar type.
    $this->configuration['calendar_type'] = $values['calendar_type'];

    // Set the heading level.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Set the allow navigation.
    $this->configuration['allow_navigation'] = $values['allow_navigation'];

    // Set the default date.
    $this->configuration['default_date'] = $values['default_date'];

    // Set the show all items.
    $this->configuration['show_all_items'] = $values['show_all_items'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // If there is a default date set, then check that
    // it is valid.
    if (
      isset($values['default_date']) &&
      $values['default_date'] !== ''
    ) {

      // If the date is invalid set the form error.
      if ((strtotime($values['default_date'])) == FALSE) {
        $form_state->setError(
          $form['default_date'],
          $this->t('You must enter a valid default date value.')
        );
      }
    }
  }

}
