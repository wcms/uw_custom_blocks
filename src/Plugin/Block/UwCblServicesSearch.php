<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Component\Utility\Html;

/**
 * UW custom block service search.
 *
 * @Block(
 *   id = "uw_cbl_services_search",
 *   admin_label = @Translation("Service search"),
 * )
 */
class UwCblServicesSearch extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'search_description' => NULL,
      'search_placeholder' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Set the uuid.
    $service_search['uuid'] = $this->configuration['uuid'] = uniqid();

    // Get the service search form.
    $service_search['form'] = $this->formBuilder->getForm(
      'Drupal\uw_ct_service\Form\ServiceSearchForm',
      (bool) $this->configuration['search_placeholder'],
      $this->configuration['search_placeholder'],
      Html::escape($this->configuration['search_description']),
      $service_search['uuid']
    );

    // If there is no block title, then set the label so that
    // the template will know to place a visually hidden one.
    if (!$this->configuration['label_display']) {
      $service_search['label'] = $this->t('Service search');
    }

    return [
      '#theme' => 'uw_block_form_search',
      '#form_search' => $service_search,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // The description for the service search.
    $form['service_settings']['search_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Search description'),
      '#description' => $this->t('Enter text for the service search description. Leave blank to have no description. (Note: text cannot contain HTML, and line breaks.)'),
      '#default_value' => $this->configuration['search_description'],
    ];

    // The placeholder for the service search.
    $form['service_settings']['search_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search placeholder'),
      '#description' => $this->t('Enter text for the service search placeholder. Leave blank to have no placeholder.'),
      '#default_value' => $this->configuration['search_placeholder'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Set the description and placeholder.
    $this->configuration['search_description'] = $values['service_settings']['search_description'];
    $this->configuration['search_placeholder'] = $values['service_settings']['search_placeholder'];
    $this->configuration['uuid'] = uniqid();
  }

}
