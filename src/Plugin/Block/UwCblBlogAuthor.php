<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UW custom block blog author.
 *
 * @Block(
 *   id = "uw_cbl_blog_author",
 *   admin_label = @Translation("Blog author"),
 * )
 */
class UwCblBlogAuthor extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The variable for route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The routeMatch.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $routeMatch, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $routeMatch;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Check if we are on a node page and if so set the
    // node object, if not author should be blank.
    if ($node = $this->routeMatch->getParameter('node')) {

      // If the node is not an object, meaning it has just
      // passed the nid instead of the object, load the node.
      if (!is_object($node)) {
        $rid = $this->routeMatch->getParameter('node_revision');
        $node = $this->entityTypeManager->getStorage('node')->loadRevision($rid);
      }

      // Get the author field from the node, if there is
      // no author specified the value will be NULL.
      $author = $node->field_author->value;

      // If there is no author in the field, get the last
      // person to revise teh blog post.
      if (!$author) {

        // Set the author to the person who made blog.
        $author = $node->getOwner()->getDisplayName();
      }
    }

    // If there is no node object, means we are probably on the layout
    // settings page, ensure that we have something to display, just
    // set the author to blank.
    else {

      $author = '';
    }

    // Return the build array with the template ane variables.
    return [
      '#theme' => 'uw_block_blog_author',
      '#author' => $author,
    ];
  }

}
