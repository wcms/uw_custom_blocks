<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\media\Entity\Media;
use Drupal\path_alias\AliasManager;
use Drupal\uw_cfg_common\Service\UWService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * UW custom block links.
 *
 * @Block(
 *   id = "uw_cbl_links",
 *   admin_label = @Translation("Links"),
 * )
 */
class UwCblLinks extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Regular expression to detect leading slash for external links.
   */
  public const LEADING_SLASH_REPLACE_REGEX = '/^\/(https?:\/\/)/i';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UW node content.
   *
   * @var \Drupal\uw_cfg_common\Service\UwService
   */
  protected $uwService;

  /**
   * Path alias manager from core.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  protected $pathAliasManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Path validator from the core.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\uw_cfg_common\Service\UwService $uwService
   *   UW Service.
   * @param \Drupal\path_alias\AliasManager $pathAliasManager
   *   The path alias.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Path\PathValidatorInterface $pathValidator
   *   Path validator from core.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    UwService $uwService,
    AliasManager $pathAliasManager,
    RequestStack $requestStack,
    PathValidatorInterface $pathValidator,
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->uwService = $uwService;
    $this->pathAliasManager = $pathAliasManager;
    $this->requestStack = $requestStack;
    $this->pathValidator = $pathValidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uw_cfg_common.uw_service'),
      $container->get('path_alias.manager'),
      $container->get('request_stack'),
      $container->get('path.validator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // The array to store the links.
    $links = [];

    // All the info from the block config about the links.
    $link_info = $this->configuration['links'];

    // If there is a top icon, get the info.  If no top
    // icon set it to null so that it is not displayed.
    if ($link_info['top_icon'] !== NULL) {

      // Load the media entity from the image selected.
      $media = $this->entityTypeManager
        ->getStorage('media')
        ->load($link_info['top_icon']);

      // Get the top icon info.
      $links['top_icon'] = $this->getIconInfo($media);
    }
    else {
      $links['top_icon'] = NULL;
    }

    // Get the link style.
    $links['link_style'] = $link_info['link_style'];

    // Step through each of the items and get the link info.
    foreach ($link_info['items'] as $count => $item) {

      // Step through each piece of the link info and set the
      // variables to correct for templating.
      foreach ($item as $index => $value) {

        // If icon and has a value, then get out its info.
        if ($index == 'icon' && $value !== NULL) {

          // Load the media entity from the image selected.
          $media = $this->entityTypeManager
            ->getStorage('media')
            ->load($value);

          // Get the icon info.
          $links['items'][$count][$index] = $this->getIconInfo($media);
        }
        // Get the background image.
        elseif ($index == 'background_image') {

          // If the background type is color then set to blank.
          // If not then get out its info.
          if ($link_info['items'][$count]['background'] == 'color') {
            $links['items'][$count][$index] = '';
          }
          else {

            // If there is a value get out the icon info.
            if ($value !== NULL) {

              // Load the media entity from the image selected.
              $media = $this->entityTypeManager
                ->getStorage('media')
                ->load($value);

              // Get the responsive image and alt text.
              $links['items'][$count][$index]['image'] = $this->uwService->prepareResponsiveImage($media, 'uw_ris_media');
              $links['items'][$count][$index]['alt'] = $media->field_media_image->alt;
            }
            else {
              $links['items'][$count][$index] = '';
            }
          }
        }
        elseif ($index == 'background_color') {
          if ($link_info['items'][$count]['background'] == 'image' || $link_info['items'][$count]['background'] == 'none') {
            $links['items'][$count][$index] = '';
          }
          else {
            $links['items'][$count][$index] = $value;
          }
        }
        elseif (
          $index == 'link_coloring' ||
          $index == 'image_link_coloring'
        ) {

          if (
            $index == 'image_link_coloring' &&
            $link_info['link_style'] !== 'option' &&
            $links['items'][$count]['background'] == 'image'
          ) {
            $links['items'][$count][$index] = $value;
          }
          elseif (
            $index == 'link_coloring' &&
            $link_info['link_style'] == 'option' &&
            $links['items'][$count]['background'] == 'image'
          ) {
            if ($value == 'default') {
              $links['items'][$count][$index] = theme_get_setting('wcms_colour_scheme', 'uw_fdsu_theme_resp');
            }
            else {
              $links['items'][$count][$index] = $value;
            }
          }
          elseif (
            $index == 'link_coloring' &&
            $link_info['link_style'] == 'buttons' &&
            $links['items'][$count]['background'] !== 'color' &&
            $links['items'][$count]['background'] !== 'image'
          ) {
            if ($value == 'default') {
              $links['items'][$count][$index] = theme_get_setting('wcms_colour_scheme', 'uw_fdsu_theme_resp');
            }
            else {
              $links['items'][$count][$index] = $value;
            }
          }
          else {
            if ($index == 'link_coloring') {
              $links['items'][$count][$index] = $value;
            }
          }
        }
        elseif ($index == 'uri') {

          // For some reason, the # is treated as external, so we want
          // to ensure that if the link is only a #, that it will set
          // that value and use it.
          // May as well do this for all links that are only anchors,
          // because there's no need to send them through the alias
          // lookup and they are for the current page so don't need
          // the base path.
          if (str_starts_with($value, '#')) {
            $links['items'][$count][$index] = $value;
          }
          // If this is not an external link, get the friendly path
          // to the page.  If it is external, just use the value.
          elseif (!UrlHelper::isExternal($value)) {
            // Add the url to the links array.
            $links['items'][$count][$index] = $this->cleanUpLink($value, TRUE, FALSE);
          }
          else {

            // This is an external link so just use the value.
            $links['items'][$count][$index] = $value;
          }
        }
        elseif ($index === 'title') {
          // See if link title needs to be modified.
          if (empty($item['title'])) {

            $links['items'][$count][$index] = $this->getTitleOverride($item['uri']);
          }
          else {
            $links['items'][$count][$index] = $value;
          }
        }
        else {
          $links['items'][$count][$index] = $value;
        }
      }
      $links['items'][$count]['uuid'] = uniqid();
    }

    // Return the build array.
    return [
      '#theme' => 'uw_block_links',
      '#links' => $links,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Array to store the links.
    $links = [];

    // Attach the js library.
    $form['#attached']['library'][] = 'uw_custom_blocks/uw_cbl_links';

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // Adding the ability to do ajax forms.
    $form['#tree'] = TRUE;

    // Setup the add_more_called flag, so that we know
    // which of the items to load.
    // If we even do one add or remove, we set this flag
    // so that the form will load the values from the
    // form_state, if flag is not set, it will load
    // from the block config.
    if (empty($form_state->get('add_more_called'))) {
      $add_more_called = FALSE;
      $form_state->set('add_more_called', $add_more_called);
    }
    else {
      $add_more_called = $form_state->get('add_more_called');
    }

    // If the add_more_flag is not set, we load from the
    // block config.  If it is set, we load from the form_state.
    if (!$add_more_called) {
      $links = $this->configuration['links'] ?? NULL;
      $form_state->set('links', $links);
    }
    else {

      $links = $form_state->get('links');
    }

    // Get the num_of_rows from the form_state.
    $num_of_rows = $form_state->get('num_of_rows');

    // If the num_of_rows is not set, we first look at
    // the block config and see if we have links, and if
    // so set num_of_rows to the number of links.  If we
    // do not have num_of_rows set, default to 1 (this is
    // the first load of the form).
    if (empty($num_of_rows)) {
      if (isset($links['items'])) {
        $num_of_rows = count($links['items']);
      }
      else {
        $num_of_rows = 1;
      }
    }

    // Set the num_of_rows to the form_state so that we
    // can use it in the ajax calls.
    $form_state->set('num_of_rows', $num_of_rows);

    // The class to be used for groups.
    $group_class = 'group-order-weight';

    $form['link_style'] = [
      '#type' => 'details',
      '#title' => $this->t('Link style'),
      '#open' => TRUE,
    ];

    // The link style element.
    $form['link_style']['style'] = [
      '#type' => 'select',
      '#title' => $this->t('Style'),
      '#options' => [
        'buttons' => $this->t('Buttons'),
        'navbar' => $this->t('Navigation bar'),
        'option' => $this->t('Option links'),
        'quick' => $this->t('Quick links'),
        'related' => $this->t('Related links'),
      ],
      '#default_value' => $links['link_style'] ?? 'related',
      '#description' => $this->t('<strong>Buttons:</strong> Links in a button format, displayed horizontally across the page.<br>
<strong>Navigation bar:</strong> Links in a horizontal navigation bar format. Best used with anchor links on the same page.<br>
<strong>Option links:</strong> Large boxed links, similar to a banner link. Works well with background images.<br>
<strong>Quick links:</strong> Center-aligned links with no bullets in front<br>
<strong>Related links:</strong> Left-aligned links with bullets in front'),
    ];

    $form['top_icon'] = [
      '#type' => 'details',
      '#title' => $this->t('Top icon'),
      '#open' => TRUE,
    ];

    // The top icon element.
    $form['top_icon']['icon'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => ['uw_mt_icon'],
      '#title' => $this->t('Add icon (optional)'),
      '#default_value' => $links['top_icon'] ?? '',
      '#description' => $this->t('When provided, the icon will display at the top of the block. If the block title is enabled, this icon will be above the title.'),
    ];

    // The field set for the links groups.
    $form['items_fieldset'] = [
      '#type' => 'container',
      '#prefix' => '<div id="items-wrapper">',
      '#suffix' => '</div>',
    ];

    // Build the table.
    $form['items_fieldset']['items'] = [
      '#type' => 'table',

      '#header' => [
        [
          'data' => $this->t('<h4 class="label">Add links</h4>'),
          'colspan' => 2,
        ],
        // @todo Make this work properly with the remove button.
        // phpcs:disable
        // '',
        // phpcs:enable
        $this->t('Weight'),
      ],
      '#empty' => $this->t('<span class="form-required">You must have at least one link.</span>'),
      '#tableselect' => FALSE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $group_class,
        ],
      ],
      '#prefix' => '<div class="uw-links__table">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $num_of_rows; $i++) {

      // Reset the settings array.
      $settings = [];

      // Set the table to be draggable.
      $settings['#attributes']['class'][] = 'draggable';

      // Set the weight.
      $settings['#weight'] = $i;

      // The first column of the table, that will house the arrows
      // for rearranging links.
      $settings['arrow'] = [
        '#type' => 'markup',
        '#markup' => '',
        '#title_display' => 'invisible',
      ];

      // If there is a link already set, setup the details to
      // have the link in the title so when it is closed,
      // we can see what the actual is.
      $value_uri = $this->cleanUpLink($links['items'][$i]['uri'] ?? '');

      if (isset($links['items'][$i]['uri'])) {
        $link = $this->t('Link (@uri)', ['@uri' => $value_uri]);
      }
      else {
        $link = $this->t('Link');
      }

      // Container for the link.
      $settings['link_info'] = [
        '#type' => 'details',
        '#title' => $link,
        '#open' => $i === $num_of_rows - 1,
        '#prefix' => '<div class="uw-links__link-info">',
        '#suffix' => '</div>',
      ];

      // The link element.
      $settings['link_info']['uri'] = [
        '#type' => 'linkit',
        '#title' => $this->t('URL'),
        '#description' => $this->t('Start typing to see a list of results. Click to select.'),
        '#autocomplete_route_name' => 'linkit.autocomplete',
        '#autocomplete_route_parameters' => [
          'linkit_profile_id' => 'default',
        ],
        '#default_value' => $value_uri,
        '#required' => FALSE,
      ];

      // The link title element.
      $settings['link_info']['title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Link text'),
        '#default_value' => $links['items'][$i]['title'] ?? '',
      ];

      // The description element.
      $settings['link_info']['description'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Description'),
        '#default_value' => $links['items'][$i]['description'] ?? NULL,
      ];

      // The icon element.
      $settings['link_info']['icon'] = [
        '#type' => 'media_library',
        '#allowed_bundles' => ['uw_mt_icon'],
        '#title' => $this->t('Icon (optional)'),
        '#default_value' => $links['items'][$i]['icon'] ?? '',
        '#description' => $this->t('This will display above, below, or beside the link, depending on the selected link style.'),
      ];

      // The choice for background element.
      $settings['link_info']['background'] = [
        '#type' => 'select',
        '#title' => $this->t('Background'),
        '#options' => [
          'none' => $this->t('None'),
          'image' => $this->t('Image'),
          'color' => $this->t('Solid colour'),
        ],
        '#default_value' => $links['items'][$i]['background'] ?? '',
      ];

      // The background color element.
      $settings['link_info']['background_color'] = [
        '#type' => 'select',
        '#title' => $this->t('Background colour'),
        '#options' => [
          'neutral' => $this->t('Light grey'),
          'org-default' => $this->t('UW Gold'),
          'org-default-b' => $this->t('UW Black'),
          'org-art' => $this->t('Orange (Arts)'),
          'org-eng' => $this->t('Purple (Engineering)'),
          'org-env' => $this->t('Green (Environment)'),
          'org-ahs' => $this->t('Teal (Health)'),
          'org-mat' => $this->t('Pink (Mathematics)'),
          'org-sci' => $this->t('Blue (Science)'),
          'org-school' => $this->t('Red (school)'),
          'org-cgc' => $this->t('Red (Conrad Grebel University College)'),
          'org-ren' => $this->t('Green (Renison University College)'),
          'org-stj' => $this->t('Green (St. Jerome’s University)'),
          'org-stp' => $this->t('Blue (United College)'),
        ],
        '#default_value' => $links['items'][$i]['background_color'] ?? '',
      ];

      // The background image element.
      $settings['link_info']['background_image'] = [
        '#type' => 'media_library',
        '#allowed_bundles' => ['uw_mt_image'],
        '#title' => $this->t('Background image'),
        '#default_value' => $links['items'][$i]['background_image'] ?? '',
        '#description' => $this->t('This image will be proportionally scaled as needed to ensure it covers the entire area behind the link. This may mean that the top and bottom or left and right of the image get cut off. Images should not contain text or any other content that would require a description, and must be selected so that text over top can maintain sufficient colour contrast.'),
      ];

      $options = _uw_cfg_common_get_faculty_color_options('uni');
      $options['default'] = $this->t('Default');

      // The link color element.
      $settings['link_info']['link_coloring'] = [
        '#type' => 'select',
        '#title' => $this->t('Link colouring'),
        '#options' => $options,
        '#default_value' => $links['items'][$i]['link_coloring'] ?? 'default',
        '#description' => $this->t('Affects the colour of the entire link, except for option links with an image background, where it sets the colour for the link text and hover effect.'),
      ];

      // The link color element when there is an image backgrounds.
      $settings['link_info']['image_link_coloring'] = [
        '#type' => 'select',
        '#title' => $this->t('Image link colouring'),
        '#options' => [
          'black-without' => $this->t('Black without shadows (for lighter images)'),
          'black-with' => $this->t('Black with shadows (for lighter images)'),
          'white-without' => $this->t('White without shadows (for darker images)'),
          'white-with' => $this->t('White with shadows (for darker images)'),
        ],
        '#default_value' => $links['items'][$i]['image_link_coloring'] ?? 'white-with',
      ];

      // The weight element.
      $settings['weight'] = [
        '#type' => 'weight',
        '#title_display' => 'invisible',
        '#default_value' => $i,
        '#attributes' => ['class' => [$group_class]],
      ];

      // Add the settings to the items array, which is full row
      // in the table.
      $form['items_fieldset']['items'][] = $settings;
    }

    // The add links group button.
    $form['add_group'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add link'),
      '#submit' => [[$this, 'linkAddOne']],
      '#ajax' => [
        'callback' => [$this, 'linkCallback'],
        'wrapper' => 'items-wrapper',
      ],
      '#attributes' => [
        'class' => ['button--large'],
      ],
      '#suffix' => '<p>Leave a link blank to remove it.</p>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Get the items from the form state.
    $items = $values['items_fieldset']['items'];

    // Have at least blank links.
    $links = [];

    // Step through each item and get only the links
    // that are not empty.
    foreach ($items as $index => $item) {

      // Ensure that we are only adding links that actually,
      // have a value.  If there is no value, then check its
      // info and if any of the info have values, set the error
      // that it needs a link of there is info.
      if (
        $item['link_info']['uri'] !== NULL &&
        $item['link_info']['uri'] !== ''
      ) {

        $links[] = [
          'uri' => $item['link_info']['uri'],
        ];
      }
    }

    // If there are no links, set the error.
    // If not check for other empty values.
    if (count($links) == 0) {
      $form_state->setError(
        $form['items_fieldset']['items'][0]['link_info']['uri'],
        $this->t('You must have at least one link.')
      );
    }
    else {

      // Step through each of the link info and if there
      // is a value, set the error.
      foreach ($item['link_info'] as $key => $value) {

        // If this is a background and there is no background image,
        // then set the form error.
        if (
          $items[$index]['link_info']['uri'] !== '' &&
          $key == 'background' &&
          $value == 'image' &&
          $items[$index]['link_info']['background_image'] == NULL
        ) {

          $form_state->setError(
            $form['items_fieldset']['items'][$index]['link_info']['background_image'],
            $this->t('You must select a background image.')
          );
        }

        // If the key is a URL, we have to ensure that it is somewhat
        // a valid url, meaning start with a /, http:// or https://.
        if ($key == 'uri') {

          // If the URL does not start with a /, http:// or https://,
          // set the form error.
          if (
            $value !== '' &&
            !preg_match('/^(https?:\/\/.)/i', $value) &&
            !preg_match('/^[\/#]/', $value)
          ) {

            $form_state->setError(
              $form['items_fieldset']['items'][$index]['link_info']['uri'],
              $this->t('URL must start with a #, /, http:// or https://.')
            );
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Load in the values from the form_sate.
    $values = $form_state->getValues();

    // Get the items from the form_state.
    $items = $values['items_fieldset']['items'];

    // Array of links, at least blank so no undefined index.
    $links = [];

    // Set the top icon and link style.
    $links['top_icon'] = $values['top_icon']['icon'];
    $links['link_style'] = $values['link_style']['style'];

    // Counter for the number of links.
    $count = 0;

    // Step through each of the items and set links if they exist.
    foreach ($items as $item) {
      // Remove any keys that aren't used by the link type.
      // This is a series of ifs because more than one can be true.
      if ($links['link_style'] != 'option') {
        if ($item['link_info']['background'] != 'none') {
          $item['link_info']['link_coloring'] = NULL;
        }
        if ($item['link_info']['background'] != 'image') {
          $item['link_info']['image_link_coloring'] = NULL;
        }
        if ($item['link_info']['background'] != 'color') {
          $item['link_info']['background_color'] = NULL;
        }
      }
      else {
        // The option style has different rules.
        if ($item['link_info']['background'] == 'color') {
          $item['link_info']['link_coloring'] = NULL;
        }
      }

      // Ensure that we are only adding links that actually,
      // have a value.
      if (
        $item['link_info']['uri'] !== NULL &&
        $item['link_info']['uri'] !== ''
      ) {

        // Step through each of the keys and add them to array.
        foreach (array_keys($item['link_info']) as $key) {

          if (
            $key == 'background_image' &&
            $item['link_info']['background'] == 'color'
          ) {
            $links['items'][$count][$key] = NULL;
          }
          elseif (
            $key == 'background_color' &&
            $item['link_info']['background'] == 'image'
          ) {
            $links['items'][$count][$key] = NULL;
          }
          elseif (
            $item['link_info']['background'] == 'none' &&
            (
              $key == 'background_color' ||
              $key == 'background_image'
            )
          ) {
            $links['items'][$count][$key] = NULL;
          }
          elseif ($key === 'uri') {
            $links['items'][$count][$key] = $this->cleanUpLink($item['link_info'][$key]);
          }
          else {
            $links['items'][$count][$key] = $item['link_info'][$key];
          }
        }

        // Increment the counter for the number of links.
        $count++;
      }
    }

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Save the ecs in the block config.
    $this->configuration['links'] = $links;
  }

  /**
   * Add one more link to the form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function linkAddOne(array &$form, FormStateInterface $form_state) {

    // At least have no links.
    $links = [];

    // Increase by 1 the number of rows and set it in
    // the form_state.
    $num_of_rows = $form_state->get('num_of_rows');
    $num_of_rows++;
    $form_state->set('num_of_rows', $num_of_rows);

    // Set the add_more_called flag, so that we will load
    // the items by form_state, rather than block config.
    $form_state->set('add_more_called', TRUE);

    // Get the current values of the form_state.
    $values = $form_state->getValues();

    // Set the top icon and link style.
    $links['top_icon'] = $values['settings']['top_icon']['icon'];
    $links['link_style'] = $values['settings']['link_style']['style'];

    // Get the items from the form state, which will contain
    // the e/c settings.
    $items = $values['settings']['items_fieldset']['items'];

    // Step through each of the items and set links if they exist.
    foreach ($items as $count => $item) {

      // Ensure that we are only adding links that actually,
      // have a value.
      if ($item['link_info']['uri'] !== NULL) {

        // Step through each of the keys and add them to array.
        foreach (array_keys($item['link_info']) as $key) {

          if (
            $key == 'background_image' &&
            $item['link_info']['background'] == 'color'
          ) {
            $links['items'][$count][$key] = '';
          }
          elseif (
            $key == 'background_color' &&
            $item['link_info']['background'] == 'image'
          ) {
            $links['items'][$count][$key] = '';
          }
          elseif ($key == 'link') {
            $links['items'][$count]['link']['uri'] = $item[$key]['uri'];
            $links['items'][$count]['link']['title'] = $item[$key]['title'];
          }
          else {
            $links['items'][$count][$key] = $item['link_info'][$key];
          }
        }
      }
    }

    // Set the ecs from the items in the form_state, so that we
    // will load newly added values.
    $form_state->set('links', $links);

    // Rebuild form with 1 extra row.
    $form_state->setRebuild();
  }

  /**
   * The ajax call back for link.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The form element to be overwritten.
   */
  public function linkCallback(array &$form, FormStateInterface $form_state) {
    return $form['settings']['items_fieldset'];
  }

  /**
   * Function to get info about the icon.
   *
   * @param \Drupal\media\Entity\Media $media
   *   The media object.
   *
   * @return array
   *   Array of info about the icon.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getIconInfo(Media $media): array {

    // Load the image style.
    $style = $this->entityTypeManager
      ->getStorage('image_style')
      ->load('thumbnail');

    // Get the file entity based on the media entity.
    // Get the file entity based on the media entity.
    $file = $this->entityTypeManager
      ->getStorage('file')
      ->load($media->field_media_image_1->target_id);

    // Get the url to the icon.
    return [
      'url' => $style->buildUrl($file->getFileUri()),
      'alt' => $media->field_media_image_1->alt,
    ];
  }

  /**
   * Cleans up links by removing 'internal:' and base path.
   *
   * @param string $raw_link
   *   The original link that needs to be cleaned up.
   * @param bool $return_alias
   *   Should alias be returned instead of an internal url.
   * @param bool $prefix_base_path
   *   Should base_path be concateneted before url.
   *
   * @return string
   *   The cleaned link with specified prefixes removed.
   */
  private function cleanUpLink(string $raw_link, bool $return_alias = FALSE, bool $prefix_base_path = TRUE): string {
    if (empty($raw_link)) {
      return $raw_link;
    }

    // Get the base path of the site, this is required for Pantheon.
    $base_path = $this->requestStack
      ->getCurrentRequest()
      ?->getBasePath();

    // Function to modify its own variable, leaving function argument unchanged.
    $link = $raw_link;

    // Remove internal: from the link. This has to go first, before base_path
    // check. Links are formatted with internal followed by base_path, if it
    // exists.
    if (str_starts_with($link, 'internal:')) {
      $link = substr_replace($link, '', 0, strlen('internal:'));
    }

    // In some cases, the external link is prefixed with a leading slash. This
    // removes that leading slash.
    $updated = preg_replace(self::LEADING_SLASH_REPLACE_REGEX, '$1', $link);

    // In case match is not found, unchanged value is returned.
    // Null is returned in case of an error.
    if (is_string($updated) && $link !== $updated) {
      $link = $updated;
    }

    $path = $this->getInternalUrlIfValid($link, $base_path);

    // If node alias is used, modify it so a canonical link is used.
    if ($path && !$path->isExternal()) {
      if ($return_alias) {
        $canonical = $path->toString();
      }
      else {
        $canonical = $path->getInternalPath();

        // Detects if there is a fragment or query string.
        $fragment_query = preg_replace('/^[^#?]+/', '', $path->toString());

        // Need to check if fragment/query starts with # or ?, to avoid
        // a scenario where the link is returned unchanged.
        if (!empty($fragment_query) && (str_starts_with($fragment_query, '#') || str_starts_with($fragment_query, '?'))) {
          $canonical .= $fragment_query;
        }
      }

      if (!str_starts_with($canonical, '/')) {
        $canonical = '/' . $canonical;
      }

      if ($base_path && $prefix_base_path) {
        $canonical = $base_path . $canonical;
      }

      if ($link !== $canonical) {
        $link = $canonical;
      }
    }

    return $link;
  }

  /**
   * Retrieves an overridden title based on the given URL.
   *
   * This method checks if the URL corresponds to routed entity,
   * such as a node, view, taxonomy term, or user. If found, it retrieves
   * the title or name of the entity as the overridden title.
   *
   * @param string $url
   *   The URL to validate and extract the title from.
   *
   * @return string
   *   The overridden title is if available, or an empty string
   *   if no title can be determined.
   */
  private function getTitleOverride(string $url): string {
    $title = "";

    $link = $this->cleanUpLink($url, prefix_base_path: FALSE);

    /** @var \Drupal\Core\Url|bool $route_check */
    $path = $this->pathValidator->getUrlIfValidWithoutAccessCheck($link);
    if ($path && $path->isRouted()) {

      $route = $path->getRouteName();
      $params = $path->getRouteParameters();

      if (str_starts_with($route, 'view')) {
        /** @var \Drupal\views\Entity\View $view */
        $view = $this->entityTypeManager->getStorage('view')
          ->load($params['view_id']);

        if ($view) {
          $display = $view->getDisplay($params['display_id']);

          // Get display id title, if that fails, use view title.
          $title = $display['display_options']['title'] ?? $view->label();
        }
      }
      // This part should take care of nodes, taxonomy terms and webforms.
      elseif ($route === 'entity.node.canonical') {
        $route_parts = explode('.', $route);
        $entity_type = $route_parts[1];
        if (isset($params[$entity_type])) {
          $entity_id = $params[$entity_type];

          $title = $this->entityTypeManager->getStorage($entity_type)
            ?->load($entity_id)
            ?->label();
        }
      }
    }

    // In case there was a NULL pointer exception when loading the entity
    // title will be NULL, and the function must return string.
    // Returning an empty string.
    return $title ?? "";
  }

  /**
   * Validates and retrieves an internal URL if applicable.
   *
   * Checks if the provided URL starts with the specified base path
   * and attempts to validate it as an internal path without access checks.
   *
   * @param string $url
   *   The URL to be validated.
   * @param string $base_path
   *   The base path to check against.
   *
   * @return \Drupal\Core\Url|null
   *   The valid internal URL object if the URL is valid and matches the
   *   base path, or NULL otherwise.
   */
  private function getInternalUrlIfValid(string $url, string $base_path): ?Url {
    $link = $url;

    // Remove base path, this will be used to validate url.
    if ($base_path && str_starts_with(strtolower($link), $base_path . '/')) {
      $link = substr_replace($link, '', 0, strlen($base_path));
    }

    if ($path = $this->pathValidator->getUrlIfValidWithoutAccessCheck($link)) {
      return $path;
    }

    return NULL;
  }

}
