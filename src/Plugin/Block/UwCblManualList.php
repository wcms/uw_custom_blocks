<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\uw_custom_blocks\Service\UwBlockManualElements;
use Drupal\uw_custom_blocks\Service\UwBlockManualGetConfig;
use Drupal\uw_custom_blocks\Service\UwBlockManualRender;
use Drupal\uw_custom_blocks\Service\UwBlockService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * UW Manual listing block.
 *
 * @Block(
 *   id = "uw_cbl_manual_list",
 *   admin_label = @Translation("Manual list"),
 * )
 */
class UwCblManualList extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * UW block service.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockService
   */
  protected $uwBlockService;

  /**
   * UW block manual form elements.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockManualElements
   */
  protected $uwBlockManualElements;

  /**
   * UW block manual get config.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockManualGetConfig
   */
  protected $uwBlockManualGetConfig;

  /**
   * UW block manual get render array.
   *
   * @var \Drupal\uw_custom_blocks\Service\UwBlockManualRender
   */
  protected $uwBlockManualRender;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\uw_custom_blocks\Service\UwBlockService $uwBlockService
   *   The UW block service.
   * @param \Drupal\uw_custom_blocks\Service\UwBlockManualElements $uwBlockManualElements
   *   The manual block elements.
   * @param \Drupal\uw_custom_blocks\Service\UwBlockManualGetConfig $uwBlockManualGetConfig
   *   The manual block get config.
   * @param \Drupal\uw_custom_blocks\Service\UwBlockManualRender $uwBlockManualRender
   *   The manual block render array.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    UwBlockService $uwBlockService,
    UwBlockManualElements $uwBlockManualElements,
    UwBlockManualGetConfig $uwBlockManualGetConfig,
    UwBlockManualRender $uwBlockManualRender,
    EntityTypeManagerInterface $entityTypeManager
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->uwBlockService = $uwBlockService;
    $this->uwBlockManualElements = $uwBlockManualElements;
    $this->uwBlockManualGetConfig = $uwBlockManualGetConfig;
    $this->uwBlockManualRender = $uwBlockManualRender;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('uw_custom_blocks.uw_block_service'),
      $container->get('uw_custom_blocks.uw_block_manual_elements'),
      $container->get('uw_custom_blocks.uw_block_manual_get_config'),
      $container->get('uw_custom_blocks.uw_block_manual_render'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    // Return the form_id.
    return 'uw_cbl_manual_list';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Have at least empty build array to return.
    $build = [];

    // The block config.
    $config = $this->configuration;

    $build = $this->uwBlockManualRender->getRenderArray(
      $config['content_type'],
      $config
    );

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm(
    $form,
    FormStateInterface $form_state
  ) {

    // The list of form elements.
    $form = [];

    // Ensure that we can use ajax.
    $form['#tree'] = TRUE;

    // Get the config.
    $config = $this->configuration;

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($config);

    // The content type form element.
    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of content'),
      '#options' => $this->uwBlockService->getManualContentTypes(),
      '#description' => $this->t('Select the content to list'),
      '#required' => TRUE,
      '#empty_option' => '-- Select --',
      '#default_value' => $config['content_type'] ?? '',
    ];

    // Get the form elements.
    $this->uwBlockManualElements->getManualFormElements(
      $form,
      $config,
      $form_state
    );

    // Add library to the form.
    $form['#attached']['library'][] = 'uw_custom_blocks/uw_cbl_manual_list';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit(
    $form,
    FormStateInterface $form_state
  ) {

    // Load in the values from the form_state.
    $values = $form_state->getValues();

    // Get the content type from the values.
    $content_type = $values['content_type'];

    // Put all the items into an array using the weight.
    // If there is more than one in a given weight, we will
    // sort by the title later.
    foreach ($values[$content_type]['items_fieldset'][$content_type . '_ids'] as $value) {
      $weight_items[$value['weight']][] = $value['id'];
    }

    // Counter to be used for weight.
    $counter = 0;

    // Ensure weight items are in order of weight.
    // Without this they are in the originally listed order.
    ksort($weight_items);

    // Step through all the weighted items and check if
    // there is more than one, and if so, sort by title.
    foreach ($weight_items as $weight_item) {

      // If there is more than one in a given weight, then step
      // through, get all the titles, then sort that array.
      if (count($weight_item) > 1) {

        // Reset the sorted items array.
        $sorted_items = [];

        // Step through each in the weight and get the title
        // from the node.
        foreach ($weight_item as $wi) {
          $node = $this->entityTypeManager
            ->getStorage('node')
            ->load($wi);
          $sorted_items[$wi] = $node->getTitle();
        }

        // Sort the array by the title, keeping the indexes.
        asort($sorted_items);

        // Now that the given weight is sorted by title, we can
        // simply add to an array.
        foreach (array_keys($sorted_items) as $key) {

          // Add the item to the array.
          $items_to_use[] = [
            'id' => $key,
            'weight' => $counter,
          ];

          // Increment the counter so we have the correct index.
          $counter++;
        }
      }
      else {

        // If we reach here there is only one item in the given weight,
        // so we can simply add it to the array.
        $items_to_use[] = [
          'id' => $weight_item[0],
          'weight' => $counter,
        ];

        // Increment the counter, so we get the correct index.
        $counter++;
      }
    }

    // Now replace the items with the sorted by weight and title array.
    $values[$content_type]['items_fieldset'][$content_type . '_ids'] = $items_to_use;

    // Set the content type and heading level
    // in the block config.
    $this->configuration['content_type'] = $content_type;
    $this->configuration['heading_level'] = $values['heading_level'];

    // Step through each of the content types and reset the config.
    foreach (array_keys($this->uwBlockService->getManualContentTypes()) as $ct) {
      $this->configuration[$ct] = [];
    }

    // Set the selected content type config.
    $this->configuration[$content_type] = $this->uwBlockManualGetConfig->getConfig(
      $content_type,
      $values
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Get the content type from the values.
    $content_type = $values['content_type'];

    // Get the title, so we can display the content type properly.
    $title = str_replace('_', ' ', $content_type);

    // Get the name of content type ids.
    $content_ids = $content_type . '_ids';

    // If there is no value in the nid, set the form error.
    if (
      isset($values[$content_type]['items_fieldset'][$content_ids]) &&
      count($values[$content_type]['items_fieldset'][$content_ids]) > 0 &&
      current($values[$content_type]['items_fieldset'][$content_ids])['id'] == NULL
    ) {
      $form_state->setError(
        $form[$content_type]['items_fieldset'][$content_ids][0],
        $this->t(
          'You must select at least one @title',
          ['@title' => $title]
        )
      );
    }
  }

}
