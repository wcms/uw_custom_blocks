<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Component\Utility\Html;

/**
 * UW custom block catalog search.
 *
 * @Block(
 *   id = "uw_cbl_catalog_search",
 *   admin_label = @Translation("Catalog search"),
 * )
 */
class UwCblCatalogSearch extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, FormBuilderInterface $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'catalog' => NULL,
      'search_description' => NULL,
      'search_placeholder' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Set the uuid.
    $catalog_search['uuid'] = $this->configuration['uuid'] = uniqid();

    // Flag that there are catalogs to search.
    $catalog_available_flag = TRUE;

    // The error message for when there are no catalogs.
    $catalog_available_error_message = 'There are no catalog(s) available to search.';

    // Load all the terms from the catalogs.
    $terms = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->loadTree('uw_vocab_catalogs');

    // If there are no terms, set the flag.
    if (count($terms) == 0) {
      $catalog_available_flag = FALSE;
    }

    // If this is not an all catalog search, check if there is a term.
    elseif ($this->configuration['catalog'] !== "0") {

      // Load the taxonomy term.
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($this->configuration['catalog']);

      // If the term is null, set the flag.
      if ($term == NULL) {
        $catalog_available_flag = FALSE;
      }
    }

    // If there is a catalog, then load form, if not set error.
    if ($catalog_available_flag) {

      // Get the catalog search form using the options selected.
      $catalog_search['form'] = $this->formBuilder->getForm(
        'Drupal\uw_ct_catalog\Form\CatalogSearchForm',
        $this->configuration['catalog'],
        Html::escape($this->configuration['search_description']),
        (bool) $this->configuration['search_placeholder'],
        $this->configuration['search_placeholder'],
        $catalog_search['uuid']
      );
    }
    else {
      $catalog_search['error'] = $catalog_available_error_message;
    }

    // If there is no block title, then set the label so that
    // the template will know to place a visually hidden one.
    if (!$this->configuration['label_display'] && $catalog_available_flag) {
      $catalog_search['label'] = $this->t('Catalog search');
    }

    return [
      '#theme' => 'uw_block_form_search',
      '#form_search' => $catalog_search,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Get all the catalogs, which are terms in catalogs vocabulary.
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree('uw_vocab_catalogs');

    // Set the all to search for catalogs options.
    $options[0] = 'All';

    // Step through each of the terms and set the options
    // to search for catalogs.
    foreach ($terms as $term) {
      $options[$term->tid] = $term->name;
    }

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // The catalog to search for.
    $form['catalog_settings']['catalog'] = [
      '#type' => 'radios',
      '#title' => $this->t('Catalog to search'),
      '#options' => $options,
      '#default_value' => $this->configuration['catalog'],
      '#required' => TRUE,
    ];

    // The description for the catalog search.
    $form['catalog_settings']['search_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Search description'),
      '#description' => $this->t('Enter text for the catalog search description. Leave blank to have no description. (Note: text cannot contain HTML, and line breaks.)'),
      '#default_value' => $this->configuration['search_description'],
    ];

    // The placeholder for the catalog search.
    $form['catalog_settings']['search_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search placeholder'),
      '#description' => $this->t('Enter text for the catalog search placeholder. Leave blank to have no placeholder.'),
      '#default_value' => $this->configuration['search_placeholder'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Set the catalog and description.
    $this->configuration['catalog'] = $values['catalog_settings']['catalog'];
    $this->configuration['search_description'] = $values['catalog_settings']['search_description'];
    $this->configuration['search_placeholder'] = $values['catalog_settings']['search_placeholder'];
  }

}
