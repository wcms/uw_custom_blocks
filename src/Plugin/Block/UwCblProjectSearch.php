<?php

namespace Drupal\uw_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;

/**
 * UW custom block project search.
 *
 * @Block(
 *   id = "uw_cbl_project_search",
 *   admin_label = @Translation("Project search"),
 * )
 */
class UwCblProjectSearch extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a BlockComponentRenderArray object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, FormBuilderInterface $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Set the uuid.
    $project_search['uuid'] = $this->configuration['uuid'] = uniqid();

    // Get the catalog search form using the options selected.
    $project_search['form'] = $this->formBuilder->getForm(
      'Drupal\uw_ct_project\Form\UwProjectForm',
      $this->configuration['search_option'],
      $this->configuration['search_description'],
      $this->configuration['search_placeholder'],
      $project_search['uuid']
    );

    if ($this->configuration['search_option'] == 'text') {
      $project_search['#attributes']['class'][] = 'uw-project-search-form';
    }

    // If there is no block title, then set the label so that
    // the template will know to place a visually hidden one.
    if (
      !$this->configuration['label_display'] &&
      $this->configuration['search_option'] !== 'all'
    ) {
      $project_search['label'] = $this->t('Project search');
    }

    return [
      '#theme' => 'uw_block_form_search',
      '#form_search' => $project_search,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = [];

    // The heading level.
    $form['heading_level'] = _uw_custom_blocks_get_heading_level_form_element($this->configuration);

    // Search option fieldset.
    $form['search_option'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Project search options'),
    ];

    // The search options.
    $form['search_option']['option'] = [
      '#type' => 'select',
      '#options' => [
        'text' => $this->t('Text only'),
        'all' => $this->t('All options'),
      ],
      '#default_value' => $this->configuration['search_option'] ?? 'text',
    ];

    // The description for the project search.
    $form['project_settings']['search_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Search description'),
      '#description' => $this->t('Enter text for the catalog search description. Leave blank to have no description. (Note: text cannot contain HTML, and line breaks.)'),
      '#default_value' => $this->configuration['search_description'] ?? NULL,
    ];

    // The placeholder for the project search.
    $form['project_settings']['search_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search placeholder'),
      '#description' => $this->t('Enter text for the catalog search placeholder. Leave blank to have no placeholder.'),
      '#default_value' => $this->configuration['search_placeholder'] ?? NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Add the heading level setting to the config.
    $this->configuration['heading_level'] = $values['heading_level'];

    // Set the search options.
    $this->configuration['search_option'] = $values['search_option']['option'];
    $this->configuration['search_description'] = $values['project_settings']['search_description'];
    $this->configuration['search_placeholder'] = $values['project_settings']['search_placeholder'];
  }

}
