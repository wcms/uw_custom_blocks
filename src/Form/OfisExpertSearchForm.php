<?php

namespace Drupal\uw_custom_blocks\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OfisExpertSearchForm.
 *
 * Form for ofis expert search.
 */
class OfisExpertSearchForm extends FormBase {

  /**
   * The http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructor for ofis expert search.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The guzzle http client.
   */
  public function __construct(ClientInterface $httpClient) {

    $this->httpClient = $httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('http_client'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ofis_expert_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    string $dept = NULL,
    string $uuid = NULL
  ) {

    // The text to be used in the search.
    $form['search'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'id' => 'ofis-search-' . $uuid,
        'aria-controls' => 'ofis-results-' . $uuid,
      ],
      '#placeholder' => $this->t('Search for keyword, name, or research area'),
    ];

    // Create the submit button.
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => '::getExperts',
        'disable-refocus' => FALSE,
        'event' => 'click',
        'wrapper' => 'ofis-results-' . $uuid,
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Searching ...'),
        ],
      ],
      '#attributes' => [
        'data-uuid' => $uuid,
      ],
    ];

    // The department to search.
    $form['dept'] = [
      '#type' => 'hidden',
      '#default_value' => $dept,
    ];

    // The hidden uuid element.
    $form['uuid'] = [
      '#type' => 'hidden',
      '#default_value' => $uuid,
    ];

    // Attach the css for the form.
    $form['#attached']['library'][] = 'uw_custom_blocks/ofis_expert_search';

    // Add the form id with the uuid.
    $form['#attributes'] = [
      'id' => 'ofis-expert-search-form--' . $uuid,
      'data-uuid' => $uuid,
    ];

    return $form;
  }

  /**
   * Function to get the expert.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The element to be updated.
   */
  public function getExperts(
    array &$form,
    FormStateInterface $form_state
  ) {

    // Get the uuid from the form state.
    $uuid = $form_state->getValue('uuid');

    // Reset the html and results.
    $html = '';
    $results = [];

    // Get the values from the form state.
    $values = $form_state->getValues();
    $values['search'] = trim($values['search']);

    // Set the element id to be updated.
    $element_id = '#ofis-results-' . $uuid;

    // If there is nothing to search for, simply return.
    if (strlen($values['search']) < 2) {

      // Ensure that we have something to update later and
      // add html that there is no search text.
      $html .= '<div id="ofis-results-' . $uuid . '" class="ofis-results" aria-live="polite">';
      $html .= '<p>You must enter at least 2 characters in order to search.</p>';
      $html .= '</div>';
    }
    else {

      // Ensure that we have something to update later.
      $html .= '<div id="ofis-results-' . $uuid . '" class="ofis-results uw-exp-col-all-flag" aria-live="polite" data-uuid="' . $uuid . '">';

      // Get the values from the form state.
      $values = $form_state->getValues();
      $values['search'] = trim($values['search']);

      // Get the results from the api.
      $results = $this->getResults($values['search'], $values['dept']);

      // If no results, then check if there is space,
      // and if there is a space, then break apart the
      // search string and search separately.
      if (!$results && str_contains($values['search'], " ")) {

        // Possibly searching for first name + last name.
        $keywords = explode(" ", $values['search']);

        // Step through each of the search keywords and perform
        // the search separately.
        foreach ($keywords as $name) {

          // Get the results from the api.
          $results[] = $this->getResults($name, $values['dept']);
        }

        // Merge all the results.
        $results = array_merge([], ...$results);

        // Ensure that the results are unique.
        $results = array_map("unserialize", array_unique(array_map("serialize", $results)));
      }

      // Get a count of the number of results.
      $result_count = count($results);

      // Add the number of results only if there are results.
      if ($result_count > 0) {

        // Add the number of experts to the html.
        $html .= '<p>' . $this->t('Number of experts found:') . ' ' . $result_count . '</p>';
      }

      // Only display exp/col all if there is more than one result.
      switch (TRUE) {
        case ($result_count > 1):
          // Set the exp/col all.
          $html .= '<div class="uw-tabs uw-exp-col">';
          $html .= '<div class="uw-tab" role="tablist" aria-label="Multi-tab-list">';
          $html .= '<button class="uw-tablinks uw-exp-col-button uw-exp-col-expand-all" data-uuid="' . $uuid . '">Expand all</button>';
          $html .= '<button class="uw-tablinks uw-exp-col-button uw-exp-col-collapse-all" data-uuid="' . $uuid . '">Collapse all</button>';
          $html .= '</div>';
          $html .= '</div>';

        case ($result_count > 0):
          // Step through each of the results and get the person info.
          foreach ($results as $result) {
            $html .= $this->getPerson($result);
          }
          break;

        default:
          $html .= '<p>No results. Please try again.</p>';
      }

      // Closing tag for the first div.
      $html .= '</div>';
    }

    // Create a new ajax response and update the results.
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand($element_id, $html));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Rebult the form state.
    $form_state->setRebuild();
  }

  /**
   * Function to get the person info from search results.
   *
   * @param array $person
   *   Array with person information.
   *
   * @return string
   *   The html for the person.
   */
  private function getPerson(array $person): string {

    // Get the link to the actual profile.
    $link = 'https://uwaterloo.ca/' . $this->getDepartmentId($person['departments'][0]) . '/profile/' . $person['userid'];

    // Make header levels variables so they can be dynamic in the future.
    $header_start = 2;
    $first_header = 'h' . $header_start;
    $second_header = 'h' . ($header_start + 1);

    $html = '';
    $html .= '<div class="expert">';
    $html .= '<details class="uw-details " data-once="details">';
    $html .= '<summary class="details__summary">';
    $html .= '<' . $first_header . '>' . $person['first_name'] . ' ' . $person['last_name'] . '</' . $first_header . '>';
    $html .= '<div class="profile">';

    foreach ($person['departments'] as $department) {
      $html .= '<div class="department">' . $department . '</div>';
    }

    $html .= '<div class="contact">Email: ' . $person['email'];
    if ($person['phone']) {
      $html .= ' | Phone: ' . $person['phone'];
    }
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</summary>';

    $html .= '<div class="research-interests">';
    if ($person['keywords']) {
      $html .= '<' . $second_header . '>Research interests</' . $second_header . '>';
      $html .= '<ul>';

      foreach ($person['keywords'] as $interest) {
        $html .= '<li>' . $interest . '</li>';
      }
      $html .= '</ul>';
    }
    $html .= '<div class="more">';
    $html .= '<a href="' . $link . '" class="button">Show full profile</a>';
    $html .= '</div>';
    $html .= '</div>';

    $html .= '</div>';
    return $html;
  }

  /**
   * Function to get the results from the api.
   *
   * @param string $search
   *   The string to search for.
   * @param string $dept
   *   The department to search.
   *
   * @return array
   *   Array of results.
   */
  private function getResults(string $search, string $dept): array {

    // The url to the api for experts.
    $url = sprintf('https://ofis.uwaterloo.ca/api/index.php/list/faculty/%s/%s', $dept, rawurlencode($search));

    // Get the data from the api.
    $data = $this->httpClient->get($url, ['verify' => FALSE]);

    // Get the results from the data.
    return json_decode($data->getBody(), TRUE);
  }

  /**
   * Returns department name as id.
   *
   * @param string $department_name
   *   Department name.
   *
   * @return string
   *   Department id.
   */
  private function getDepartmentId($department_name) {

    // Have at least a default department.
    $department_id = "management-sciences";

    $departments = [
      // Engineering departments.
      'Department of Chemical Engineering' => 'chemical-engineering',
      'Department of Civil and Environmental Engineering' => 'civil-environmental-engineering',
      'Department of Electrical and Computer Engineering' => 'electrical-computer-engineering',
      'Department of Environmental Engineering' => 'civil-environmental-engineering',
      'Department of Geological Engineering' => 'civil-environmental-engineering',
      'Department of Management Sciences and Engineering' => 'management-sciences',
      'Department of Mechanical and Mechatronics Engineering' => 'mechanical-mechatronics-engineering',
      'Department of Systems Design Engineering' => 'systems-design-engineering',
      'School of Architecture' => 'architecture',
      // Science departments.
      'Department of Biology' => 'biology',
      'Department of Chemistry' => 'chemistry',
      'Department of Earth and Environmental Sciences' => 'earth-environmental-sciences',
      'Department of Physics and Astronomy' => 'physics-astronomy',
      'School of Optometry and Vision Science' => 'optometry-vision-science',
      'School of Pharmacy' => 'pharmacy',
    ];

    // If there is a department selected, use it.
    if (isset($departments[$department_name])) {
      $department_id = $departments[$department_name];
    }

    return $department_id;
  }

}
