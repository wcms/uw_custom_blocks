<?php

namespace Drupal\uw_custom_blocks\Form;

use Drupal\block\BlockInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigurationForm.
 *
 * Adds an alternative admin configuration form for special alerts.
 */
class ConfigurationForm extends ConfigFormBase {
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Drupal\block\BlockInterface definition.
   *
   * @var \Drupal\block\BlockInterface
   */
  protected BlockInterface $block;

  /**
   * Config settings.
   *
   * @var string
   */
  public const SETTINGS = 'uw_custom_blocks.special_alert.settings';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;

    $storage = $this->entityTypeManager->getStorage('block');
    $all_blocks = $storage->loadByProperties([
      'plugin' => 'uw_cbl_special_alert',
    ]);

    // There should be one block only, but as it happens there could be
    // more than one block for special alert. So we need to load one with
    // 'specialalert' as an id.
    $this->block = $all_blocks['specialalert'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uw_cbl_special_alert_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Make sure there is a block there first (in default theme) or the form
    // settings will throw errors.
    if (!$this->block) {
      $form['message'] = [
        '#markup' => $this->t('You need to place the block first before you configure it.'),
      ];
      return $form;
    }

    $values = $this->block->getVisibility();
    $settings = $this->block->get('settings');

    $form['display_option'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display this alert'),
      '#default_value' => $settings['display_option'],
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $settings['label'],
      '#description' => $this->t('Optionally enter a title to display with this alert'),
    ];

    $form['label_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display title'),
      '#default_value' => $settings['label_display'] === 'visible',
    ];

    $form['display_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Display style'),
      '#options' => [
        'default' => $this->t('Default'),
        'priority' => $this->t('Priority'),
      ],
      '#default_value' => $settings['display_style'] ?? 'default',
    ];

    $default_date = NULL;
    if (isset($settings['date']) && $settings['date']) {
      $default_date = DrupalDateTime::createFromTimestamp($settings['date']);
    }

    $default_time = NULL;
    if (isset($settings['time']) && $settings['time']) {
      $default_time = DrupalDateTime::createFromTimestamp($settings['time']);
    }

    $form['date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Date'),
      '#default_value' => $default_date,
      '#date_time_format' => 'short',
      '#date_timezone' => 'America/Toronto',
      '#date_date_element' => 'date',
      '#date_time_element' => 'none',
      '#date_year_range' => '-0:+1',
    ];

    $form['time'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Time'),
      '#description' => $this->t('Optionally enter a date and time to display with this alert.'),
      '#default_value' => $default_time,
      '#date_time_format' => 'short',
      '#date_timezone' => 'America/Toronto',
      '#date_date_element' => 'none',
      '#date_time_element' => 'time',
    ];

    $form['message'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_basic',
      '#allowed_formats' => ['uw_tf_basic'],
      '#title' => $this->t('Special alert message'),
      '#required' => TRUE,
      '#default_value' => $settings['message']['value'] ?? '',
    ];
    $form['pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#default_value' => $values['request_path']['pages'] ?? '',
    ];

    $form['request_path']['#title'] = $this->t('Pages');
    $form['request_path']['negate']['#type'] = 'radios';
    $form['request_path']['negate']['#default_value'] = (int) ($values['request_path']['negate'] ?? 0);
    $form['request_path']['negate']['#title_display'] = 'invisible';
    $form['request_path']['negate']['#options'] = [
      $this->t('Show for the listed pages'),
      $this->t('Hide for the listed pages'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the visibility and extra settings (display and message).
    $form_state->cleanValues();
    $settings = $this->block->get('settings');
    $vis = $this->block->getVisibility();
    // Have to set the request_path the first time, or saving on the form will
    // throw a php error that it cannot find it when you set the value.
    if (count($vis) <= 0) {
      $this->block->setVisibilityConfig('request_path', []);
    }
    $vis['request_path']['pages'] = $form_state->getValue('pages');
    $vis['request_path']['negate'] = $form_state->getValue('negate');
    $settings['display_option'] = $form_state->getValue('display_option');
    $settings['message'] = $form_state->getValue('message');
    $settings['label_display'] = $form_state->getValue('label_display') ? "visible" : "0";

    // Saving date/time as timestamp, it may be easier to compare.
    $settings['date'] = NULL;
    if ($dt = $form_state->getValue('date')) {
      $settings['date'] = $dt->getTimestamp();
    }

    $settings['time'] = NULL;
    if ($dt = $form_state->getValue('time')) {
      $settings['time'] = $dt->getTimestamp();
    }

    $settings['display_style'] = $form_state->getValue('display_style');
    $settings['label'] = $form_state->getValue('label');

    $this->block->set('visibility', $vis);
    $this->block->set('settings', $settings);
    $this->block->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Display error when time has a value, but date is empty.
    if ($form_state->getValue('date') === NULL && $form_state->getValue('time')) {
      $form_state->setErrorByName('date', $this->t('Date is required when time is used.'));
    }

    parent::validateForm($form, $form_state);
  }

}
