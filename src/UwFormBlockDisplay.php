<?php

namespace Drupal\uw_custom_blocks;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\Block\ViewsBlock;
use Drupal\views_block_placement_exposed_form_defaults\ExposedFormBlockDisplay;

/**
 * A plugin class that overrides the core views block display plugin class.
 */
class UwFormBlockDisplay extends ExposedFormBlockDisplay {

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    // Get parent build options form.
    parent::buildOptionsForm($form, $form_state);

    // If there is no allowed settings, simply return.
    if ($form_state->get('section') !== 'allow') {
      return;
    }

    // Set the heading level to the allowed settings.
    $form['allow']['#options']['heading_level'] = $this->t('Heading level');
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);

    // Get the allowed settings.
    $allowed = $this->getOption('allow');

    // Reset the listing.
    $options['allow']['value'] = '';

    // If there are no items per page or heading level settings,
    // add the None to the listing.
    if (
      isset($allowed['items_per_page']) &&
      !$allowed['items_per_page'] &&
      isset($allowed['heading_level']) &&
      !$allowed['heading_level']
    ) {
      $options['allow']['value'] .= 'None';
    }
    else {

      // If there are items per page, add to listing.
      if (
        isset($allowed['items_per_page']) &&
        $allowed['items_per_page']
      ) {

        // If there are listings, add the comma.
        if ($options['allow']['value'] !== '') {
          $options['allow']['value'] .= ', ';
        }

        // Add items per page the listing.
        $options['allow']['value'] .= 'Items per page';
      }

      // If there is heading level, add to the listing.
      if (
        isset($allowed['heading_level']) &&
        $allowed['heading_level']
      ) {

        // If there are listings, add the comma.
        if ($options['allow']['value'] !== '') {
          $options['allow']['value'] .= ', ';
        }

        // Add items per page the listing.
        $options['allow']['value'] .= 'Heading level';
      }
    }

    // Add the customizable filter to the listing.
    $customizable_filters = $this->getOption('customizable_exposed_filters');
    $filter_count = !empty($customizable_filters) ? count($customizable_filters) : 0;
    $options['allow']['value'] .= ', ' . $this->formatPlural($filter_count, '1 customizable filter', '@count customizable filters');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm(ViewsBlock $block, array &$form, FormStateInterface $form_state) {

    // Get the parent form.
    $form = parent::blockForm($block, $form, $form_state);

    // If there are items per page, put the heading level first,
    // then the items per page.  If not just add the heading level.
    if (isset($form['override']['items_per_page'])) {

      // The heading level.
      $override['heading_level'] = [
        '#type' => 'select',
        '#title' => $this->t('Heading level'),
        '#options' => _uw_custom_blocks_heading_levels(),
        '#default_value' => $block_configuration['heading_level'] ?? 'h2',
        '#required' => TRUE,
      ];

      // The items per page.
      $override['items_per_page'] = $form['override']['items_per_page'];
    }
    else {

      // The heading level.
      $override['heading_level'] = [
        '#type' => 'select',
        '#title' => $this->t('Heading level'),
        '#options' => _uw_custom_blocks_heading_levels(),
        '#default_value' => $block_configuration['heading_level'] ?? 'h2',
        '#required' => TRUE,
      ];
    }

    // Add the overrides.
    $form['override'] = $override;

    return $form;
  }

  /**
   * Handles form submission for the views block configuration form.
   *
   * @param \Drupal\views\Plugin\Block\ViewsBlock $block
   *   The ViewsBlock plugin.
   * @param mixed $form
   *   The form definition array for the full block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\views\Plugin\Block\ViewsBlock::blockSubmit()
   */
  public function blockSubmit(ViewsBlock $block, $form, FormStateInterface $form_state) {

    // Submit the parent block.
    parent::blockSubmit($block, $form, $form_state);

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Set the heading level to the block config.
    $block->setConfigurationValue('heading_level', $values['override']['heading_level']);
  }

}
